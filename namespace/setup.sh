#!/bin/bash
set -u

callstack() { local frame=0; while caller $frame; do ((frame++)); done; }

fail() { echo "ERROR($?): $@"; callstack >&2; exit 1; return 1; }

log() { echo "## $@" ; return 0; }

gcurl() {
  local uri="$1"
  shift
  curl -s --fail -H "private-token: ${GITLAB_API_TOKEN}" "${GITLAB_URL}/api/v4/${uri}" "$@" || fail
}

main() {
  log "username=${username:=$username}"
  log "project=${project:=$project}"
  log "ci_project=${ci_project:=$ci_project}"
  log "deployment_project=${deployment_project:=$deployment_project}"
  log "namespace=${namespace:=$namespace}"

  GITLAB_URL=$GITLAB_URL
  GITLAB_API_TOKEN=$GITLAB_API_TOKEN
  GIT_SSH_HOST=$GIT_SSH_HOST

  local uid
  log "search username=${username}"
  uid=$(gcurl "users?username=${username}" | jq -r '.[0].id | select(.!=null)')
  log "found id=${uid}"
  if [[ "$uid" != "" ]]; then
    log "delete user id=${uid}"
    gcurl "users/${uid}" -XDELETE || fail
    sleep 2s
  fi

  log "creating username=${username}"
  uid=$(gcurl "users?username=${username}&name=${username}&email=${username}@example.com&force_random_password=true&skip_confirmation=true" -XPOST | jq -r .id)
  echo "- created, id=${uid}"


  log "creating api token for usename=${username}"
  local token
  token=$(gcurl "users/${uid}/personal_access_tokens?name=${username}&scopes=api" -XPOST | jq -r .token)
  log "- token=${token}"

  log "creating ssh key for username=${username}"
  local sshkey
  rm ${username} ${username}.pub &>/dev/null
  trap "rm ${username} ${username}.pub &>/dev/null" EXIT
  ssh-keygen -f ${username} -C "$username" -N "" &>/dev/null || fail
  sshkey=$(gcurl "users/${uid}/keys" -XPOST -d "title=${username}" --data-urlencode "key=$(cat ${username}.pub)" | jq -r .id) || fail
  log "- generated, registered: id=${sshkey}"

  # membership
  log "membership: ${project}"
  gcurl "groups/${project//\//%2F}/members" -d user_id=${uid} -d access_level=20 | jq .id
  log "membership: ${ci_project}"
  gcurl "projects/${ci_project//\//%2F}/members" -d user_id=${uid} -d access_level=20 | jq .id
  log "membership: ${deployment_project}"
  gcurl "projects/${deployment_project//\//%2F}/members" -d user_id=${uid} -d access_level=20 | jq .id

  # k8s
  log "configuring k8s namespace..."
  helm upgrade --install --atomic \
    nsconfig . \
    -n ${namespace} --create-namespace \
    -f local.yaml \
    || fail

  k8s_url=$(kubectl config view -o json | jq -r .clusters[0].cluster.server)
  log "- k8s_url=${k8s_url}"

  k8s_token=$(
    kubectl -n ${namespace} get sa owner -o json \
      | jq -r '.secrets[0].name' \
      | xargs -I% kubectl -n ${namespace} get secret % -o json \
      | jq -r '.data.token' \
      | base64 -d)
  log "- k8s_token=${k8s_token}"

  # cleanup vars
  log "reset related gitlab variables in group ${project}"
  (
  gcurl "groups/${project//\//%2F}/variables/GITLAB_API_TOKEN" -XDELETE
  gcurl "groups/${project//\//%2F}/variables/GIT_SSH_KEY" -XDELETE
  gcurl "groups/${project//\//%2F}/variables/K8S_URL" -XDELETE
  gcurl "groups/${project//\//%2F}/variables/K8S_TOKEN" -XDELETE
  gcurl "groups/${project//\//%2F}/variables/X_NAMESPACE" -XDELETE
  gcurl "groups/${project//\//%2F}/variables/GIT_HOST_KEY" -XDELETE
  ) &>/dev/null

  log "configure gitlab variables in group ${project}"
  # create variables
  gcurl "groups/${project//\//%2F}/variables" -XPOST \
    -d "key=GITLAB_API_TOKEN" \
    --data-urlencode "value=${token}" \
    | jq -r .key
  gcurl "groups/${project//\//%2F}/variables" -XPOST \
    -d "key=GIT_SSH_KEY" \
    --data-urlencode "value=$(cat ${username})" \
    | jq -r .key
  gcurl "groups/${project//\//%2F}/variables" -XPOST \
    -d "key=K8S_URL" \
    --data-urlencode "value=${k8s_url}" \
    | jq -r .key
  gcurl "groups/${project//\//%2F}/variables" -XPOST \
    -d "key=K8S_TOKEN" \
    --data-urlencode "value=${k8s_token}" \
    | jq -r .key
  gcurl "groups/${project//\//%2F}/variables" -XPOST \
    -d "key=X_NAMESPACE" \
    --data-urlencode "value=${namespace}" \
    | jq -r .key

  local ssh_host_key
  ssh_host_key=$(ssh-keyscan -t rsa $GIT_SSH_HOST | awk '{printf "* %s %s",$2,$3}')
  gcurl "groups/${project//\//%2F}/variables" -XPOST \
    -d "key=GIT_HOST_KEY" \
    --data-urlencode "value=$ssh_host_key" \
    | jq -r .key

  gcurl "groups/${project//\//%2F}/variables" | jq .
}


test-project() {
  username=test-project \
  project=test/project \
  ci_project=jrevolt/ci/pipeline \
  deployment_project=jrevolt/ci/deployment \
  namespace=test-project \
  main
}

"$@"

