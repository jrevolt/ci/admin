
import {S3} from "@aws-sdk/client-s3";
import {cfg} from "./cfg";

export function createS3Client() : S3 {
  return new S3({
    endpoint: cfg.s3.url,
    region: 'us-east-1',
    credentials: {
      accessKeyId: cfg.s3.accessKey,
      secretAccessKey: cfg.s3.secretKey,
    },

  })
}
