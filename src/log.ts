import winston, {Logger} from 'winston';
import {cfg} from "./cfg";

// beware: this is bootstrap code, do not add any other project imports (only Configuration is allowed)

export default class Log {

  private static $log : Logger

  private static createLogger() {
    process.on('exit', () => this.$log?.end())
    return winston.createLogger({
      silent: is_unit_test() && !cfg.debug,
      level: cfg.debug ? 'debug' : is_unit_test() ? 'error' : 'info',
      transports: [
        new winston.transports.Console({format: format()}),
      ],
    })
  }

  static get instance() : Logger {
    return this.$log || (this.$log = this.createLogger())
  }

}

export function is_unit_test() {
  return process.env.NODE_ENV == "test" && process.env.JEST_WORKER_ID != undefined
}

export function is_cli() {
  return !is_unit_test()
}

function format() {
  return winston.format.combine(
    winston.format.splat(),
    winston.format.simple(),
    winston.format.timestamp(),
    winston.format.printf(({timestamp, level, label, message}) => `${timestamp}|${lvl(level)}|${cfg.command}| ${message}`),
  );
}

function lvl(level) {
  // dirty translation to (DBG|INF|WRN|ERR)
  return level.toLowerCase()
    .replace(/[aeou]/gi, '')
    .substring(0, 3)
    .toUpperCase()
    .replace(/RRR/gi, 'ERR');
}

export const log = Log.instance
