import {Kibana} from "./kibana";
import {ignore} from "./utils";
import './testutils';
import {Gitlab, GitlabAccess} from "./gitlab";
import {Elastic} from "./elastic";

describe('kibana', () => {

  let kibana = new Kibana()

  let id

  beforeEach(() => {
    id = id = new Date().getTime().toString().replace(/.*(\d{5})$/, '$1')
  })

  describe('spaces', () => {

    let name

    beforeEach(() => {
      name = `test-space-${id}`
    })

    afterEach(async () => {
      await kibana.deleteSpace(name).catch(ignore)
    })


    test('create', async () => {
      await expect(kibana.createSpace(name)).resolves.toBeDefined()
      await expect(kibana.createSpace(name)).rejects.toThrow(/already exists/)
    })

    test('get', async () => {
      await expect(kibana.getSpace(name)).resolves.toBeUndefined()
      await kibana.createSpace(name)
      await expect(kibana.getSpace(name)).resolves.toBeDefined()
    })

    test('make', async () => {
      await expect(kibana.makeSpace(name)).resolves.toBeDefined() //create
      await expect(kibana.makeSpace(name)).resolves.toBeDefined() //get
    })

    test('list', async () => {
      await expect(kibana.listSpaces()).resolves.toBeNonEmptyArray()
    })

    test('delete', async () => {
      await expect (kibana.deleteSpace(name)).resolves.toBeFalsy()  // unknown
      await kibana.createSpace(name)
      await expect (kibana.deleteSpace(name)).resolves.toBeTruthy() // existing
    })

  })

  describe('roles', () => {

    let name

    beforeEach(() => name=`test-role-${id}`)

    afterEach(async () => {
      await kibana.deleteRole(name).catch(ignore)
    })

    test('make', async () => {
      await expect(kibana.makeRole(name)).resolves.toBeUndefined()
    })

    test('get', async () => {
      await expect(kibana.getRole(name)).resolves.toBeUndefined()
      await kibana.makeRole(name)
      await expect(kibana.getRole(name)).resolves.toBeDefined()
    })

    test('list', async () => {
      await kibana.makeRole(name)
      let roles = await kibana.listRoles()
      expect(roles).toBeNonEmptyArray()
      expect(roles.find(x => x.name == name)).toBeDefined()
    })

    test('delete', async () => {
      await expect(kibana.deleteRole(name)).resolves.toBeFalsy()
      await kibana.makeRole(name)
      await expect(kibana.deleteRole(name)).resolves.toBeTruthy()
    })
  })

  describe('index patterns', () => {

    let space

    beforeEach(async () => {
      space = `test-space-${id}`
      await kibana.makeSpace(space)
    })

    afterEach(async () => {
      await kibana.deleteSpace(space).catch(ignore)
    })

    test('getDefault', async () => {
      await expect(kibana.getDefaultIndexPattern(space)).resolves.toBeUndefined()
    })

    test('setDefault', async () => {
      let created = await kibana.createIndexPattern(space, {setDefault: true})
      await expect(kibana.getDefaultIndexPattern(space)).resolves.toBe(created.id)
    })

    test('create', async () => {
      await expect(kibana.createIndexPattern(space)).resolves.toBeDefined() //create
      await expect(kibana.createIndexPattern(space)).resolves.toBeDefined() //update
    })

    test('get', async () => {
      await expect(kibana.getIndexPattern(space)).resolves.toBeUndefined()
      // get by id
      let created = await kibana.createIndexPattern(space)
      await expect(kibana.getIndexPattern(space, created.id)).resolves.toBeDefined()
      // get as default
      await kibana.setDefaultIndexPattern(space, created.id)
      await expect(kibana.getIndexPattern(space)).resolves.toBeDefined()
    })

    test('list', async () => {
      let patterns = await kibana.listIndexPatterns(space)
      expect(patterns).toHaveLength(0)
      await kibana.createIndexPattern(space)
      patterns = await kibana.listIndexPatterns(space)
      expect(patterns).toHaveLength(1)
    })

    test('find', async () => {
      await expect(kibana.findIndexPattern(space, 'xxx-*')).resolves.toBeUndefined()
    })

    test('delete', async () => {
      await expect(kibana.deleteIndexPattern(space)).resolves.toBeFalsy()

      // default
      await kibana.createIndexPattern(space, {setDefault: true})
      await expect(kibana.deleteIndexPattern(space)).resolves.toBeTruthy()

      // explicit
      let created = await kibana.createIndexPattern(space)
      await expect(kibana.deleteIndexPattern(space, created.id)).resolves.toBeTruthy()
    })
  })

  describe('custom', () => {

    let gitlab = new Gitlab()
    let elastic = new Elastic()

    let project, space
    let onexit

    beforeEach(async () => {
      project = `test/space-${id}`
      space = `test-space-${id}`
      onexit = []
      await kibana.makeSpace(space, project)
    })

    afterEach(async () => {
      await kibana.deleteSpace(space).catch(ignore)
      await gitlab.deleteGroup(project).catch(ignore)

      let groups = await gitlab.listGroups()
      await groups
        .filter(x => x.full_path.startsWith('test/space-'))
        .forEachAsync(async (g) => await gitlab.deleteGroup(g.full_path).catch(ignore))

      await kibana.listRoles()
        .then(roles => roles.filter(x => x.name?.startsWith(`${space}:`)))
        .then(async (roles) => roles.forEachAsync(async (x) => await kibana.deleteRole(x.name)))
        .catch(ignore)
      await onexit.forEachAsync(async (x) => await x().catch(ignore))
    })

    test('setupProject', async () => {
      await kibana.setupProject({path: space})
    })

    test('syncUsers', async () => {
      let gitlab = new Gitlab()
      let repo = `${project}/repo`
      let user = `test-user-${id}`

      await gitlab.makeGroup(project)
      onexit.push(async () => await gitlab.deleteGroup(project))

      await gitlab.makeProject(repo)
      await gitlab.createUser(user, `${user}@example.com`)
      onexit.push(async () => await gitlab.deleteUser(user))

      await gitlab.addMember(user, project, GitlabAccess.Maintainer)

      await kibana.setupProject({path: project, space: space, username: user})

      onexit.push(async () => await elastic.deleteUser(user))
      await kibana.syncUsers()
    })

    test('activateUsers', async () => {
      let user = `test-user-${id}`
      onexit.push(async () => await gitlab.deleteUser(user))
      onexit.push(async () => await elastic.deleteUser(user))

      await gitlab.createUser(user, `patrikbeno+${user}@gmail.com`)
      await elastic.createUser(user)
      await kibana.activateNewUsers()
    })
  })

})



