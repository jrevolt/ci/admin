import config from 'config'
import {RequestOptions} from "http";
import Bottleneck from "bottleneck";

// beware: this is bootstrap code, do not add any project imports (only 3rd parties)

export class Configuration {

  command: string|any
  debug: boolean|any
  debugTruncateBody: number|any

  caFile: string

  cacerts: any // map

  proxy?: {
    url: string
    nonProxyHosts: string[]
  }

  shell: string

  ldap: {
    url: string
    username: string
    password: string
    baseDN: string
    domain: string
    tls: {
      strict: boolean
      cacerts: string
    }
    users: {
      class: string
      username: string
      email: string
      base: string
      filter: string
      attributes: string[]
    }
    groups: {
      class: string
      name: string
      base: string
      filter: string
      attributes: string[]
    }

    filter: string
    dnFilter: string
    mapping: any
    ignored: string[]
    cache: boolean
  }

  s3: {
    url: string
    internalUrl: string
    accessKey: string
    secretKey: string
  }

  gitlab: {
    url: string
    token: string
  }

  sonar: {
    url: string
    internalUrl: string
    token: string
    badgesUrl: string
    useBranchSupport: boolean
  }

  rancher: {
    url: string
    token: string
  }

  nexus: {
    url: string
    username: string
    password: string
    bucketNamePrefix: string
    repositories: {}
    deleteRepositories: string[]
  }

  elastic: {
    url: string
    username: string
    password: string
  }

  kibana: {
    url: string
    username: string
    password: string
  }

  mailer: {
    transport: any // nodemailer config
    from: string
    domainFilter: {
      allow: string
      deny: string
    }
  }

  ciadmin: {
    domain: string
    request: RequestOptions
    bottleneck: Bottleneck.ConstructorOptions
  }

  ci: {
    project: string
    variables: any
    sonar: {
      defaultQualityProfileName: string
    }
    rancher: {
      clusterId: string
    }
  }
  cd: {
    kubernetes: {
      nsconfig: {
        helmReleaseName: string
        values: any
      }
    }
  }

  cisetup: {
    project: {
      path: string
      username: string
      namespace: string
      member: string[]
    }
    mappings: {
      gitlabApiToken: string
      gitSshPrivateKey: string
      kubeUrl: string
      kubeToken: string
      kubeNamespace: string
      sonarToken: string
      sonarLanguages: string
    }
    masked: string
    variables: Map<string,string>
    repositories: any[] //todo
  }

  groups: Group[]|any

  private static $instance : Configuration

  static get instance() : Configuration {
    return this.$instance || (this.$instance = new Configuration())
  }

  constructor() {
    process.env['ALLOW_CONFIG_MUTATIONS']='true'
    Object.assign(this, config.get('ciadmin'))
    this.resolveReferences()
  }

  resolveReferences() {
    this.groups?.forEach(g => this.resolveReferencesByGroup(g))
  }

  resolveReferencesByGroup(g:Group) {
    g.groups?.forEach(x => {
      x.parent = g
      this.resolveReferencesByGroup(x)
    })
    g.projects?.forEach(p => {
      p.group = g
      p.repositories?.forEach(r => r.project = p)
    })
  }


  getAllGroups(group?:Group, all:Group[]=[]) : Group[] {
    let groups = group ? [group] : this.groups
    groups.forEach(g=>{
      all.push(g)
      g.groups?.forEach(x => this.getAllGroups(x, all))
    })
    return all
  }

  getAllProjects(group?:Group, all:Project[]=[]) : Project[] {
    let groups : Group[] = group ? [group] : this.groups
    groups.forEach(g => {
      g.projects?.forEach(p => all.push(p))
      g.groups?.forEach(x => this.getAllProjects(x, all))
    })
    return all
  }

  getAllRepositories(group?:Group, all:Repository[]=[]) : Repository[] {
    return this.getAllProjects(group).flatMap(p => p.repositories) as Repository[]
  }

}

interface Named {
  name: string
}

interface Maintained {
  maintainers?: string[]
}

interface Configured {
  settings?: any,
  options?: any
}

export interface Group extends Named, Maintained, Configured {
  parent?: Group
  groups?: Group[]
  projects?: Project[]
}

export interface Project extends Named, Maintained, Configured {
  group: Group
  repositories?: Repository[]
}

export interface Repository extends Named, Maintained, Configured {
  project: Project
  //type?: string
  defaultBranch?: string

}

export const cfg = Configuration.instance