import {createS3Client} from "./s3";

let s3 = createS3Client();

afterAll(async () => {
})

test('s3', async () => {
  let all = await s3.listBuckets({})
  let created = await s3.createBucket({Bucket: 'test1'})
  await s3.deleteBucket({Bucket: 'test1'})
  return s3
})

