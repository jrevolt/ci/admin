import extend from "extend";
import {cfg} from "./cfg";
import {fail, rethrow, xrequest} from "./utils";
import assert from "assert";
import {Role} from "./ciadmin";



class RCluster {
  id: string
  name: string
}

class RProject {
  id: string
  name: string
  clusterId: string
  namespaceId: string
}

class RNamespace {
  id: string
  name: string
  projectId: string
}

export class RPrincipal {
  id: string
  loginName: string
  name: string
  provider: string
}

class RUser {
  id: string
  username: string
  name: string
  enabled: boolean
  principalIds: string[]
}

class RRoleTemplateBinding {
  id: string
  name: string
  userId: string
  userPrincipalId: string
  roleTemplateId: string

  constructor(src:Partial<RRoleTemplateBinding>) { Object.assign(this, src) }
}


export class Rancher {

  clusterId = cfg.ci.rancher.clusterId

  /// clusters ///

  async getCluster(name) : Promise<RCluster> {
    return await xrequest(get(`clusters`)).then(all => all.data.find(c => c.name == this.clusterId))
  }

  /// namespaces ///

  async createNamespace(name, project?:RProject) : Promise<RNamespace> {
    return await xrequest(post(`clusters/${this.clusterId}/namespaces`, { name: name, projectId: project?.id }))
  }

  async getNamespace(name) : Promise<RNamespace> {
    return await xrequest(get(`clusters/${this.clusterId}/namespaces/${name}`))
      .catch(err => /not found/i.test(err.message) ? undefined : rethrow(err))
  }

  async makeNamespace(name, project?:RProject) {
    return (
      await this.getNamespace(name)
      ||
      await this.createNamespace(name, project)
    )
  }

  async listNamespaces() : Promise<RNamespace[]> {
    return await xrequest(get(`clusters/${this.clusterId}/namespaces`)).then(all => all.data)
  }

  async deleteNamespace(name) {
    await xrequest(del(`clusters/${this.clusterId}/namespaces/${name}`))
  }

  /// projects ///

  async createProject(name) : Promise<RProject> {
    await this.getProject(name) && fail(`Project already exists: ${name}`)
    return await xrequest(post(`projects`, {
      name: name,
      clusterId: this.clusterId,
    }))
  }

  async getProject(name) : Promise<RProject|undefined> {
    let found = await xrequest(get(`clusters/${this.clusterId}/projects`))
      .then(all => all.data.filter(p => p.name == name))
    if (found.length > 1) fail(`Multiple projects with name: ${name}: ${found}`)
    return found.first()
  }

  async listProjects() : Promise<RProject[]> {
    return await xrequest(get(`clusters/${this.clusterId}/projects`)).then(all => all.data)
  }

  async deleteProject(name) {
    let p = await this.getProject(name) || fail(`Project not found: ${name}`)
    await xrequest(del(`projects/${p.id}`))
  }

  async makeProject(name) : Promise<RProject> {
    return (
      await this.getProject(name)
      ||
      await this.createProject(name)
    )
  }

  /// users ///

  async createUser(username) : Promise<RUser> {
    return await xrequest(post(`users`, { username: username, password: 'eeCoof7aitei', mustChangePassword: true }))
  }

  async getUser(username) : Promise<RUser> {
    return await xrequest(get(``))
  }

  async makeUser(username) {
    return (
      await this.getUser(username)
      ||
      await this.createUser(username)
    )
  }

  async listUsers() {}

  async deleteUser(username) {
    let u = await this.getUser(username)
    assert(u,`User not found: ${username}`)
    await xrequest(del(`users/${u.id}`))
  }


  /// principals ///

  async getPrincipal(username) : Promise<RPrincipal|undefined> {
    return xrequest(qpost(`principals`, {action: 'search'}, { name: username }))
      .then(all => all.data.find(x => x.loginName == username))
  }

  async listPrincipals() {
    return await xrequest(qpost(`principals`, {action: 'search'}, { name: '', principalType: 'user' }))
      .then(all => all.data)
  }


  /// groups ///


  /// memberships ///

  async addClusterMember(username) {
    let [ u, list ] = await Promise.all([
      this.getPrincipal(username),
      this.listClusterMembers(),
    ])
    assert(u)
    let uid = u?.id
    let done = list.filter(x => x.userPrincipalId == uid).map(x => x.roleTemplateId)
    let roles = ['nodes-view', 'projects-view'].filter(x => !done.contains(x))
    await roles.forEachAsync(async (r) =>
      await xrequest(post(`clusters/${this.clusterId}/clusterroletemplatebindings`, {
        userPrincipalId: uid,
        clusterId: this.clusterId,
        roleTemplateId: r,
      })))
  }

  async addProjectMember(username, projectName, role: Role) {
    let [u,p] = await Promise.all([
      this.getPrincipal(username),
      this.getProject(projectName),
    ])
    assert(u)
    assert(p)
    let roleTemplates = [ 'project-member', 'read-only' ]
    await xrequest(post(`projects/${p.id}/projectroletemplatebindings`, {
      userPrincipalId: u.id,
      projectId: p.id,
      roleTemplateId: roleTemplates[role],
    }))
  }

  async listClusterMembers() : Promise<RRoleTemplateBinding[]> {
    return await xrequest(get(`clusters/${this.clusterId}/clusterroletemplatebindings`)).then(all => all.data)
  }



}


///

function req(uri, method, options, query, data) {
  let x = cfg;
  return extend(
    {
      url: `${cfg.rancher.url}/v3/${uri}`,
      headers: {
        'Authorization': `Basic ${Buffer.from(cfg.rancher.token).toString('base64')}`,
        'Content-Type': 'application/json',
        'Accept': 'application/json',
      },
      method: method,
      resolveWithFullResponse: false,
      json: true,
      body: data,
    },
    options,
    { qs: query }
  );
}

function get(uri, query?) {
  return req(uri, 'GET', {}, query, {});
}

function post(uri, data) {
  return req(uri, 'POST', {}, {}, data);
}

function qpost(uri, query, data) {
  return req(uri, 'POST', {}, query, data);
}

function put(uri, data) {
  return req(uri, 'PUT', {}, {}, data);
}

function del(uri) {
  return req(uri, 'DELETE', {}, {}, {});
}

