import './testutils'
import {Elastic} from "./elastic";
import {ignore} from "./utils";

describe('elastic', () => {

  let elastic = new Elastic()

  let id

  let onexit

  beforeEach(() => {
    id = id = new Date().getTime().toString().replace(/.*(\d{5})$/, '$1')
    onexit = []
  })

  afterEach(async () => {
    await onexit.forEachAsync(async (x) => await x().catch(ignore))
  })

  describe('roles', () => {

    let name

    beforeEach(() => {
      name = `test-role-${id}`
    })

    afterEach(async () => {
      await elastic.deleteRole(name).catch(ignore)
    })

    test('create', async () => {
      await expect(elastic.createRole(name)).resolves.toBeDefined() // create
      await expect(elastic.createRole(name)).resolves.toBeDefined() // update
    })

    test('get', async () => {
      await expect(elastic.getRole(name)).resolves.toBeUndefined()
      await elastic.createRole(name)
      await expect(elastic.getRole(name)).resolves.toBeDefined()
    })

    test('list', async () => {
      await elastic.createRole(name)
      let roles = await elastic.listRoles()
      expect(roles).toBeNonEmptyArray()
      expect(roles.find(x => x.name == name)).toBeDefined()
    })

    test('delete', async () => {
      await expect(elastic.deleteRole(name)).resolves.toBeFalsy()
      await elastic.createRole(name)
      await expect(elastic.deleteRole(name)).resolves.toBeTruthy()
    })
  })

  describe('users', () => {

    let username, role

    beforeEach(() => {
      username = `test-user-${id}`
      role = `test-role-${id}`
    })

    afterEach(async () => {
      await elastic.deleteUser(username).catch(ignore)
      await elastic.deleteRole(role).catch(ignore)
    })

    test('create', async () => {
      await expect(elastic.createUser(username)).resolves.toBeDefined()
    })

    test('get', async () => {
      await expect(elastic.getUser(username)).resolves.toBeUndefined()
      await expect(elastic.createUser(username)).resolves.toBeDefined()
      await expect(elastic.getUser(username)).resolves.toBeDefined()
    })

    test('make', async () => {
      await expect(elastic.getUser(username)).resolves.toBeUndefined()
      await expect(elastic.makeUser(username)).resolves.toBeDefined()
      await expect(elastic.getUser(username)).resolves.toBeDefined()
    })

    test('list', async () => {
      await expect(elastic.listUsers()).resolves.toBeNonEmptyArray()
    })

    test('delete', async () => {
      await expect(elastic.deleteUser(username)).rejects.toThrow()
      await elastic.createUser(username)
      await expect(elastic.deleteUser(username)).resolves.toBeUndefined()
    })

    test('roles', async () => {
      let u = await elastic.createUser(username)
      await elastic.createRole(role)
      u.roles?.push(role)
      await expect(elastic.updateUser(u)).resolves.toBeDefined()
    })

  })

  describe('lifecycle policies', () => {

    let id, name;

    beforeEach(() => {
      id = id = new Date().getTime().toString().replace(/.*(\d{5})$/, '$1')
      name = `test-policy-${id}`
    })

    afterEach(async () => {
      await elastic.listLifecyclePolicies().then(async (result) =>
        await result
          .filter(x => x.name.match(/^test-policy-.*/))
          .forEachAsync(async (x) => await elastic.deleteLifecyclePolicy(x.name)))
    })

    test('create', async () => {
      await expect(elastic.createLifecyclePolicy(name)).resolves.toBeDefined()
    })

    test('get', async () => {
      await expect(elastic.getLifecyclePolicy(name)).resolves.toBeUndefined()
      await elastic.createLifecyclePolicy(name)
      await expect(elastic.getLifecyclePolicy(name)).resolves.toBeDefined()
    })

    test('make', async () => {
      await expect(elastic.makeLifecyclePolicy(name)).resolves.toBeDefined()
    })

    test('list', async () => {
      await expect(elastic.listLifecyclePolicies()).resolves.toBeNonEmptyArray()
      await elastic.createLifecyclePolicy(name)
      let list = await elastic.listLifecyclePolicies()
      expect(list.find(x => x.name === name)).toBeDefined()
    })

    test('delete', async () => {
      await expect(elastic.deleteLifecyclePolicy(name)).resolves.toBeFalsy()
      await elastic.makeLifecyclePolicy(name)
      await expect(elastic.deleteLifecyclePolicy(name)).resolves.toBeTruthy()
    })
  })

  describe('index templates', () => {

    let id, name;

    beforeEach(() => {
      id = id = new Date().getTime().toString().replace(/.*(\d{5})$/, '$1')
      name = `test-index-template-${id}`
    })

    afterEach(async () => {
      await elastic.deleteIndexTemplate(name).catch(ignore)
    })

    test('create', async () => {
      await elastic.createIndexTemplate(name)
    })

    test('get', async () => {
      await expect(elastic.getIndexTemplate(name)).resolves.toBeUndefined()
      await elastic.createIndexTemplate(name)
      await expect(elastic.getIndexTemplate(name)).resolves.toBeDefined()
    })

    test('list', async () => {
      await elastic.createIndexTemplate(name)
      let templates = await elastic.listIndexTemplates()
      expect(templates).toBeNonEmptyArray()
      expect(templates.find(x => x.name == name)).toBeDefined()
    })

    test('delete', async () => {
      await expect(elastic.deleteIndexTemplate(name)).resolves.toBeFalsy()
      await elastic.createIndexTemplate(name)
      await expect(elastic.deleteIndexTemplate(name)).resolves.toBeTruthy()
    })
  })

  describe('indices', () => {
    test('create', async () => {
      let name = `<test-index-${id}-{now/d}-0>`
      let alias = `test-alias-${id}`
      onexit.push(async () => await elastic.getIndex(alias).then(async (x) => await elastic.deleteIndex(Object.keys(x).first())))
      await elastic.createIndex(name, {}, alias)
    })
  })

})