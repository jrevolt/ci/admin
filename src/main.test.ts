import {cfg, Group, Project, Repository} from "./cfg";
import {ignore} from "./utils";
import {Main} from "./main";
import {PathRef} from "./ciadmin";
import {Gitlab} from "./gitlab";
import {Sonar} from "./sonar";



let main : Main
let gitlab : Gitlab
let sonar : Sonar

let id:string, gname:string, pname:string, rname:string, bname:string

let r:Repository, p:Project, g:Group

let ref : PathRef

beforeAll(async () => {
  main = new Main()
  gitlab = new Gitlab()
  sonar = new Sonar()
  id = new Date().getTime().toString().replace(/.*(\d{5})$/, '$1')
  gname = `group-${id}`
  pname = `project-${id}`
  rname = `repo-${id}`
  bname = `master`
  cfg.groups.push(g = {
    name: gname,
    projects: [ p = {
      name: pname,
      repositories: [ r = {
        name: rname,
        options: { language: 'cs' }
      } as Repository]
    } as Project]
  } as Group)
  cfg.resolveReferences()
  ref = PathRef.parse(`${gname}/${pname}/${rname}:${bname}`)
})

describe('sonar', () => {

  let path:string, spath:string

  beforeEach(() => {
    path = `${gname}/${pname}/${rname}`
    spath = `${gname}:${pname}:${rname}:${bname}`
  })

  afterEach(async () => {
    await Promise.all([
      sonar.deleteProject(ref),
      sonar.deleteProject(ref.asRepoRef()),
      sonar.deleteUserGroups(ref, true),
      sonar.deletePermissionTemplate(ref),
      sonar.deleteQualityProfile(sonar.qualityProfileName(ref), 'cs', true),
      sonar.deleteQualityGate(ref),
      gitlab.deleteGroup(gname),
    ]).catch(ignore)
  })

  test('prepare-sonar', async () => {
    await main.run(['prepare-sonar', path])
  })

  test('update-merge-request-with-sonar-badges', async () => {
    await main.run(['update-merge-request-with-sonar-badges', '--project', 'test/project/services'])
  })

  test('consolidate-sonar-users', async () => {
    await main.run(['consolidate-sonar-users'])
  })

  test('update-sonar-groups', async () => {
    await Promise.all([
      gitlab.makeProject(path),
      sonar.makeUserGroups(ref),
    ])
    await main.run(['update-sonar-groups', path])
  })

})



