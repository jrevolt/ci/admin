import './testutils'
import {Ldap} from "./ldap";
import {ignore} from "./utils";

describe('ldap', () => {

  let ldap : Ldap
  let id, user, group

  let onexit

  beforeEach(async () => {
    id = new Date().getTime().toString().replace(/.*(\d{5})$/, '$1')
    user = `user${id}`
    group = `group${id}`
    ldap = new Ldap()
    await ldap.connect()
    onexit=[]
  })

  afterEach(async () => {
    await Promise.all([
      ldap.deleteUser(user),
      ldap.deleteGroup(group),
    ]).catch(ignore)
    onexit.forEachAsync(async (x) => await x().catch(ignore)).catch(ignore)
    await ldap.disconnect().catch(ignore)
  })

  describe('users', () => {
    test('create', async () => {
      await expect(ldap.createUser(user)).resolves.toHaveProperty('cn', user)
      await expect(ldap.createUser(user)).rejects.toThrow(/entry_exists/i)
    })
    test('get', async () => {
      await expect(ldap.getUser(user)).resolves.toBeUndefined()
      await ldap.createUser(user)
      await expect(ldap.getUser(user)).resolves.toHaveProperty('cn', user)
    })
    test('list', async () => {
      await expect(ldap.listUsers()).resolves.toBeNonEmptyArray()
      let u = await ldap.createUser(user)
      await expect(ldap.listUsers()).resolves.toContainEqual(u)
    })
    test('delete', async () => {
      await expect(ldap.deleteGroup(group)).rejects.toThrow(/no_object/i)
      await ldap.createGroup(group)
      await expect(ldap.deleteGroup(group)).resolves.toBeUndefined()
    })

    test('create with group', async () => {
      await ldap.createGroup(group)
      let created = await ldap.createUser(user, group)
      expect(created).toBeDefined()
      expect(created.memberOf).toMatch(new RegExp(group))
    })

  })

  describe('groups', () => {
    test('create', async () => {
      await expect(ldap.createGroup(group)).resolves.toHaveProperty('cn', group)
      await expect(ldap.createGroup(group)).rejects.toThrow(/entry_exists/i)
    })
    test('get', async () => {
      await expect(ldap.getGroup(group)).resolves.toBeUndefined()
      await ldap.createGroup(group)
      await expect(ldap.getGroup(group)).resolves.toHaveProperty('cn', group)
    })
    test('list', async () => {
      await expect(ldap.listGroups()).resolves.toBeDefined()
      let g = await ldap.createGroup(group)
      await expect(ldap.listGroups()).resolves.toContainEqual(g)

    })
    test('delete', async () => {
      await expect(ldap.deleteGroup(group)).rejects.toThrow(/no_object/i)
      await ldap.createGroup(group)
      await expect(ldap.deleteGroup(group)).resolves.toBeUndefined()
    })
  })

  describe('members', () => {

    beforeEach(async () => {
      await ldap.createGroup(group)
      await ldap.createUser(user)
    })

    afterEach(async () => {
      await ldap.deleteUser(user).catch(ignore)
      await ldap.deleteGroup(group).catch(ignore)
    })

    test('add', async () => {
      await expect(ldap.addGroupMember(user, group)).resolves.toBeUndefined()
      await expect(ldap.addGroupMember(user, group)).rejects.toThrow(/ENTRY_EXISTS/)
    })

    test('remove', async () => {
      await ldap.addGroupMember(user, group)
      await expect(ldap.removeGroupMember(user, group)).resolves.toBeUndefined()
      await expect(ldap.removeGroupMember(user, group)).rejects.toThrow(/WILL_NOT_PERFORM/)
    })
  })

})

