import {
  configureNexusProxy,
  configureNexusTruststore,
  prepareProject,
  prepareSonar,
  updateMergeRequestWithSonarBadges
} from "./commands";
import {cfg} from "./cfg";
import {Sonar} from "./sonar";
import {PathRef} from "./ciadmin";
import {Gitlab} from "./gitlab";



describe('commands', () => {

  jest.setTimeout(60000)

  const gitlab = new Gitlab()
  const sonar = new Sonar()

  beforeEach(async () => {
    cfg.groups = [{
      name: 'test',
      projects: [{
        name: 'project',
        repositories: [
          { name: 'console', languages: ['cs'] },
        ]
      }],
    }]
    cfg.resolveReferences()
    cfg.cisetup.project = {
      path: 'test/project',
      username: 'test-project',
      namespace: 'test-project',
      member: [],
    }
    cfg.cisetup.repositories = [{
      name: 'console',
      languages: [ 'cs' ],
    }]
    await gitlab.makeProject('test/project/console')
  })

  afterEach(async () => {
    let ref = PathRef.parse('test/project/console:feature')
    await sonar.deleteQualityProfile(sonar.qualityProfileName(ref), 'cs', true)
    await sonar.deleteQualityGate(ref)
    await sonar.deleteProject(ref)
    await sonar.deleteApplication(ref)
    await gitlab.deleteGroup(ref.group!)
  })

  test('prepare project', async () => {
    await prepareProject()
  })

  test('prepare sonar', async () => {
    await prepareSonar({
      project: 'test/project/console',
      mainBranch: 'main',
      branch: 'feature',
      languages: 'cs'
    })
  })
  test('badges', async() => {
    await updateMergeRequestWithSonarBadges({
      project: 'test/project/console',
      branch: 'feature',
    })
  })
  test('configure-nexus-proxy', async () => {
    await configureNexusProxy()
  })
  test('configure-nexus-truststore', async () => {
    await configureNexusTruststore()
  })
})