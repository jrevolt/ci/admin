import {cfg} from "./cfg";



test('cfg', () => {
  expect(cfg).toBeDefined()
  expect(cfg.groups).toBeDefined()
})

test('get all groups', () => {
  expect(cfg.getAllGroups().flatMap(g=>g.name)).toEqual(['g-1', 'g-1.1'])
})