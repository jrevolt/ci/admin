import {rethrow} from "./utils";
import {Main} from "./main";
import {CIAdmin, PathRef} from "./ciadmin";
import {cfg} from "./cfg";

export interface Options {
  quiet?: boolean,
  debug?: boolean,
  logfile?: string,

}

export function registerCommands(main: Main) {
  main.program
    .command('prepare-project')
    .option('-p, --project [project]')
    .option('-l, --languages [languages]')
    .action(main.wrap(prepareProject))
  main.program
    .command('prepare-sonar')
    .option('-p, --project [project]')
    .option('-b, --branch [branch]')
    .option('-l, --languages [languages]', 'comma separated list of languages')
    .action(main.wrap(prepareSonar))
  main.program
    .command('update-merge-request-with-sonar-badges')
    .option('-p, --project [project]', 'project path')
    .option('-b, --branch [branch]', 'branch name')
    .action(main.wrap(updateMergeRequestWithSonarBadges))
  // main.program
  //   .command('prepare-sonar [project]')
  //   .option('-x, --xxx', 'xxx')
  //   .action(main.wrap(prepareSonar))
  // main.program
  //   .command('consolidate-sonar-users')
  //   .action(main.wrap(consolidateSonarUsers))
  main.program
    .command('update-sonar-groups')
    .action(main.wrap(updateSonarUserGroups))
  main.program
    .command('update-gitlab-users')
    .action(main.wrap(updateGitlabUsers))
  main.program
    .command('update-elastic-users')
    .action(main.wrap(updateElasticUsers))
  main.program
    .command('activate-new-elastic-users')
    .action(main.wrap(activateNewElasticUsers))

  main.program
    .command('configure-nexus-repositories')
    .action(main.wrap(configureNexusRepositories))

  main.program
    .command('configure-nexus-proxy')
    .action(main.wrap(configureNexusProxy))
  main.program
    .command('configure-nexus-truststore')
    .action(main.wrap(configureNexusTruststore))
  main.program
    .command('configure-nexus-auth')
    .action(main.wrap(configureNexusAuth))

  // todo: on-branch-deleted --project --branch

  // todo: cleanup-obsolete-sonar-projects
  // todo: cleanup-obsolete-helm-releases
  // todo: cleanup-obsolete-docker-snapshots
  // todo: cleanup-obsolete-nuget-snapshots
  // todo: cleanup-obsolete-npm-snapshots
}

export interface ProjectOptions extends Options {
  project: string
  branch: string
  languages: string //csv
}

export interface PrepareProjectOptions extends ProjectOptions {}

export async function prepareProject(opts?: PrepareProjectOptions) {
  await ciadmin.setupProject().catch(rethrow);
}

export interface PrepareSonarOptions extends ProjectOptions {
  mainBranch: string
  languages: string //csv
}

export async function prepareSonar(opts: PrepareSonarOptions) {
  let ref = opts.branch
    ? PathRef.parse(`${opts.project}:${opts.branch}`)
    : PathRef.parse(opts.project)
  let languages = opts.languages?.split(',')
  await ciadmin.prepareSonarProject(ref, opts.mainBranch, languages)
}

export interface UpdateMergeRequestWithSonarBadgesOptions extends Options {
  project: string
  branch: string
}

export async function updateMergeRequestWithSonarBadges(opts: UpdateMergeRequestWithSonarBadgesOptions) {
  await gitlab.updateSonarBadgesInMergeRequestDescription(opts.project, opts.branch)
}

// export interface PrepareSonarOptions extends Options {}
//
// export async function prepareSonar(opts: PrepareSonarOptions, project: string) {
//   project || fail('project')
//   let ref = gitlab.parseProjectPath(project)
//   let r = gitlab.resolveRepository(ref)
//   r || fail(`Unsupported: ${project}`)
//   //fixme await sonar.prepareSonarProjectForCI(r, ref.branch)
// }

export async function consolidateSonarUsers() {
  await sonar.consolidateSonarUsers()
}

interface UpdateUserGroupsOptions extends Options {}

export async function updateSonarUserGroups(opts: UpdateUserGroupsOptions) {
  // project || fail('project')
  // let ref = gitlab.parseProjectPath(project)
  // let r = gitlab.resolveRepository(ref)
  // r || fail(`Unsupported: ${project}`)
  await sonar.consolidateSonarUsers()
  await ciadmin.updateSonarUserGroups()
}

export async function updateGitlabUsers() {
  await ciadmin.updateGitlabUsers()
}

export async function updateElasticUsers() {
  await ciadmin.updateElasticUsers()
}

export async function activateNewElasticUsers() {
  await kibana.activateNewUsers()
}

export async function configureNexusRepositories() {
  await ciadmin.configureNexusRepositories()
}

export async function configureNexusAuth() {
  await ciadmin.configureNexusAuth()
}

export async function configureNexusProxy() {
  await ciadmin.configureNexusProxy()
}

export async function configureNexusTruststore() {
  await ciadmin.configureNexusTruststore()
}


const ciadmin = new CIAdmin()
const sonar = ciadmin.sonar
const gitlab = ciadmin.gitlab
const elastic = ciadmin.elastic
const kibana = ciadmin.kibana
const nexus = ciadmin.nexus