import {deleteUndefined, fail, rethrow, xrequest} from './utils'

const request = xrequest

describe('arrays', () => {

  test('first', () => {
    expect([1,2,3].first()).toBe(1)
  })

  test('last', () => {
    expect([1,2,3].last()).toBe(3)
  })

  test('forEachAsync', async () => {
    let result : number[] = []
    await [1,2,3].forEachAsync(async (x) => Promise.all([
      new Promise((resolve) => {
        result.push(x)
        return resolve()
      })
    ]))
    expect(result).toHaveLength(3)
  })

  test('deleteUndefined', () => {
    let x = {
      a: 1, b: 2, c: undefined
    }
    deleteUndefined(x)
    expect(Object.keys(x)).toHaveLength(2)
  })

  test('fail', () => {
    expect(() => fail('test')).toThrow(/test/)
  })

  test('rethrow', () => {
    expect(() => rethrow(new Error('test'))).toThrow(/test/)
  })
})