import extend from "extend";
import {cfg} from "./cfg";
import {rethrow, xrequest} from "./utils";
import {CoreV1Api} from "@kubernetes/client-node";
import {Kibana} from "./kibana";
import {GitlabUser} from "./gitlab";

export interface EUser {
  username?: string
  full_name?: string
  email?: string
  roles?: string[]
  enabled?: boolean
  metadata?: any
}

export interface ERole {
  name?: string
  cluster?: string[]
  indices?: [{
    names: string[]
    privileges: string[]
  }]
  applications?: [{
    application: string
    privileges?: string[]
    resources?: string[]
  }]
}

export interface ELifecyclePolicy {
  name: string // synthetic, not in data
}

export interface EIndexTemplate {
  name: string,
}

export class Elastic {

  /// utils ///

  generatePassword() : string {
    return Math.random().toString(36).slice(-8)
  }

  /// users ///

  async createUser(username, data?:EUser) : Promise<EUser> {
    await xrequest(post(`_security/user/${username}`, extend(true, {
      //email:
      //full_name:
      password: this.generatePassword(),
      roles: [],
      metadata: {
        creator: 'ciadmin',
      },
    }, data ?? {})))
    return this.getUser(username)
  }

  async updateUser(u:EUser) {
    if (!u.roles) {
      // API will complain about missing roles, and empty [] would reset user roles; retrieve and use current roles
      u.roles = await this.getUser(u.username).then(x => x.roles)
    }
    return await xrequest(put(`_security/user/${u.username}`, u))
  }

  async updateUserPassword(username, password) {
    return await xrequest(post(`_security/user/${username}/_password`, {
      password: password,
    }))
  }

  async getUser(username) : Promise<EUser> {
    return await xrequest(get(`_security/user/${username}`))
      .then(x => x[username])
      .catch(e => e.statusCode == 404 ? undefined : rethrow(e))
  }

  async makeUser(username, data?:EUser) : Promise<EUser> {
    return (
      await this.getUser(username)
      ||
      await this.createUser(username, data)
    )
  }

  async makeUserFromGitlab(u:GitlabUser) {
    return await this.makeUser(u.username, {
      full_name: u.name,
      email: u.email,
      metadata: {
        source: 'gitlab',
        identities: u.identities,
      },
    })
  }

  async deleteUser(username) {
    await xrequest(del(`_security/user/${username}`))
  }

  async listUsers() : Promise<EUser[]> {
    return await xrequest(get(`_security/user`))
      .then(x => Object.keys(x).map((k) => x[k]))
  }

  /// roles ///

  async createRole(name) : Promise<ERole> {
    return await xrequest(post(`_security/role/${name}`, {}))
  }

  async getRole(name) : Promise<ERole> {
    return await xrequest(get(`_security/role/${name}`))
      .then(x => extend(<ERole>{ name: name}, x[name]))
      .catch(err => err.statusCode == 404 ? undefined : rethrow(err))
  }

  async listRoles() : Promise<ERole[]> {
    let list : ERole[] = []
    await xrequest(get(`_security/role`))
      .then(resp => Object.keys(resp)
        .map(k => extend(<ERole>{ name: k }, resp[k]))
        .forEach(r => list.push(r)))
    return list
  }

  async deleteRole(name) : Promise<boolean> {
    return await xrequest(del(`_security/role/${name}`))
      .then(() => true)
      .catch(err => err.statusCode == 404 ? false : rethrow(err))
  }

  /// privileges ///

  /// role mappings ///

  /// index templates ///

  async createIndexTemplate(name) {
    return await xrequest(put(`_index_template/${name}`, {
      index_patterns: [
        `${name}-*`
      ],
      template: {
        settings: {
          index: {
            lifecycle: {
              name: name,
              rollover_alias: name,
            },
            codec: "best_compression",
            refresh_interval: "5s",
            number_of_replicas: 0,
          },
        },
        mappings: {
          _data_stream_timestamp: {
            enabled: false,
          },
          dynamic_templates: [
            {
              numerics_in_fields: {
                match_pattern: "regex",
                path_match: "\"fields\.[\d+]$\"",
                mapping: {
                  norms: false,
                  index: true,
                  type: "text",
                },
              },
            },
            {
              string_fields: {
                mapping: {
                  norms: false,
                  index: true,
                  type: "text",
                  fields: {
                    raw: {
                      ignore_above: 256,
                      index: true,
                      type: "keyword",
                    },
                  },
                },
                match_mapping_type: "string",
                match: "*",
              },
            }
          ],
          properties: {
            message: {
              index: true,
              type: "text",
            },
            exceptions : {
              type: "nested",
              properties: {
                ExceptionMessage: {
                  type: "object",
                  properties: {
                    MemberType: {
                      type: "integer"
                    }
                  }
                },
                StackTraceString: {
                  index: true,
                  type: "text"
                },
                HResult: {
                  type: "integer"
                },
                RemoteStackTraceString: {
                  index: true,
                  type: "text"
                },
                RemoteStackIndex: {
                  type: "integer"
                },
                Depth: {
                  type: "integer"
                }
              }
            },
          },
        },
      },
      composed_of: []
    }))
  }

  async makeIndexTemplate(name) {
    await this.createIndexTemplate(name)
  }

  async getIndexTemplate(name) : Promise<EIndexTemplate> {
    return await xrequest(get(`_index_template/${name}`))
      .then(result => extend({name: name}, result.index_templates.find(x => x.name === name).index_template))
      .catch(err => err.statusCode == 404 ? undefined : rethrow(err))
  }

  async listIndexTemplates(): Promise<EIndexTemplate[]> {
    return await xrequest(get(`_index_template`)).then(result => {
      let list : EIndexTemplate[] = []
      result.index_templates
        .map(x => extend({name: x.name}, x.index_template))
        .forEach(x => list.push(x))
      return list
    })
  }

  async deleteIndexTemplate(name) : Promise<boolean> {
    return await xrequest(del(`_index_template/${name}`))
      .then(() => true)
      .catch(err => err.statusCode == 404 ? false : rethrow(err))
  }

  /// indices ///

  async createIndex(name, data?, alias?) {
    if (!data) data = {}
    if (alias) data.aliases = {
      [alias]: {
        //is_writable: true
      }
    }
    await xrequest(put(`${encodeURIComponent(name)}`, data))
  }

  async getIndex(name) {
    return await xrequest(get(`${name}`))
      .catch(err => err.statusCode == 404 ? undefined : rethrow(err))
  }

  async deleteIndex(name) {
    await xrequest(del(name))
  }

  async rolloverIndex(name) {
    await xrequest(post(`${name}/_rollover`, {}))
  }

  /// aliases ///

  async createIndexAlias(alias, index) {
    await xrequest(post(`_aliases`, {
      actions: [{
        add: {
          alias: alias,
          index: index,
        },
      }],
    }))
  }


  /// lifecycle policies ///

  async createLifecyclePolicy(name, rollover="1d", drop="5d") {
    return await xrequest(put(`_ilm/policy/${name}`, {
      policy: {
        phases: {
          hot: {
            actions: {
              rollover: {
                max_age: rollover
              }
            }
          },
          delete: {
            min_age: drop,
            actions: {
              delete: {}
            }
          }
        },
      }
    }))
  }

  async getLifecyclePolicy(name) : Promise<ELifecyclePolicy> {
    return await xrequest(get(`_ilm/policy/${name}`))
      .then(result => extend({name: name}, result[name]))
      .catch(err => err.statusCode == 404 ? undefined : rethrow(err))
  }

  async listLifecyclePolicies() : Promise<ELifecyclePolicy[]> {
    return await xrequest(get(`_ilm/policy`))
      .then(result => {
        let list : ELifecyclePolicy[] = []
        Object.keys(result)
          .map(k => extend({name: k}, result[k]))
          .forEach(x => list.push(x))
        return list
      })
  }

  async makeLifecyclePolicy(name, rollover="1d", drop="5d") {
    return await this.createLifecyclePolicy(name, rollover, drop)
  }

  async deleteLifecyclePolicy(name) : Promise<boolean> {
    return await xrequest(del(`_ilm/policy/${name}`))
      .then(() => true)
      .catch(err => err.statusCode == 404 ? false : rethrow(err))
  }

  /// custom ///

  async setupProject(p: {
    path: string
    space?: string
    username?: string
  }) {

    p.space = p.space ?? p.path.replace('/', '-')
    p.username = p.username ?? p.space
    await this.makeUser(p.username)
    await this.createLifecyclePolicy(p.space)
    await this.createIndexTemplate(p.space)

    let alias = p.space
    if (await this.getIndex(alias)) {
      return
    }

    await this.createIndex(`<${alias}-{now/d}-0>`, {}, alias)
  }

  async updateAppLoggingUser(
    k8s: CoreV1Api,
    namespace: string,
    secretName: string,
    usernameKey: string,
    passwordKey: string,
  ) {
    let kibana = new Kibana()
    const b64decode = (s) => Buffer.from(s, 'base64').toString('ascii')
    let secret = await k8s.readNamespacedSecret(secretName, namespace).catch(rethrow)
    let username = b64decode(secret.body.data![usernameKey])
    let password = b64decode(secret.body.data![passwordKey])
    let u = await this.makeUser(username)
    u.roles!.push(kibana.writersRoleName(namespace))
    await this.updateUser(u)
    await this.updateUserPassword(username, password)
  }



}

function req(uri, method, data?, options?) {
  let credentials = `${cfg.elastic.username}:${cfg.elastic.password}`;
  return extend(
    {
      url: `${cfg.elastic.url}/${uri}`,
      headers: {
        'Authorization': `Basic ${Buffer.from(credentials).toString('base64')}`,
        'Content-Type': 'application/json',
        'Accept': 'application/json',
      },
      method: method,
      resolveWithFullResponse: false,
      json: true,
      qs: (method === 'GET') ? data : {},
      query: (method === 'GET') ? data : {},
      body: (method.match(/PUT|POST/)) ? data : undefined,
    },
    options || {}
  );
}

function get(uri, data?) {
  return req(uri, 'GET', data);
}

function put(uri, data) {
  return req(uri, 'PUT', data);
}

function post(uri, data) {
  return req(uri, 'POST', data);
}

function del(uri) {
  return req(uri, 'DELETE')
}

