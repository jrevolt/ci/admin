import extend from "extend";
import {cfg} from "./cfg";
import {deleteNulls, fail, ignore, rethrow, xrequest} from "./utils";
import fetch from 'node-fetch'
import {HttpProxyAgent} from "http-proxy-agent";
import request from 'request-promise';
import {createS3Client} from "./s3";

export enum NRepoFormat {
  apt = 'apt',
  bower = 'bower',
  cocoapods = 'cocoapods',
  conan = 'conan',
  conda = 'conda',
  docker = 'docker',
  gitlfs = 'gitlfs',
  go = 'go',
  helm = 'helm',
  maven = 'maven',
  npm = 'npm',
  nuget = 'nuget',
  p2 = 'p2',
  pypi = 'pypi',
  r = 'r',
  raw = 'raw',
  rubygems = 'rubygems',
  yum = 'yum',
}

export enum NRepoType {
  group = 'group',
  proxy = 'proxy',
  hosted = 'hosted',
}

export class NRepoStorage {
  blobStoreName: string
  strictContentTypeValidation: boolean = true
  writePolicy: NRepoWritePolicy = NRepoWritePolicy.allow

  constructor(src: Partial<NRepoStorage>) {
    Object.assign(this, src)
  }
}

export enum NRepoWritePolicy {
  allow = 'allow',
  deny = 'deny',
  allow_once = 'allow_once',
}

export abstract class NRepo {
  name: string
  format: NRepoFormat
  type: NRepoType
  online: boolean = true
  storage: NRepoStorage
  docker: {
    v1Enabled: boolean
    forceBasicAuth: boolean
    httpPort?: number
    httpsPort?: number
  }
  maven: {
    versionPolicy?: string
    layoutPolicy?: string
    contentDisposition?: string
  }

  protected constructor(src:Partial<NRepo>) {
    extend(true, this,
      src.format == NRepoFormat.docker ? <NRepo>{
        docker: { v1Enabled: false, forceBasicAuth: false }
      } : {},
      src.format == NRepoFormat.maven ? <NRepo>{
        maven: { versionPolicy: "MIXED", layoutPolicy: "STRICT", contentDisposition: "ATTACHMENT" }
      } : {},
      src
    )
  }
}

export class NProxyRepo extends NRepo {
  type = NRepoType.proxy
  routingRule?: string
  proxy: {
    remoteUrl: string
    contentMaxAge: number
    metadataMaxAge: number
  }
  negativeCache: {
    enabled: boolean
    timeToLive: number
  }
  httpClient: {
    blocked: boolean
    autoBlock: boolean
    connection: {
      retries?: number
      timeout?: number
      useTrustStore?: boolean
      proxy?: {
        host: string
        port: number
        authentication?: {
          type: string
          username: string
          password: string
        }
      }
    }
    authentication: {
      type: string
      username: string
      password: string
    }
  }
  nugetProxy: {
    nugetVersion: string
    queryCacheItemMaxAge: number
  }
  dockerProxy: {
    indexType : string
    indexUrl ?: string
  }

  constructor(src:Partial<NProxyRepo>) {
    super(src)
    src?.format || fail('repo format?')
    extend(true, this,
      <NProxyRepo>{
        proxy: {
          contentMaxAge: 60,
          metadataMaxAge: 60,
        },
        negativeCache: {
          enabled: true,
          timeToLive: 1440,
        },
        httpClient: {
          blocked: false,
          autoBlock: true,
          connection: {
            useTrustStore: false
          },
        },
      },
      src.format == NRepoFormat.nuget ? <NProxyRepo>{
        nugetProxy: {
          nugetVersion: "V3",
          queryCacheItemMaxAge: 60,
        }
      } : {},
      src.format == NRepoFormat.docker ? <NProxyRepo> {
        dockerProxy: { indexType: 'REGISTRY' }
      } : {},
      src.format == NRepoFormat.maven ? <NRepo>{
        maven: { versionPolicy: "MIXED", layoutPolicy: "STRICT", contentDisposition: "ATTACHMENT" }
      } : {},
      src,
      <NProxyRepo>{
        type: NRepoType.proxy,
        routingRule: src?.routingRule || src['routingRuleName'],
      })
  }
}

export class NHostedRepo extends NRepo {
  type = NRepoType.hosted
  cleanup: {
    policyNames: string[]
  }

  constructor(src: Partial<NHostedRepo>) {
    super(src);
    extend(true, this,
      src.format == NRepoFormat.docker ? <NHostedRepo> {
        docker: {
          v1Enabled: false,
          forceBasicAuth: true,
        }
      } : {},
      src
    );
  }
}

export class NGroupRepo extends NRepo {
  type = NRepoType.group
  group: {
    memberNames: string[]
  }

  constructor(src:Partial<NGroupRepo>) {
    super(src);
    extend(true, this, src, <NGroupRepo>{
      type: NRepoType.group
    })
  }
}

export enum NBlobStoreType {
  File = 'File',
  S3 = 'S3',
}

export abstract class NBlobStore {
  name: string
  type: NBlobStoreType
}

export class NS3BlobStore extends NBlobStore {
  type = NBlobStoreType.S3
  bucketConfiguration: {
    bucket: {
      name: string
      region?: string
      expiration?: number
    }
    bucketSecurity: {
      accessKeyId: string
      secretAccessKey: string
    }
    advancedBucketConnection: {
      endpoint: string
      forcePathStyle?: boolean
    }
  }

  constructor(src:Partial<NS3BlobStore>) {
    super()
    extend(true, this,
      <NS3BlobStore>{ // defaults
        bucketConfiguration: {
          bucket: {
            region: 'us-east-1',
            expiration: 3,
          },
          advancedBucketConnection: {
            forcePathStyle: true,
          }
        }
      },
      src,
      <NS3BlobStore>{ // forced overrides
        type: NBlobStoreType.S3
      })
  }

  static create(name:string, url:string, bucket:string, accessKey:string, secretKey:string) : NS3BlobStore {
    return new NS3BlobStore({
      name: name,
      bucketConfiguration: {
        bucket: {
          name: bucket,
        },
        bucketSecurity: {
          accessKeyId: accessKey,
          secretAccessKey: secretKey,
        },
        advancedBucketConnection: {
          endpoint: url,
        }
      }
    })
  }
}

export class NFileBlobStore extends NBlobStore {

  path: string

  constructor(src:Partial<NFileBlobStore>) {
    super()
    extend(true, this,
      src,
      <NFileBlobStore>{ // forced overrides
        type: NBlobStoreType.File
      })
  }

  static create(name: string, path: string) : NFileBlobStore {
    return new NFileBlobStore({
      name: name,
      path: path,
    })
  }
}

export enum NRoutingMode { ALLOW='ALLOW', BLOCK='BLOCK' }

export class NRoutingRule {
  name: string
  description: string = ""
  mode: NRoutingMode = NRoutingMode.ALLOW
  matchers: string[] = [ ".*" ]

  constructor(src:Partial<NRoutingRule>) {
    extend(true, this, src)
  }
}


class NUser {
  userId: string
  firstName: string
  lastName: string
  emailAddress: string
  source: string
  status: string
  readOnly: boolean
  roles: string[]
  externalRoles: string[]
}

export class NLdapServer {
  id ?: string
  name: string
  protocol = "ldap"
  useTrustStore = true
  host?: string
  port?: number
  searchBase: string //dc=example,dc=com
  authScheme?: string //NONE
  authRealm?: string //example.com
  authUsername?: string
  authPassword?: string
  connectionTimeoutSeconds = 1
  connectionRetryDelaySeconds = 0
  maxIncidentsCount = 0
  userBaseDn?: string //ou=people
  userSubtree = true
  userObjectClass = "inetOrgPerson"
  userLdapFilter?: string //(|(mail=*@example.com)(uid=dom*))
  userIdAttribute = "uid"
  userRealNameAttribute = "cn"
  userEmailAddressAttribute = "mail"
  //userPasswordAttribute?: string
  ldapGroupsAsRoles = true
  groupType = "dynamic"
  //groupBaseDn?: string //ou=Group
  //groupSubtree = true
  // groupObjectClass = "group"
  // groupIdAttribute = "cn"
  // groupMemberAttribute = "memberUid"
  //groupMemberFormat?: uid=${username},ou=people,dc=example,dc=com
  userMemberOfAttribute = "memberOf"

  constructor(src:Partial<NLdapServer>) {
    extend(true, this, src)
  }

}

export class NCertificate {
  id?: string
  fingerprint?: string
  issuedOn?: Date
  expiresOn?: Date
  issuerCommonName?: string
  issuerOrganization?: string
  issuerOrganizationalUnit?: string
  subjectCommonName?: string
  subjectOrganization?: string
  subjectOrganizationalUnit?: string
  pem?: string
}

export class NScript {
  name: string
  type: string
  content: string
  constructor(src:Partial<NScript>) {
    extend(true, this, src)
  }

}

export class Nexus {

  /// blob stores ///

  async createBlobStore(bs:NBlobStore) {
    await xrequest(post(`v1/blobstores/${bs.type.toLowerCase()}`, bs))
  }

  async getBlobStore(name:string, type?:NBlobStoreType) : Promise<NBlobStore|undefined> {
    if (!type) {
      return await xrequest(get(`v1/blobstores`))
        .then((all:any[]) => all.filter(x => x.name == name).first())
        .then(x =>
          !x ? undefined :
          x?.type == NBlobStoreType.S3 ? new NS3BlobStore(x) :
          x?.type == NBlobStoreType.File ? new NFileBlobStore(x) :
          fail(`unsupported type: ${x?.type}`)
        )
    } else {
      return await xrequest(get(`v1/blobstores/${type.toLowerCase()}/${name}`))
        .then(x => new NS3BlobStore(x))
        .catch(err => /doesn't exist|unable to find/i.test(err.message) ? undefined : rethrow(err))
    }
  }

  async updateBlobStore(bs:NBlobStore) {
    await xrequest(put(`v1/blobstores/${bs.type.toLowerCase()}/${bs.name}`, bs))
  }

  async makeBlobStore(bs:NBlobStore) {
    let found = await this.getBlobStore(bs.name, bs.type)
    return found ? await this.updateBlobStore(bs) : await this.createBlobStore(bs)
  }

  async deleteBlobStore(name:string) {
    await xrequest(del(`v1/blobstores/${name}`))
  }

  async listBlobStores() : Promise<NBlobStore[]> {
    return (await xrequest(get(`v1/blobstores`))).map(x => {
      (x.type == NBlobStoreType.S3) ? new NS3BlobStore(x) :
      (x.type == NBlobStoreType.File) ? new NFileBlobStore(x) :
      fail(`unsupported type: ${x.type}`)
    })
  }

  /// repositories ///


  async getRepositories() : Promise<NRepo[]> {
    return await xrequest(get(`repositories`))
  }

  async createRepository(r:NRepo) : Promise<NRepo> {
    let bs = await this.getBlobStore(r.storage.blobStoreName)
      || fail(`No such blobstore: ${r.storage.blobStoreName}`)
    await xrequest(post(`v1/repositories/${r.format}/${r.type}`, r))
    return await this.getRepository(r.name).then(x => x || r)
  }

  async getRepository(name: string) : Promise<NRepo|undefined> {
    let all = await xrequest(get(`v1/repositories`));
    return all
      .filter(x => x.name == name)
      .map(x =>
        x.type == 'proxy' ? new NProxyRepo(x) :
        x.type == 'hosted' ? new NHostedRepo(x) :
        x.type == 'group' ? new NGroupRepo(x) :
        fail(`unsupported type: ${x.type}`)
      )
      .first()
  }

  async updateRepository(r:NRepo) : Promise<NRepo> {
    // qdh
    if (String(r.format) == "maven2") r.format = NRepoFormat.maven
    await xrequest(put(`v1/repositories/${r.format}/${r.type}/${r.name}`, r))
    return await this.getRepository(r.name).then(x => x || r);
  }

  async makeRepository(r: NRepo) : Promise<NRepo> {
    let found = await this.getRepository(r.name)
    return found
      ? await this.updateRepository(extend(true, r, deleteNulls(found)))
      : await this.createRepository(r)
  }

  async deleteRepository(name:string) {
    await xrequest(del(`v1/repositories/${name}`))
  }

  async listRepositories() : Promise<NRepo[]> {
    let all = await xrequest(get(`v1/repositories`))
    return all.map(x =>
      x.type == 'proxy' ? new NProxyRepo(x) :
      x.type == 'hosted' ? new NHostedRepo(x) :
      x.type == 'group' ? new NGroupRepo(x) :
      fail(`unsupported type: ${x.type}`)
    )
  }

  /// routing rules ///

  async createRoutingRule(r:NRoutingRule) : Promise<NRoutingRule> {
    await xrequest(post(`v1/routing-rules`, r))
    return await this.getRoutingRule(r.name) || fail()
  }

  async updateRoutingRule(r: NRoutingRule) {
    await xrequest(put(`v1/routing-rules/${r.name}`, r))
    return await this.getRoutingRule(r.name) || fail()
  }

  async deleteRoutingRule(name:string) {
    await xrequest(del(`v1/routing-rules/${name}`))
  }

  async getRoutingRule(name:string) : Promise<NRoutingRule|undefined> {
    return await xrequest(get(`v1/routing-rules/${name}`))
      .then(x => x ? new NRoutingRule(x) : undefined)
      .catch(err => /did.*n.t find.*rule/i.test(err.message) ? undefined : rethrow(err))
  }

  async makeRoutingRule(r: NRoutingRule) : Promise<NRoutingRule> {
    let found = await this.getRoutingRule(r.name);
    return found
      ? await this.updateRoutingRule(extend(true, found, r))
      : await this.createRoutingRule(r);
  }

  async listRoutingRules() {
    let all = await xrequest(get(`v1/routing-rules`))
    return all.map(x => new NRoutingRule(x))
  }


  /// users ///

  async listUsers() {
    return await xrequest(get(`beta/security/users`))
  }

  /// ldap

  async listLdapServers() {}
  async createLdapServer(ldap: NLdapServer) {
    return await xrequest(post(`v1/security/ldap`, ldap))
  }
  async getLdapServer(name: string) : Promise<NLdapServer> {
    return await xrequest(get(`v1/security/ldap/${name}`)).catch(err => /404/.test(err.message) ? undefined : rethrow(err))
  }
  async updateLdapServer(ldap: NLdapServer) {
    return await xrequest(put(`v1/security/ldap/${ldap.name}`, ldap))
  }
  async deleteLdapServer(name: string) {
    await xrequest(del(`v1/security/ldap/${name}`))
  }
  async makeLdapServer(ldap: NLdapServer) {
    let found = await this.getLdapServer(ldap.name)
    if (found) ldap.id = found.id
    return found
      ? await this.updateLdapServer(ldap)
      : await this.createLdapServer(ldap)
  }

  /// ssl

  async getCertificates(): Promise<NCertificate[]> {
    return await xrequest(get(`v1/security/truststore`))
  }

  async addCertificate(pem:string) {
    await xrequest(req(`v1/security/ssl/truststore`, 'POST', pem, {json: false, resolveWithFullResponse: true}))
      .catch(err => /409/.test(err.message) ? undefined : rethrow(err))
  }
  async removeCertificate(id) {
    await xrequest(del(`v1/security/ssl/truststore/${id}`))
  }

  /// scripts

  async getScripts() {
    return await xrequest(get(`v1/script`)) as NScript[]
  }
  async addScript(script:NScript) {
    await xrequest(post(`v1/script`, script))
  }
  async updateScript(script:NScript) {
    await xrequest(put(`v1/script/${script.name}`, script))
  }
  async deleteScript(name) {
    await xrequest(del(`v1/script/${name}`))
  }
  async makeScript(script:NScript) {
    await this.deleteScript(script.name).catch(ignore)
    await this.addScript(script)
  }
  async runScript(name:string) {
    await xrequest(post(`v1/script/${name}/run`, {}))
  }

  /// proxy

  async configureHttpProxy(host, port, user, pass, reset = true, http = true, https = true) {
    let auth = !!(user && pass)
    let script = new NScript({
      name: `configureHttpProxy${new Date().getTime()}`,
      type: 'groovy',
      content: `
      if (${reset}) {
        core.removeHTTPProxy();
        core.removeHTTPSProxy();
      }
      if (${auth}) {
        if (${http}) { core.httpProxyWithBasicAuth("${host}", ${port}, "${user}", "${pass}"); }
        if (${https}) { core.httpsProxyWithBasicAuth("${host}", ${port}, "${user}", "${pass}"); }
      } else {
        if (${http})  { core.httpProxy("${host}", ${port}); }
        if (${https}) { core.httpsProxy("${host}", ${port}); }
      }
      `,
    })
    try {
      await this.makeScript(script)
      await this.runScript(script.name)
    } finally {
      await this.deleteScript(script.name).catch(ignore)
    }
  }
  async configureNonProxyHosts(items:string[]) {
    let script = new NScript({
      name: `configureNonProxyHosts${new Date().getTime()}`,
      type: 'groovy',
      content: `core.nonProxyHosts(${items.map(x => `"${x}"`).join(",")})`,
    })
    try {
      await this.makeScript(script)
      await this.runScript(script.name)
    } finally {
      await this.deleteScript(script.name).catch(ignore)
    }
  }



}


///

function req(uri: string, method: string, data: any = {}, options: any = {}) {
  return extend(
    {
      url: `${cfg.nexus.url}/service/rest/${uri}`,
      headers: {
        'Authorization': `Basic ${Buffer.from(`${cfg.nexus.username}:${cfg.nexus.password}`).toString(
          'base64')}`,
        'Content-Type': 'application/json',
        'Accept': 'application/json',
      },
      method: method,
      qs: (method.match(/GET|DELETE/)) ? data : {},
      body: (method.match(/POST|PUT/)) ? data : {},
      resolveWithFullResponse: false,
      json: true,
    },
    options || {}
  );
}

function get(uri: string, data: any = {}) {
  return req(uri, 'GET', data);
}

function xget(uri: string, data: any = {}) {
  return req(uri, 'GET', data, {resolveWithFullResponse: true});
}

function post(uri: string, data: any) {
  return req(uri, 'POST', data);
}

function put(uri: string, data: any) {
  return req(uri, 'PUT', data);
}

function del(uri: string, data: any = {}) {
  return req(uri, 'DELETE', data, {resolveWithFullResponse: true});
}

