import {cfg} from "./cfg";
import request from 'request-promise';
import Bottleneck from "bottleneck";
import TraceError from 'trace-error';
import {log} from "./log";
import extend from "extend";

declare global {
  interface String {
    sonarize() : string
    truncate(max: number): string
    trimToUndefined(filter?:RegExp) : string|undefined
    trimMatch(filter:RegExp) : string
    trimTrailingSlashes() : string
  }
  interface Array<T> {
    forEachAsync(action: (item:T)=>Promise<any>) : Promise<any>
    first() : T;
    last() : T;
    contains(x:T): boolean;
  }
  interface Map<K,V> {
    forEachAsync(action: (item: V) => void): void;
  }
}

Array.prototype.first = function () {
  return this.length > 0 ? this[0] : undefined
};

Array.prototype.last = function () {
  return this.length > 0 ? this[this.length-1] : undefined
};

Array.prototype.forEachAsync = async function (cb : (x:any) => Promise<void>) {
  await Promise.all(this.map(async (x) => cb(x)));
};

Array.prototype.contains = function (x) {
  return this.indexOf(x) != -1
}


String.prototype.truncate = function (max) {
  return this.substring(0, Math.min(this.length, max));
};

String.prototype.trimToUndefined = function (filter?:RegExp) {
  let s = this.trim()
  if (filter) s = s.replace(filter, '')
  return s.length > 0 ? s : undefined
}

String.prototype.trimMatch = function (filter:RegExp) : string {
  return this.replace(filter, '')
}

String.prototype.trimTrailingSlashes = function () : string {
  return this.replace(/\/*$/, '')
}


export function deleteUndefined(o : any) : any {
  for (let k in o) if (o.hasOwnProperty(k) && o[k] == undefined) delete o[k]; // drop undefined
  return o;
}

export function deleteNulls(o : any) : any {
  for (let k in o) {
    if (!o.hasOwnProperty(k)) continue;
    if (o[k] == null) delete o[k];
    if (typeof(o[k]) == "object") deleteNulls(o[k]);
  }
  return o;
}

export async function sleep(millis:number) {
  await new Promise((_) => setTimeout(_, millis))
}

export async function retry(max, pause, fn) {
  for (let i=1; i<=max; i++) {
    let x = await fn()
    if (x) return x
    await sleep(pause)
  }
  return undefined
}

export function fail<T>(msg ?: string) : never {
  throw Error(msg)
}

export function rethrow(err) : never {
  throw new TraceError(err.message, err);
}

//export function wrap(err: Error) : never {}

export function ignore(_:Error) {} //NOSONAR

export function report(err) {
  if (err) {
    let e = new TraceError(err.message, err);
    log.debug('%s', e.stack)
    log.error('Error (reported, ignored): %s', e.message);
    reportedErrors++;
  }
}

let limiter : Bottleneck;

async function dbgrequest(options) {
  let opts = extend(true, {}, cfg.ciadmin.request, options);
  log.debug('%s %s %s %s',
    opts.method.padEnd(6),
    opts.url,
    JSON.stringify(opts.qs || opts.query || {}),
    JSON.stringify(opts.form || opts.formData || opts.body || {}).truncate(cfg.debugTruncateBody),
  );
  return await request(opts).catch(err => {
    err.message += `\nHTTP/${err.statusCode} ${err.options.method} ${err.options.url}`
    err.stack = new TraceError(err.message, err).stack
    throw err
  })
}

export const xrequest = (options) => {
  let enableLimiter = (cfg.ciadmin.bottleneck.maxConcurrent ?? 0) > 0;
  limiter = limiter || new Bottleneck(cfg.ciadmin.bottleneck)
  let req = enableLimiter ? limiter.wrap(dbgrequest) : dbgrequest
  return req(options)
}

export function parseUrl(url) : URL {
  return new URL(url)
}

declare global {
  let TraceError: TraceError;
}

export let reportedErrors = 0;

