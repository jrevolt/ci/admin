import './testutils'
import {Sonar, SonarProjectRef, SonarProjectVisibility} from "./sonar";
import {cfg, Group, Project, Repository} from "./cfg";
import {ignore, report} from "./utils";
import {Gitlab} from "./gitlab";
import {PathRef} from "./ciadmin";

describe('sonar', () => {

  const sonar = new Sonar()
  const gitlab = new Gitlab()

  let id, group, subgroup, project, repo, branch, lang;
  let g : Group, p : Project, r : Repository
  let gname: string, sgname: string, pname: string, rname: string, bname: string
  let sproject : SonarProjectRef
  let user
  let pathref: PathRef

  beforeEach(async () => {
    id = new Date().getTime().toString().replace(/.*(\d{5})$/, '$1')
    group = `group-${id}`
    subgroup = `subgroup-${id}`
    project = `project-${id}`
    repo = `repo-${id}`
    branch = `master`
    lang = 'cs'

    g = {
      name: gname = group,
      groups: [{
        name: sgname = subgroup,
        projects: [ p = {
          name: pname = project,
          repositories: [ r = {
            name: rname = repo,
            options: { languages: [ lang ] }
          } as Repository]
        } as Project]
      }],
    }
    cfg.ci.sonar.defaultQualityProfileName = `dflt-${id}`
    cfg.groups.push(g)
    cfg.resolveReferences()
    bname = branch

    user = `test-${id}`

    pathref = PathRef.parse(`${group}/${subgroup}/${project}/${repo}:${branch}`)
  })

  afterEach(async () => {
    await Promise.all([
      sonar.deleteProject(pathref),
      sonar.deleteQualityProfile(cfg.ci.sonar.defaultQualityProfileName, lang),
      sonar.deleteQualityGate(pathref),
      sonar.deletePermissionTemplate(pathref),
      sonar.deleteUserGroups(pathref, true),
      sonar.deleteUser(user),
    ]).catch(ignore)
  })

  xtest('cleanup', async () => {
    let re = new RegExp('^(group|jrevolt|test)')
    await Promise.all([
      sonar.listProjects().then(async (all) =>
        all.filter(p => re.test(p.key))
          .forEachAsync(async (p) => await sonar.deleteProjectByKey(p.key).catch(ignore))),
      sonar.listLanguages().then(async (languages) =>
        await languages.forEachAsync(async (l) =>
          sonar.listQualityProfiles(l).then(async (all) =>
            all.filter(p => re.test(p.name))
              .forEachAsync(async (p) => sonar.deleteQualityProfile(p.name, p.language).catch(ignore))))),
      sonar.listQualityGates().then(async (all) =>
        all.filter(p => re.test(p.name))
          .forEachAsync(async (p) => sonar.deleteQualityGateById(p.id).catch(ignore))),
      sonar.listUserGroups().then(async (all) =>
        all.filter(x => re.test(x.name))
          .forEachAsync(g => sonar.deleteUserGroup(g.name).catch(ignore))),
      sonar.listPermissionTemplates().then(async (all) =>
        all.filter(x => re.test(x.name))
          .forEachAsync(x => sonar.deletePermissionTemplateByName(x.name).catch(ignore)))

    ])
  })

  describe('identifiers', () => {

    test('id: project', () => {
      expect(sonar.projectName(pathref)).toBe(`${gname}:${sgname}:${pname}:${rname}:${bname}`)
      expect(sonar.projectName(pathref.asRepoRef())).toBe(`${gname}:${sgname}:${pname}:${rname}`)
      expect(sonar.projectName(pathref.asProjectRef())).toBe(`${gname}:${sgname}:${pname}`)
      expect(sonar.projectName(pathref.asGroupRef())).toBe(`${gname}:${sgname}`)
      expect(sonar.projectName(pathref.asGroupRef().getParent())).toBe(`${gname}`)
    })

    test('id: permission template', () => {
      expect(sonar.permissionTemplateName(pathref)).toBe(`${gname}:${sgname}:${pname}:${rname}`)
      expect(sonar.permissionTemplateName(pathref.asRepoRef())).toBe(`${gname}:${sgname}:${pname}:${rname}`)
      expect(sonar.permissionTemplateName(pathref.asProjectRef())).toBe(`${gname}:${sgname}:${pname}`)
      expect(sonar.permissionTemplateName(pathref.asGroupRef())).toBe(`${gname}:${sgname}`)
      expect(sonar.permissionTemplateName(pathref.asGroupRef().getParent())).toBe(`${gname}`)
    })

    test('id: owners group', () => {
      expect(sonar.ownersGroupName(pathref.asRepoRef())).toBe(`${gname}:${sgname}:${pname}:${rname}:${Sonar.ownersGroupSuffix}`)
      expect(sonar.ownersGroupName(pathref.asProjectRef())).toBe(`${gname}:${sgname}:${pname}:${Sonar.ownersGroupSuffix}`)
      expect(sonar.ownersGroupName(pathref.asGroupRef())).toBe(`${gname}:${sgname}:${Sonar.ownersGroupSuffix}`)
      expect(sonar.ownersGroupName(pathref.asGroupRef().getParent())).toBe(`${gname}:${Sonar.ownersGroupSuffix}`)
    })

    test('id: members group', () => {
      expect(sonar.memberGroupName(pathref.asRepoRef())).toBe(`${gname}:${sgname}:${pname}:${rname}:${Sonar.membersGroupSuffix}`)
      expect(sonar.memberGroupName(pathref.asProjectRef())).toBe(`${gname}:${sgname}:${pname}:${Sonar.membersGroupSuffix}`)
      expect(sonar.memberGroupName(pathref.asGroupRef())).toBe(`${gname}:${sgname}:${Sonar.membersGroupSuffix}`)
      expect(sonar.memberGroupName(pathref.asGroupRef().getParent())).toBe(`${gname}:${Sonar.membersGroupSuffix}`)
    })

    test('getParentName()', () => {
      expect(sonar.getParentName('g:p:r')).toBe('g:p')
      expect(sonar.getParentName('g:p')).toBe('g')
      expect(sonar.getParentName('g')).toBe('')
    })
  })


  describe('projects', () => {
    test('create', async () => {
      await expect(sonar.createProject(pathref)).resolves.toHaveProperty('key')
      await expect(sonar.createProject(pathref)).rejects.toThrow(/already exists/)
    })

    test('get', async () => {
      await expect(sonar.getProject(pathref)).resolves.toBeUndefined()
      await sonar.createProject(pathref)
      await expect(sonar.getProject(pathref)).resolves.toHaveProperty('key')
    })

    test('delete', async () => {
      await expect(sonar.deleteProject(pathref)).rejects.toThrow(/not found/)
      await sonar.createProject(pathref)
      await expect(sonar.deleteProject(pathref)).resolves.toBeUndefined()
    })

    test('make', async () => {
      await expect(sonar.makeProject(pathref)).resolves.toHaveProperty('key')
      await expect(sonar.makeProject(pathref)).resolves.toHaveProperty('key')
    })

    test('make concurrent', async () => {
      await Promise.all([
        expect(sonar.makeProject(pathref)).resolves.toHaveProperty('key'),
        expect(sonar.makeProject(pathref)).resolves.toHaveProperty('key'),
        expect(sonar.makeProject(pathref)).resolves.toHaveProperty('key'),
        expect(sonar.makeProject(pathref)).resolves.toHaveProperty('key'),
      ])
    })

    test('list', async () => {
      let x = await sonar.listProjects();
    })

    test('update visibility', async () => {
      let x = await sonar.makeProject(pathref)
      await sonar.updateProjectVisibility(pathref, SonarProjectVisibility.public)
      return
    })
  })

  describe('users', () => {
    test('create', async () => {
      await expect(sonar.createUser(user)).resolves.toBeDefined()
    })
    test('get', async () => {
      await expect(sonar.getUser(user)).resolves.toBeUndefined()
      await sonar.createUser(user)
      await expect(sonar.getUser(user)).resolves.toHaveProperty('login', user)
    })
    test('delete', async () => {
      await sonar.createUser(user)
      await expect(sonar.deleteUser(user)).resolves.toBeUndefined()
    })
    test('list', async () => {
      await sonar.createUser(user)
      await expect(sonar.listUsers()).resolves.toBeNonEmptyArray()
    })
  })

  describe('tokens', () => {
    beforeEach(async () => {
      await sonar.makeUser(user)
      await sonar.generateUserToken(user, 'test')
    })
    test('generate', async () => {
      await expect(sonar.generateUserToken(user, 'test2')).resolves.toBeDefined()
    })
    test('search', async () => {
      await expect(sonar.listUserTokens(user)).resolves.toHaveLength(1)
    })
    test('has', async () => {
      await expect(sonar.hasToken(user, 'test')).resolves.toBeTruthy()
    })
    test('revoke', async () => {
      await expect(sonar.revokeUserToken(user, 'test')).resolves.toBeUndefined()
    })
    test('revoke test-qdh tokens', async () => {
      await expect(sonar.revokeUserToken('test-qdh', 'test-qdh')).resolves.toBeUndefined()
    })
  })

  describe('user groups', () => {

    xtest('cleanup', async () => {
      sonar.listUserGroups().then(async (groups) => groups
        .filter(x => /^(dflt|dlft|group).*/.test(x.name))
        .forEachAsync(async (x) => await sonar.deleteUserGroup(x.name).catch(report))
      )
    })

    test('create', async () => {
      await expect(sonar.createUserGroup(sonar.ownersGroupName(pathref))).resolves.toHaveProperty('name')
      await expect(sonar.createUserGroup(sonar.ownersGroupName(pathref))).rejects.toThrow(/already exists/)
    })

    test('get', async () => {
      await expect(sonar.getUserGroup(sonar.ownersGroupName(pathref))).resolves.toBeUndefined()
      await sonar.createUserGroup(sonar.ownersGroupName(pathref))
      await expect(sonar.getUserGroup(sonar.ownersGroupName(pathref))).resolves.toHaveProperty('name')
    })

    test('delete', async () => {
      let name = sonar.ownersGroupName(pathref)
      await expect(sonar.deleteUserGroup(name)).rejects.toThrow(/No group/)
      await sonar.createUserGroup(name)
      await expect(sonar.deleteUserGroup(name)).resolves.toBeUndefined()
    })

    test('make', async () => {
      let name = sonar.ownersGroupName(pathref)
      await expect(sonar.makeUserGroup(name)).resolves.toBeDefined()
      await expect(sonar.makeUserGroup(name)).resolves.toBeDefined()
    })

    // fixme concurrent
    xtest('make concurrent', async () => {
      let name = sonar.ownersGroupName(pathref)
      await Promise.all([
        expect(sonar.makeUserGroup(name)).resolves.toBeDefined(),
        expect(sonar.makeUserGroup(name)).resolves.toBeDefined(),
        expect(sonar.makeUserGroup(name)).resolves.toBeDefined(),
      ])
    })

    test('make all', async () => {
      await expect(sonar.makeUserGroups(pathref)).resolves.toBeUndefined()
      await expect(sonar.makeUserGroups(pathref)).resolves.toBeUndefined()
      await [Sonar.membersGroupSuffix, Sonar.ownersGroupSuffix].forEachAsync(async (x) => {
        await expect(sonar.getUserGroup(`${group}:${x}`)).resolves.toBeDefined()
        await expect(sonar.getUserGroup(`${group}:${subgroup}:${x}`)).resolves.toBeDefined()
        await expect(sonar.getUserGroup(`${group}:${subgroup}:${project}:${x}`)).resolves.toBeDefined()
        await expect(sonar.getUserGroup(`${group}:${subgroup}:${project}:${repo}:${x}`)).resolves.toBeDefined()
      })
    })

    describe('memberships', () => {

      let sgroup : string

      beforeEach(() => {
        sgroup = sonar.ownersGroupName(pathref.asGroupRef())
      })

      test('list', async () => {
        await expect(sonar.listGroupMembers(sgroup)).rejects.toThrow()

        await expect(sonar.makeUserGroup(sgroup)).resolves.toBeDefined()
        await expect(sonar.listGroupMembers(sgroup)).resolves.toHaveLength(0)

        await sonar.makeUser(user)
        await sonar.addGroupMember(user, sgroup)
        await expect(sonar.listGroupMembers(sgroup)).resolves.toBeNonEmptyArray()
      })

      test('add', async () => {
        await sonar.makeUserGroup(sgroup)
        await expect (sonar.addGroupMember(user, sgroup)).rejects.toThrow()
        await sonar.makeUser(user)
        await expect (sonar.addGroupMember(user, sgroup)).resolves.toBeUndefined()
      })

      test('remove', async () => {
        await expect (sonar.removeGroupMember(user, sgroup)).rejects.toThrow()
        await sonar.makeUserGroup(sgroup)
        await expect (sonar.removeGroupMember(user, sgroup)).rejects.toThrow()
        await sonar.makeUser(user)
        await expect (sonar.removeGroupMember(user, sgroup)).resolves.toBeUndefined()
        await sonar.addGroupMember(user, sgroup)
        await expect (sonar.listGroupMembers(sgroup)).resolves.toHaveLength(1)
        await expect (sonar.removeGroupMember(user, sgroup)).resolves.toBeUndefined()
        await expect (sonar.listGroupMembers(sgroup)).resolves.toHaveLength(0)
      })
    })

  })


  describe('permission templates', () => {
    test('create', async () => {
      await(expect(sonar.createPermissionTemplate(pathref))).resolves.toHaveProperty('name')
      await(expect(sonar.createPermissionTemplate(pathref))).rejects.toThrow(/already exists/)
    })

    test('get', async () => {
      await(expect(sonar.getPermissionTemplate(pathref))).resolves.toBeUndefined()
      await sonar.createPermissionTemplate(pathref)
      await(expect(sonar.getPermissionTemplate(pathref))).resolves.toHaveProperty('name')
    })

    test('delete', async () => {
      await(expect(sonar.deletePermissionTemplate(pathref))).rejects.toThrow(/not found/)
      await sonar.createPermissionTemplate(pathref)
      await(expect(sonar.deletePermissionTemplate(pathref))).resolves.toBeUndefined()
    })

    test('make', async () => {
      await expect(sonar.makePermissionTemplate(pathref)).resolves.toBeDefined()
      await expect(sonar.makePermissionTemplate(pathref)).resolves.toBeDefined()
    })

    test('make concurrent', async () => {
      await Promise.all([
        expect(sonar.makePermissionTemplate(pathref)).resolves.toBeDefined(),
        expect(sonar.makePermissionTemplate(pathref)).resolves.toBeDefined(),
        expect(sonar.makePermissionTemplate(pathref)).resolves.toBeDefined(),
      ])
    })

    test('configure', async () => {
      await Promise.all([
        sonar.makeProject(pathref),
        sonar.makeUserGroups(pathref),
        sonar.makePermissionTemplate(pathref),
      ])
      await sonar.configurePermissionTemplate(pathref)
    })

    test('apply', async () => {
      await Promise.all([
        sonar.makeProject(pathref),
        sonar.makeUserGroups(pathref),
        sonar.makePermissionTemplate(pathref)
      ])
      await sonar.applyPermissionTemplate(pathref)
    })
  })

  describe('quality profiles', () => {

    let qpname:string

    beforeEach(() => {
      qpname = sonar.qualityProfileName(pathref.asRepoRef())
    })

    afterEach(async () => {
      let dflt = cfg.ci.sonar.defaultQualityProfileName
      await sonar.deleteQualityProfile(qpname, lang, true).catch(ignore)
      await sonar.deleteQualityProfile(dflt, lang).catch(ignore)
      await sonar.deleteUserGroup(`${dflt}:${Sonar.ownersGroupSuffix}`).catch(ignore)
    })

    xtest('cleanup', async () => {
      await sonar.listQualityProfiles(lang).then(async (list) =>
        await list
          .filter(p => !p.isDefault)
          .forEachAsync(async (profile) =>
            await sonar.deleteQualityProfile(profile.name, lang).catch(ignore)))
    })

    test('create', async () => {
      await expect(sonar.createQualityProfile(qpname, lang)).resolves.toHaveProperty('key')
      await expect(sonar.createQualityProfile(qpname, lang)).rejects.toThrow(/already exists/)
    })

    test('get', async () => {
      await expect(sonar.getQualityProfile(qpname, lang)).resolves.toBeUndefined()
      await sonar.createQualityProfile(qpname, lang)
      await expect(sonar.getQualityProfile(qpname, lang)).resolves.toBeDefined()
    })

    test('list', async () => {
      await expect(sonar.listQualityProfiles(lang)).resolves.toBeDefined()
    })

    test('delete', async () => {
      await expect(sonar.deleteQualityProfile(qpname, lang)).rejects.toThrow(/does not exist/)
      await sonar.createQualityProfile(qpname, lang)
      await expect(sonar.deleteQualityProfile(qpname, lang)).resolves.toBeUndefined()
    })

    test('make', async () => {
      await expect(sonar.makeQualityProfile(qpname, lang)).resolves.toBeDefined()
      await expect(sonar.makeQualityProfile(qpname, lang)).resolves.toBeDefined()
    })

    test('make concurrent', async () => {
      await Promise.all([
        expect(sonar.makeQualityProfile(qpname, lang)).resolves.toBeDefined(),
        expect(sonar.makeQualityProfile(qpname, lang)).resolves.toBeDefined(),
        expect(sonar.makeQualityProfile(qpname, lang)).resolves.toBeDefined(),
      ])
    })

    test('make hierarchy', async () => {
      let dflt = 'Sonar way' //todo review this // cfg.ci.sonar.defaultQualityProfileName
      await expect(sonar.makeQualityProfileHierarchy(pathref, lang)).resolves.toBeDefined()
      await expect(sonar.getQualityProfile(dflt, lang)).resolves.toBeDefined()
      await expect(sonar.getQualityProfile(`${group}`, lang)).resolves.toHaveProperty('parentName', dflt)
      await expect(sonar.getQualityProfile(`${group}:${subgroup}`, lang)).resolves.toHaveProperty('parentName', `${group}`)
      await expect(sonar.getQualityProfile(`${group}:${subgroup}:${project}`, lang)).resolves.toHaveProperty('parentName', `${group}:${subgroup}`)
      await expect(sonar.getQualityProfile(`${group}:${subgroup}:${project}:${repo}`, lang)).resolves.toHaveProperty('parentName', `${group}:${subgroup}:${project}`)
    })

    xtest('delete hierarchy', async () => {
      await sonar.makeQualityProfileHierarchy(pathref, lang)
      await expect(sonar.deleteQualityProfile(qpname, lang, true)).resolves.toBeUndefined()
    })

    xtest('delete hierarchy skip used', async () => {
      await sonar.makeQualityProfileHierarchy(PathRef.parse(`${gname}/p1`), lang)
      await sonar.makeQualityProfileHierarchy(PathRef.parse(`${gname}/p2`), lang)
      await expect(sonar.deleteQualityProfile(`${gname}:p1`, lang, true)).resolves.toBeUndefined()
      await expect(sonar.getQualityProfile(`${gname}:p1`, lang)).resolves.toBeUndefined()
      await expect(sonar.getQualityProfile(`${gname}:p2`, lang)).resolves.toBeDefined()
      await expect(sonar.getQualityProfile(`${gname}`, lang)).resolves.toBeDefined()
    })

    test('update parent', async () => {
      let qpname = sonar.qualityProfileName(pathref.asRepoRef())
      let parent = sonar.qualityProfileName(pathref.asProjectRef())
      await sonar.createQualityProfile(qpname, lang)
      await sonar.createQualityProfile(parent, lang)
      let updated = await sonar.updateQualityProfileParent(qpname, lang, parent)
      return
    })

    test('make all', async () => {
      await sonar.makeQualityProfiles(pathref)
    })

    xtest('configure', async () => {
      await sonar.makeQualityProfileHierarchy(pathref, lang)
      await sonar.configureQualityProfile(qpname, lang)
    })

    test('apply', async () => {
      await Promise.all([
        sonar.makeProject(pathref),
        sonar.makeQualityProfile(sonar.sonarObjectId2(pathref.asRepoRef()), 'cs'),
      ])
      await sonar.applyQualityProfile(pathref, 'cs')
    })

    test('duplicates + autofix', async () => {
      let name = cfg.ci.sonar.defaultQualityProfileName
      await Promise.all([
        sonar.createQualityProfile(name, "cs"),
        sonar.createQualityProfile(name, "cs"),
        sonar.createQualityProfile(name, "cs"),
        sonar.createQualityProfile(name, "cs"),
        sonar.createQualityProfile(name, "cs"),
        sonar.createQualityProfile(name, "cs"),
      ]).catch(ignore)
      await sonar.getQualityProfile(name, "cs")
    })

  })

  describe('quality gates', () => {

    afterEach(async () => {
      await sonar.deleteQualityGate(pathref).catch(ignore)
    })

    xtest('cleanup', async () => {
      await sonar.listQualityGates().then(all => all
        .filter(x => /group-\d+/.test(x.name))
        .forEachAsync(async (x) => await sonar.deleteQualityGateById(x.id))
      )
    })

    test('create', async () => {
      await expect(sonar.createQualityGate(pathref)).resolves.toHaveProperty('id')
      await expect(sonar.createQualityGate(pathref)).rejects.toThrow(/already been taken/)
    })

    test('get', async () => {
      await expect(sonar.getQualityGate(pathref)).resolves.toBeUndefined()
      await sonar.createQualityGate(pathref)
      await expect(sonar.getQualityGate(pathref)).resolves.toHaveProperty('id')
    })

    test('delete', async () => {
      await expect(sonar.deleteQualityGate(pathref)).rejects.toThrow(/not found/)
      await sonar.createQualityGate(pathref)
      await expect(sonar.deleteQualityGate(pathref)).resolves.toBeUndefined()
    })

    test('make', async () => {
      await expect(sonar.makeQualityGate(pathref)).resolves.toHaveProperty('id')
      await expect(sonar.makeQualityGate(pathref)).resolves.toHaveProperty('id')
    })

    // fixme: make concurrent
    xtest('make concurrent', async () => {
      await Promise.all([
        expect(sonar.makeQualityGate(pathref)).resolves.toHaveProperty('id'),
        expect(sonar.makeQualityGate(pathref)).resolves.toHaveProperty('id'),
        expect(sonar.makeQualityGate(pathref)).resolves.toHaveProperty('id'),
        expect(sonar.makeQualityGate(pathref)).resolves.toHaveProperty('id'),
        expect(sonar.makeQualityGate(pathref)).resolves.toHaveProperty('id'),
      ])
    })

    test('apply', async () => {
      await Promise.all([
        sonar.makeProject(pathref),
        sonar.makeQualityGate(pathref),
      ])
      await sonar.applyQualityGate(pathref)

    })
  })


  describe('commands', () => {

    let user, developer, maintainer, path;

    beforeEach(() => {
      user = `test-${id}`
      developer = `${user}-developer`
      maintainer = `${user}-maintainer`
      path = `${gname}/${pname}/${rname}`
    })

    afterEach(async () => {
      await Promise.all([
        gitlab.deleteGroup(`${gname}`),
        gitlab.listUsers().then(async (users) =>
          users.filter(u => u.username.startsWith(user))
            .forEachAsync(async (u) => await gitlab.deleteUser(u.username))),
      ]).catch(ignore)

    })

    xtest('consolidate sonar users', async () => {
      await sonar.consolidateSonarUsers()
    })

    test('update user groups', async () => {
      await Promise.all([
        gitlab.makeProject(path),
        gitlab.createUser(developer, `${developer}@example.com`),
        gitlab.createUser(maintainer, `${maintainer}@example.com`),
      ])
      await Promise.all([
        await gitlab.addMember(developer, path),
        await gitlab.addMember(maintainer, path),
      ])

      //fixme await sonar.updateUserGroups(pathref)
      return
    })
  })

})

