import {fail, ignore, report, rethrow, retry, sleep, xrequest} from "./utils";
import extend from "extend";
import {cfg, Group, Repository} from "./cfg";
import {log} from "./log";
import {Gitlab, GitlabAccess, GitlabMember} from "./gitlab";
import {PathRef} from "./ciadmin";

export interface SonarSystemInfo {
  System: {
    Edition: string
  }
}

export interface SonarProjectRef {
  name: string
  repo: Repository
  branch?: string
}

interface SonarApplication {
  key: string
  name: string
  description: string
  isMain: boolean
  branch: string
  visibility: string
  projects: [{
    key: string
    name: string
    isMain: boolean
    enabled: boolean
    selected: boolean
  }]
  branches: [{
    name: string
    isMain: boolean
  }]
  tags: string[]

}

interface SonarProject {
  key: string
  name: string
  visibility: SonarProjectVisibility
}

interface SonarProjectBranch {
  name: string
  isMain: boolean
  type: string // BRANCH
  status: any
  excludedFromPurge: boolean
}

export enum SonarProjectVisibility {
  private = 'private',
  public = 'public',
}

interface SonarProjectLink {
  id: string
  name: string
  url: string
}

interface SonarLanguage {
  key: string
  name: string
}

export enum SonarGlobalPermission {
  admin = 'admin',
  profileadmin = 'profileadmin',
  gateadmin = 'gateadmin',
  scan = 'scan',
  provisioning = 'provisioning',
}

interface SonarPermissionTemplate {
  id: string
  name: string
  projectKeyPattern: string
}

interface SonarQualityProfile {
  key: string
  name: string
  language: string
  parentKey: string
  parentName: string
  isInherited: boolean
  isDefault: boolean
  projectCount: number
}

interface SonarQualityGate {
  id: number
  name: string
}

interface SonarUser {
  login: string
  email: string
  externalIdentity: string
  externalProvider: string
}

interface SonarToken {
  name: string
  createdAt: Date
  lastConnectionDate: Date
}

interface SonarUserGroup {
  id: number
  name: string
}

export class Sonar {

  static membersGroupSuffix = 'members'
  static ownersGroupSuffix = 'owners'

  static memberGroupRegex = new RegExp(`.*:${Sonar.membersGroupSuffix}$`)
  static ownerGroupRegex = new RegExp(`.*:${Sonar.ownersGroupSuffix}$`)
  static groupRegex = new RegExp(`.*:(${Sonar.membersGroupSuffix}|${Sonar.ownersGroupSuffix})$`)

  static sonarAdministratorsGroup = 'sonar-administrators'
  static sonarUsersGroup = 'sonar-users'

  gitlab = new Gitlab()



  sonarObjectId = (g:Group, p?, r?, x?) =>
    `${g.name}:${p ? p.name : ''}:${r ? r.name : ''}:${x || ''}`.replace(/:+/g, ':').replace(/:$/, '');

  sonarObjectId2(x:PathRef, suffix?) {
    return `${x.group?.replace('/', ':') || ''}:${x.project || ''}:${x.repo || ''}:${x.branch || ''}:${suffix || ''}`
      .replace(/:+/g, ':') // drop empty tokens
      .replace(/^:|:$/g, '') // drop leading/reailing ':' if any
  }

  sonarProjectId(x:PathRef) : string {
    return this.sonarObjectId2(cfg.sonar.useBranchSupport ? x.asRepoRef() : x)
  }

  // sonarObjectIdSlashed = (g:Group, p?, r?, x?) =>
  //   `${g.name}/${p ? p.name : ''}/${r ? r.name : ''}:${x || ''}`
  //     .replace(/\/+/g, '/')
  //     .replace(/:+/g, ':')
  //     .replace(/\/:/g, ':')
  //     .replace(/:$/, '');

  projectName = (ref:PathRef) => this.sonarObjectId2(ref)

  permissionTemplateName = (ref:PathRef) => this.sonarObjectId2(ref.asRepoRef())

  qualityGateName = (r:Repository) =>
    this.sonarObjectId(r.project.group, r.project, r)

  qualityProfileName = (ref:PathRef) => this.sonarProjectId(ref)

  // qualityProfileName = (r:Repository) =>
  //   this.qualityProfileName0(r.project.group, r.project, r)
  //
  // qualityProfileName0 = (g:Group, p?:Project, r?:Repository) =>
  //   this.sonarObjectId(g, p, r)

  ownersGroupName = (ref:PathRef) => this.sonarObjectId2(ref, Sonar.ownersGroupSuffix)

  // ownersGroupName0 = (g:Group, p?:Project, r?:Repository) =>
  //   this.sonarObjectId(g, p, r, 'owners')

  memberGroupName = (ref:PathRef) => this.sonarObjectId2(ref, Sonar.membersGroupSuffix)

  // membersGroupName0 = (g:Group, p?:Project, r?:Repository) =>
  //   this.sonarObjectId(g, p, r, 'members')

  // toSonarProject = (r:Repository, branch?) : SonarProjectRef => Object.assign({
  //   name: this.projectName(r, branch),
  //   repo: r,
  //   branch: branch,
  // })

  // async prepareSonarProjectForCI(ref:PathRef) {
  //   let template, gate;
  //   await Promise.all([
  //     this.makeProject(ref),
  //     this.makeUserGroups(ref),
  //     this.makePermissionTemplate(ref).then(x => template = x),
  //     this.makeQualityProfiles(r),
  //     this.makeQualityGate(ref).then( g => gate = g),
  //   ])
  //   await Promise.all([
  //     this.configurePermissionTemplate(ref),
  //     this.configureQualityProfiles(r),
  //   ])
  //   await Promise.all([
  //     this.applyPermissionTemplate(ref),
  //     this.applyQualityProfiles(r, ref),
  //     this.applyQualityGate(ref)
  //   ])
  // }

  /**
   * Gitlab users are imported on demand into sonar by oauth integration:
   * - new login is a string with random suffix
   * - externalIdentity matches source in gitlab
   * Try to consolidate externalIdentity => login.
   *
   */
  async consolidateSonarUsers() {
    let users = await load('users/search', x => x.users) as SonarUser[]

    // these are created by sonar's Gitlab ALM:
    // external_id matches gitlab user ID
    // external_login matches gitlab username
    // login is generated from external_login with added suffix
    let found = users.filter(u =>
      u.externalProvider == 'gitlab' &&
      u.login != u.externalIdentity &&            // mismatch?
      ! /^[0-9]+$/.test(u.externalIdentity) &&    // filter out numeric IDs (which is gitlab user id)
      u.login.startsWith(u.externalIdentity)      // additional heuristics to avoid messing with unrelated objects
    )
    if (found.length == 0) log.debug('Found no users that need username consolidation.')

    let gitlab = new Gitlab()
    await found.forEachAsync(async (u) => {
      let gu = await gitlab.findUserByEmail(u.email)
      log.info(`Rename user: ${u.login} => ${gu.username}`)
      await xrequest(post('users/update_login', {login: u.login, newLogin: gu.username}))
        .catch(err => {
          if (/already exists/.test(err.message))
            log.error(`Cannot rename login ${u.login} => ${u.externalIdentity}. User ${u.externalIdentity} already exists`)
          else
            report(err)
        })
    })
  }

  async updateLogin(u:SonarUser, newLogin) {
    await xrequest(post('users/update_login', {login: u.login, newLogin: newLogin}))
      .catch(err => {
        if (/already exists/.test(err.message))
          log.error(`Cannot rename login ${u.login} => ${newLogin}. User ${newLogin} already exists`)
        else
          report(err)
      })
  }

  async updateUserGroups(ref: PathRef) {
    await this.makeUserGroups(ref)
    let owners = this.sonarObjectId2(ref.asRepoRef(), Sonar.ownersGroupSuffix)
    let members = this.sonarObjectId2(ref.asRepoRef(), Sonar.membersGroupSuffix)
    //let src: GitlabGroupMember[], currentOwners: Set<string>, currentMembers: Set<string>, all: Set<string>
    let [ src, currentOwners, currentMembers, all ] = await Promise.all([
      this.gitlab.getMembers(ref),
      this.getUserGroupMembers(ref, true).then(x => new Set(x.map(u => u.login))),
      this.getUserGroupMembers(ref, false).then(x => new Set(x.map(u => u.login))),
      this.listUsers().then(x => new Set(x.map(u => u.login))),
    ])
    const isOwner = (u:GitlabMember) => u.access_level >= GitlabAccess.Maintainer
    await src
      // ignore users not yet known to sonar
      .filter(x => all.has(x.username))
      // ignore already associated users
      .filter(x => isOwner(x) && !currentOwners.has(x.username) || !currentMembers.has(x.username))
      .forEachAsync(async (u) => {
        await xrequest(post('user_groups/add_user', {
          name: isOwner(u) ? owners : members,
          login: u.username,
        }))
      })
    let parent = ref.getParent()
    if (parent) await this.updateUserGroups(parent)
  }

  /// applications (Developer Edition) ///

  async createApplication(ref:PathRef) {
    let name = this.sonarObjectId2(ref.asProjectRef())
    await xrequest(post(`applications/create`, {
      key: name,
      name: name,
      visibility: 'private'
    }))
  }
  async updateApplication() {}
  async deleteApplication(ref:PathRef) {
    await xrequest(post(`applications/delete`, {
      application: this.sonarObjectId2(ref.asProjectRef()),
    }))
  }
  async getApplication(ref:PathRef) : Promise<SonarApplication> {
    let app = this.sonarObjectId2(ref.asProjectRef())
    return await xrequest(get(`applications/show`, {
      application: app,
    })).then(x => x.application)
  }

  async makeApplication(ref:PathRef) {
    let found = await this.getApplication(ref).catch(ignore)
    if (found) return found
    return await this.createApplication(ref);
  }

  async addApplicationProject(ref:PathRef) {
    let app = this.sonarObjectId2(ref.asProjectRef())
    let prj = this.sonarProjectId(ref)
    let found = await this.getApplication(ref).then(x => x.projects?.find(p => p.key == prj))
    if (found) return
    await xrequest(post(`applications/add_project`, {
      application: app,
      project: prj,
    }))
  }
  async removeApplicationProject() {}

  async createApplicationBranch(ref:PathRef) {
    await xrequest(post(`applications/create_branch`, {
      application: this.sonarObjectId2(ref.asProjectRef()),
      branch: ref.branch,
      project: this.sonarProjectId(ref),
      projectBranch: ref.branch,
    }))
  }

  /// projects ///


  async createProject(ref: PathRef) : Promise<SonarProject> {
    let name = this.sonarProjectId(ref)
    return await xrequest(post(
      'projects/create',
      { name: name, project: name, visibility: "private", mainBranch: ref.branch }))
      .then(data => data.project)
      .catch(rethrow)
  }

  async getProject(ref:PathRef) : Promise<SonarProject> {
    let name = this.sonarProjectId(ref)
    return await xrequest(get('components/search', { q: name, qualifiers: "TRK" }))
      .then(data => data.components.find(x => x.key === name))
      .catch(rethrow)
  }

  async listProjects() : Promise<SonarProject[]> {
    return await load('projects/search', data => data.components)
  }

  async deleteProject(ref: PathRef) {
    let name = this.sonarProjectId(ref)
    await xrequest(post('projects/delete', { project: name }))
  }

  async deleteProjectByKey(key) {
    await xrequest(post('projects/delete', { project: key}))
  }

  async makeProject(ref:PathRef, mainBranch?) : Promise<SonarProject> {
    let p = await this.getProject(ref).catch(rethrow)
    if (p) return p

    try {
      return await this.createProject(ref)
    } catch (err) {
      await new Promise((r) => setTimeout(r, 1000))
      if (/already exists/.test(err.message)) p = await this.getProject(ref)
      // this happens when on PSQLException: ERROR: duplicate key
      else if (/500.*contact your administrator/.test(err.message)) {
        return await retry(5, 500, async () => await this.getProject(ref))
      }
      else rethrow(err)
    }
    return p
  }

  async updateProjectVisibility(ref:PathRef, visibility:SonarProjectVisibility) {
    await xrequest(post(`projects/update_visibility`, {
      project: this.sonarProjectId(ref),
      visibility: visibility,
    }))
  }

  /// project branches ///

  async listProjectBranches(ref:PathRef) : Promise<SonarProjectBranch[]> {
    let project = this.sonarProjectId(ref)
    return await xrequest(get(`project_branches/list?project=${project}`))
  }

  async renameProjectMainBranch(ref:PathRef, name:string) {
    await xrequest(post(`project_branches/rename`, {
      project: this.sonarProjectId(ref),
      name: name,
    }))
  }

  async deleteProjectBranch(ref: PathRef, name) {
    await xrequest(post(`project_branches/delete`, {
      project: this.sonarProjectId(ref),
      branch: name,
    }))
  }

  async setProjectBranchDeletionProtection(ref:PathRef, branch:string, protect:boolean) {
    await xrequest(post(`project_branches/set_automatic_deletion_protection`, {
      project: this.sonarProjectId(ref),
      branch: branch,
      value: protect,
    }))
  }

  /// users ///

  async createUser(username, email?, name?) : Promise<SonarUser> {
    return await xrequest(post('users/create', {
      login: username,
      email: email || `${username}@${cfg.ciadmin.domain}`,
      name:  name || username,
      local: false,
    })).then(x => x.user)
  }

  async getUser(username) : Promise<SonarUser|undefined> {
    return await xrequest(get('users/search', { q: username }))
      .then(x => x.users)
      .then((found:SonarUser[]) => found.filter(u => u.login == username).first())
  }

  async makeUser(username, email?, name?) : Promise<SonarUser> {
    let found = await this.getUser(username)
    if (found) return found
    return await this.createUser(username, email, name)
  }

  async deleteUser(username) {
    await xrequest(post('users/deactivate', { login: username }))
  }

  async listUsers() : Promise<SonarUser[]> {
    return await load('users/search', x => x.users)
  }

  async getUserGroupMembers(ref: PathRef, owners: boolean) {
    let name = owners ? this.ownersGroupName(ref) : this.memberGroupName(ref)
    return await load(`user_groups/users?name=${name}`, (x) => x.users)
  }

  async updateUserIdentityProvider(username, identity, provider) {
    await xrequest(post(`users/update_identity_provider`, {
      login: username,
      newExternalIdentity: identity,
      newExternalProvider: provider,
    }))
  }

  /// tokens ///

  async generateUserToken(username, tokenName) : Promise<string> {
    let x = await xrequest(post(`user_tokens/generate`, {
      login: username,
      name: tokenName,
    }))
    return x.token
  }

  async listUserTokens(username) : Promise<SonarToken[]> {
    let all = await xrequest(get(`user_tokens/search`, { login: username }))
    return all.userTokens
  }

  async hasToken(username, tokenName) : Promise<boolean> {
    let all = await this.listUserTokens(username)
    let found = all.find(x => x.name === tokenName)
    return !!found
  }

  async revokeUserToken(username, tokenName) {
    await xrequest(post(`user_tokens/revoke`, {
      login: username,
      name: tokenName,
    }))
  }

  /// user groups ///


  async createUserGroup(name: string) {
    // sonar allow duplicate group names, check first
    let found : any = await this.getUserGroup(name)
    if (found) throw fail(`User group already exists: ${name}`)

    // if missing, create one
    let created : any = await xrequest(post('user_groups/create', { name: name }))
      .then(data => data.group)
      .catch(rethrow)

    // check if created, this call returns first found group only
    found = await this.getUserGroup(name)

    // if there is an ID mismatch, our creation is a duplicate, silently delete it
    if (found.id != created.id) await xrequest(post('user_groups/delete', { id: created.id }))

    return found
  }

  async getUserGroup(name : string) : Promise<any> {
    return await xrequest(get('user_groups/search', { q: name }))
      .then(data => data.groups.find(x => x.name === name))
      .catch(rethrow)
  }

  async listUserGroups() : Promise<SonarUserGroup[]> {
    return await load('user_groups/search', x => x.groups)
  }

  async deleteUserGroup(name: string) {
    await xrequest(post('user_groups/delete', { name: name }))
  }

  async deleteUserGroups(ref:PathRef, cascade= false) {
    if (ref.branch) ref = new PathRef(ref)
    let parent = ref.getParent()
    await Promise.all([
      this.deleteUserGroup(this.sonarObjectId2(ref, Sonar.membersGroupSuffix)),
      this.deleteUserGroup(this.sonarObjectId2(ref, Sonar.ownersGroupSuffix)),
      parent && cascade ? this.deleteUserGroups(parent, cascade) : undefined,
    ])
  }


  async makeUserGroup(name: string) {
    return (
      await this.getUserGroup(name)
      ||
      await this.createUserGroup(name)
        .catch(err => {
          if (/already exists|contact.*administrator/i.test(err.message)) return this.getUserGroup(name).then()
          else rethrow(err)
        })
    )
  }

  async makeUserGroups(ref:PathRef) {
    if (ref.branch) ref = new PathRef(ref)
    let parent = ref.getParent()
    await Promise.all([
      this.makeUserGroup(this.sonarObjectId2(ref, "members")),
      this.makeUserGroup(this.sonarObjectId2(ref, "owners")),
      parent ? this.makeUserGroups(parent) : undefined,
    ])
  }

  /// user groups: membership ///

  async listGroupMembers(name) : Promise<SonarUser[]> {
    return await load(`user_groups/users?name=${name}`, x => {
      if (x.errors) throw new Error(x.errors)
      return x.users;
    })
  }

  async addGroupMember(username, group) {
    await xrequest(post(`user_groups/add_user`, {
      login: username,
      name: group,
    }))
  }

  async removeGroupMember(username, group) {
    await xrequest(post(`user_groups/remove_user`, {
      login: username,
      name: group,
    }))
  }

  /// languages ///

  async listLanguages() : Promise<SonarLanguage[]> {
    return xrequest(get(`languages/list`)).then(x => x.languages)
  }

  /// permission templates ///


  async createPermissionTemplate(ref: PathRef) : Promise<any> {
    let name = this.sonarObjectId2(ref.asRepoRef())
    return await xrequest(post('permissions/create_template',
      { name: name, projectKeyPattern: `${name}(:.*)?` }))
      .then(data => data.permissionTemplate)
  }

  async getPermissionTemplate(ref: PathRef) : Promise<any> {
    let name = this.sonarObjectId2(ref.asRepoRef())
    return await xrequest(get('permissions/search_templates', { q: name }))
      .then(data => data.permissionTemplates.find(x => x.name === name))
      .catch(rethrow)
  }

  async deletePermissionTemplate(ref: PathRef) {
    let name = this.sonarObjectId2(ref.asRepoRef())
    await xrequest(post('permissions/delete_template', { templateName: name }))
  }

  async deletePermissionTemplateByName(name:string) {
    await xrequest(post('permissions/delete_template', { templateName: name }))
  }

  async listPermissionTemplates() : Promise<SonarPermissionTemplate[]> {
    return await xrequest(get(`permissions/search_templates`)).then(x => x.permissionTemplates)
  }

  async makePermissionTemplate(ref:PathRef) {
    return (
      await this.getPermissionTemplate(ref)
      ||
      await this.createPermissionTemplate(ref).catch(err => {
        if (/already exists/.test(err.message)) return this.getPermissionTemplate(ref)
        else rethrow(err)
      })
    )
  }

  async configurePermissionTemplate(ref: PathRef) {
    const name = this.sonarObjectId2(ref.asRepoRef())
    const memberPermissions = ["user", "scan", "codeviewer"];
    const ownerPermissions = memberPermissions.concat(["admin", "issueadmin", "securityhotspotadmin"]);
    const adminPermissions = ["user", "admin", "issueadmin", "codeviewer"];

    let ownerGroups : string[] = []
    let memberGroups : string[] = []

    for (let x : PathRef|undefined = ref; x; x = x.getParent()) {
      ownerGroups.push(this.sonarObjectId2(x, Sonar.ownersGroupSuffix))
      memberGroups.push(this.sonarObjectId2(x, Sonar.membersGroupSuffix))
    }

    await Promise.all([
      [...ownerGroups].forEachAsync(async (g) =>
        await [...ownerPermissions].forEachAsync(async (p) =>
          await this.addUserGroupToPermissionTemplate(g, name, p))),
      [...memberGroups].forEachAsync(async (g) =>
        await [...memberPermissions].forEachAsync(async (p) =>
          await this.addUserGroupToPermissionTemplate(g, name, p))),
      [...adminPermissions].forEachAsync(async p =>
        await this.addUserGroupToPermissionTemplate(Sonar.sonarAdministratorsGroup, name, p)),
    ])
  }

  async addUserGroupToPermissionTemplate(groupName, templateName, permission) {
    await xrequest(post(
      'permissions/add_group_to_template',
      { groupName: groupName, templateName: templateName, permission: permission }))
      .catch(rethrow);
  }

  async applyPermissionTemplate(ref: PathRef) {
    let template = await this.getPermissionTemplate(ref)
    let project = await this.getProject(ref)
    await xrequest(post(
      'permissions/apply_template',
      { projectKey: project.key, templateId: template.id }))
  }

  /// permissions ///

  async addPermissionToUser(username, permission:SonarGlobalPermission) {
    await xrequest(post(`permissions/add_user`, {
      login: username,
      permission: permission,
    }))
  }


  /// quality profiles ///


  async createQualityProfile(name, lang) : Promise<SonarQualityProfile> {
    let created = await xrequest(post('qualityprofiles/create',
      { name: name, language: lang }))
      .then(x => x.profile)
      .catch(rethrow)
    let found = await this.getQualityProfile(name, lang)
    if (found.key != created.key) {
      // should not happen; drop created
      log.warn(`Duplicate quality profile created: ${name}#${lang}. Cleaning up!`)
      await this.renameQualityProfile(created.key, `${name}-${created.key}`)
        .then(async () => await this.deleteQualityProfile(`${name}-${created.key}`, lang))
        .catch(report)
    }
    return found
  }

  async getQualityProfile(name, lang) : Promise<SonarQualityProfile> {
    let resp = await xrequest(get('qualityprofiles/search', { language: lang }))
      .then(x => x.profiles.filter(p => p.name == name))

    if (resp.length>1) {
      log.warn(`Duplicate quality profiles found (${name}#${lang}). Renaming to avoid conflict.`)
      await resp.slice(1).forEachAsync(async (x) =>
        await this.renameQualityProfile(x.key, `${x.name}-${x.key}`))
    }
    return resp.first()
  }

  async listQualityProfiles(lang) : Promise<SonarQualityProfile[]> {
    return await xrequest(get('qualityprofiles/search', { language: lang }))
      .then(x => x.profiles)
  }

  async deleteQualityProfile(name, lang, cascade?:boolean) {

    await xrequest(post('qualityprofiles/delete',
      { qualityProfile: name, language: lang }))
      .catch(rethrow)

    if (!cascade) return

    let components = name.split(':')
    let profiles = [ cfg.ci.sonar.defaultQualityProfileName ]
    for (let i=0; i<components.length; i++) profiles.push(components.slice(0, i+1).join(':'))

    let toDelete = profiles.slice(0, profiles.length-1).reverse()

    for (let i = 0; i < toDelete.length; i++) {
      let canDelete =
        await xrequest(post('qualityprofiles/inheritance', { qualityProfile: toDelete[i], language: lang}))
          .then(x => x.children.length == 0)
          .catch(rethrow)
      if (canDelete)
        await xrequest(post('qualityprofiles/delete',{ qualityProfile: toDelete[i], language: lang }))
          .catch(rethrow)
    }
  }

  async copyQualityProfile(id, name) {
    await xrequest(post(`qualityprofiles/copy`, {
      fromKey: id,
      toName: name,
    }))
  }

  async makeDefaultQualityProfile(lang): Promise<SonarQualityProfile> {
    let name = cfg.ci.sonar.defaultQualityProfileName
    let found = await this.getQualityProfile(name, lang)
    if (found) return found

    let dflt = await this.getDefaultQualityProfile(lang)
    await this.copyQualityProfile(dflt.key, name)
    return await this.getQualityProfile(name, lang)
  }

  async makeQualityProfile(name, lang) : Promise<SonarQualityProfile> {
    return (
      await this.getQualityProfile(name, lang)
      ||
      await this.createQualityProfile(name, lang).catch((err) =>
        /already exists|contact.*administrator/i.test(err.message)
          ? this.getQualityProfile(name, lang).then()
          : rethrow(err))
    )
  }

  async makeQualityProfileHierarchy(ref:PathRef, lang) {
    let parent = ref.getParent()
    if (parent) await this.makeQualityProfileHierarchy(parent, lang)

    let id = this.sonarObjectId2(ref)
    let dflt = cfg.ci.sonar.defaultQualityProfileName ?? await this.getDefaultQualityProfile(lang).then(x => x.name)
    let parentId = parent
      ? this.sonarObjectId2(parent)
      : dflt

    await this.makeQualityProfile(id, lang)
    await this.updateQualityProfileParent(id, lang, parentId)

    return await this.getQualityProfile(id, lang)
  }

  async makeQualityProfiles(ref:PathRef, languages?:string[]) {
    ref = ref.asRepoRef()
    //let languages = ref.resolveRepoRef()?.options?.languages

    await languages?.forEachAsync(async (lang) =>
      await this.makeDefaultQualityProfile(lang))

    let names = [cfg.ci.sonar.defaultQualityProfileName]
    names.push(cfg.ci.sonar.defaultQualityProfileName)
    for (let i = ref; i; i=i.getParent())
      names.push(this.sonarObjectId2(i))

    await languages?.forEachAsync(async (lang) =>
      await names.forEachAsync(async (name) =>
        await this.makeQualityProfile(name, lang)))

  }


  getParentName(s: string) {
    return s.replace(/(:[^:]+|^[^:]*)$/, '')
  }

  async configureQualityProfile(name, lang) {
    let profiles: string[] = []
    for (let s=name; s.length>0; s = this.getParentName(s)) profiles.push(s)
    profiles.push(cfg.ci.sonar.defaultQualityProfileName)
    let dflt = await this.getDefaultQualityProfile(lang)
    await profiles.forEachAsync(async (x) => {
      await this.makeQualityProfile(x, lang).then(async () => {
        let parent = profiles[profiles.indexOf(x) + 1] || dflt.name
        await Promise.all([
          this.updateQualityProfileParent(x, lang, parent),
          this.updateQualityProfilePermissions(x, lang),
        ])
      })
    })
  }

  async getDefaultQualityProfile(lang) : Promise<any> {
    let resp = await xrequest(get('qualityprofiles/search', { defaults: true, language: lang }))
    return resp.profiles.first()
  }


  async renameQualityProfile(key, newname) {
    await xrequest(post('qualityprofiles/rename', { key: key, name: newname }))
  }

  // async configureQualityProfiles(r:Repository) {
  //   let name = this.qualityProfileName(ref)
  //   await r.options?.languages?.forEachAsync(async (lang) =>
  //     await this.configureQualityProfile(name, lang))
  // }

  async applyQualityProfile(ref:PathRef, lang:string) {
    let project = this.sonarProjectId(ref)
    let profile = this.sonarObjectId2(ref.asRepoRef())
    await xrequest(post(
      'qualityprofiles/add_project',
      { project: project, qualityProfile: profile, language: lang}))
      .catch(rethrow);
  }

  // async applyQualityProfiles(r: Repository, ref: SonarProjectRef) {
  //   r.options?.languages?.forEachAsync(async (lang) =>
  //     await this.applyQualityProfile(ref, lang))
  // }

  async updateQualityProfileParent(name, lang, parent) : Promise<SonarQualityProfile> {
    await xrequest(post(
      'qualityprofiles/change_parent',
      { qualityProfile: name, language: lang, parentQualityProfile: parent }))
      .catch(rethrow);
    return await this.getQualityProfile(name, lang)
  }

  async updateQualityProfilePermissions(name, lang) {
    let group = `${name}:owners`;
    await this.makeUserGroup(group)
    await xrequest(post('qualityprofiles/add_group', {
      qualityProfile: name,
      language: lang,
      group: group,
    }));
  }

  /// quality gates ///

  async getDefaultQualityGate() {
    let all = await xrequest(get('qualitygates/list'))
    return all.qualitygates.find(x => x.id = all.default)
  }

  async createQualityGate(ref: PathRef) : Promise<SonarQualityGate> {
    let name = this.sonarObjectId2(ref.asRepoRef())
    let dflt = await this.getDefaultQualityGate()
    return await xrequest(post('qualitygates/copy', { sourceName: dflt.name, name: name })).catch(rethrow)
  }

  async getQualityGate(ref: PathRef) : Promise<SonarQualityGate> {
    let name = this.sonarObjectId2(ref.asRepoRef())
    return await load('qualitygates/list', x => x.qualitygates)
      .then(list => list.find((x: SonarQualityGate) => x.name === name))
      .catch(rethrow)
  }

  async listQualityGates() : Promise<SonarQualityGate[]> {
    return await load('qualitygates/list', x => x.qualitygates)
  }

  async deleteQualityGate(ref: PathRef) {
    let found = await this.getQualityGate(ref);
    if (!found) throw fail(`Quality gate not found: ${this.sonarObjectId2(ref.asRepoRef())}`)
    await xrequest(post('qualitygates/destroy', { id: found.id }))
  }

  async deleteQualityGateById(id) {
    await xrequest(post('qualitygates/destroy', { id: id }))
  }


  async makeQualityGate(ref: PathRef) {
    let name = this.sonarObjectId2(ref.asRepoRef())
    return (
      await this.getQualityGate(ref)
      ||
      await this.createQualityGate(ref)
        .catch(err => {
          if (/already.*taken|contact.*administrator/i.test(err.message)) return this.getQualityGate(ref).then()
          else rethrow(err)
        })
    )
  }

  async updateQualityGatePermissions(ref: PathRef) {
    let name = this.sonarObjectId2(ref);
    let group = this.ownersGroupName(ref);
    await this.makeUserGroup(group)
    await xrequest(post('qualitygates/add_group', {
      gateName: name,
      groupName: group,
    }));
  }

  async applyQualityGate(ref: PathRef) {
    let project = this.sonarProjectId(ref)
    let gate = await this.getQualityGate(ref)
    await xrequest(post('qualitygates/select', { projectKey: project, gateId: gate.id })).catch(report);
  }

  /// new code ///

  async setNewCodePeriod(ref:PathRef) {
    await xrequest(post(`new_code_periods/set`, {
      project: this.sonarProjectId(ref),
      type: 'NUMBER_OF_DAYS',
      value: 90
    }))
  }


  /// project links ///

  async createProjectLink(ref: PathRef, name, url) : Promise<SonarProjectLink> {
    let project = await this.getProject(ref)
    return await xrequest(post(`project_links/create`, { projectKey: project.key, name: name, url: url}))
  }

  async getProjectLink(ref: PathRef, name) : Promise<SonarProjectLink | undefined> {
    let all = await this.listProjectLinks(ref)
    return all.find(x => x.name == name)
  }

  async deleteProjectLink(ref: PathRef, name) {
    let all = await this.listProjectLinks(ref)
    let found = all.filter(x => x.name == name).first()
    if (found) await xrequest(post(`project_links/delete`, { id: found.id }))
  }

  async listProjectLinks(ref: PathRef) : Promise<SonarProjectLink[]> {
    let project = await this.getProject(ref)
    let x = await xrequest(post(`project_links/search`, { projectKey: project.key }))
    return x.links
  }

  /// badges ///

  async getBadge(ref:PathRef, metric) {
    return await xrequest(req(`project_badges/measure`, 'GET', {
      project: this.sonarObjectId2(ref),
      metric: metric,
    }, {
      json: false
    }))
  }
}

type DataExtractor = (src:any) => any[]

async function load(uri, extractor?:DataExtractor) : Promise<any[]> {
  let result : any[] = []
  let opts = get(uri, {p: 1, ps: 50})
  while (true) {
    let response = await xrequest(opts).catch(rethrow)
    let data = extractor ? extractor(response) : response
    data.forEach(x => result.push(x))

    let page = response.paging?.pageIndex || response.p || 1
    let pageSize = response.paging?.pageSize || response.ps || result.length
    let total = response.paging?.total || response.total || result.length

    if (page * pageSize >= total) break // no more data

    opts.qs.p++ // go get next page
  }
  return result
}

function req(uri, method, data?, options?) {
  cfg.sonar.url || fail('cfg.sonar.url')
  cfg.sonar.token || fail('cfg.sonar.token')
  return extend(
    {
      url: `${cfg.sonar.url}/api/${uri}`,
      headers: {
        'Authorization': `Basic ${Buffer.from(`${cfg.sonar.token}:`).toString('base64')}`,
        'Content-Type': 'application/json',
        'Accept': 'application/json',
      },
      method: method,
      resolveWithFullResponse: false,
      json: true,
      qs: (method === 'GET') ? data : {},
      query: (method === 'GET') ? data : {},
      form: (method === 'POST') ? data : {},
    },
    options || {}
  );
}

function get(uri, data?) {
  return req(uri, 'GET', data);
}

function post(uri, data) {
  return req(uri, 'POST', data);
}

function xpost(uri, data) {
  return req(uri, 'POST', data, { resolveWithFullResponse: true });
}

function del(uri) {
  return req(uri, 'DELETE')
}
