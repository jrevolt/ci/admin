import {cfg} from "./cfg";

export {}

declare global {
  namespace jest {
    interface Matchers<R> {
      toBeNonEmptyArray() : R
    }
  }
}

expect.extend({
  toBeNonEmptyArray(received) {
    return {
      pass: received.length > 0,
      message: () => 'Expected non empty array',
    }
  }
})

if (cfg.debug) jest.setTimeout(60000)
