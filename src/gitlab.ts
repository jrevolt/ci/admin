import extend from 'extend'
import {fail, report, rethrow, xrequest} from "./utils";
import {cfg, Group, Project, Repository} from "./cfg";
import assert from "assert";
import {PathRef} from "./ciadmin";
import {Sonar, SonarProjectVisibility} from "./sonar";
import request from "request-promise";
import {log} from "./log";
import moment from 'moment';

export interface GitlabGroup {
  id: number
  name: string
  path: string
  full_path: string
  parent_id?: number
}

export interface GitlabProject {
  id: number
  name: string
  path_with_namespace: string
  default_branch: string
}

export interface ProjectRef {
  group: string
  project?: string
  repo?: string
  branch?: string
}

export interface GitlabUser {
  id?: number
  username: string
  email: string
  public_email?: string
  name?: string
  reset_password?: boolean
  skip_confirmation?: boolean
  identities?: [{
    provider: string
    extern_uid: string
  }]
}

export interface GitlabMember extends GitlabUser {
  access_level: number
}

export enum GitlabAccess {
  Guest = 10,
  Reporter = 20,
  Developer= 30,
  Maintainer= 40,
  Owner = 50,
}

export interface SshKey {
  id: number,
  title: string,
  key: string,
  created_at: string,
}

export interface ImpersonationToken {
  id: number,
  user_id: number,
  scopes: string[],
  name: string,
  active: boolean,
  revoked: boolean,
  token: string, // only on create

}

export interface KCluster {
  cluster_id: number,
  name: string,
  domain: string,
  enabled: boolean,
  managed: boolean,
  environment_scope: string,

  // as returned by gitlab api
  "platform_kubernetes_attributes[api_url]": string,
  "platform_kubernetes_attributes[token]": string,
  "platform_kubernetes_attributes[authorization_type]": string,

  // non-standard properties normalized
  k8s_api_url: string,
  k8s_token: string,
  k8s_authorization_type: string,
}

export interface GVariable {
  key: string,
  value: string,
  protected: boolean,
  masked: boolean,
  variable_type: string,
  environment_scope: string,
}

export class Gitlab {

  groupId = (g)         => `${g.name}`;
  projectId = (g, p)    => `${g.name}/${p.name}`;
  repoId = (g, p, r)    => `${g.name}/${p.name}/${r.name}`;
  objectId = (g, p?, r?)  => `${g.name}` + (p ? `/${p.name}` : '') + (r ? `/${r.name}` : '');

  gitlabObjectId = (g:Group, p?:Project, r?:Repository) =>
    `${g.name}/${p ? p.name : ''}/${r ? r.name : ''}`.replace(/\/+/g, '/').replace(/\/$/, '');


  parseProjectPath(path: string) : ProjectRef {
    let parts = path.split("/")
    let hasBranch = parts[parts.length-1].indexOf(':') != -1
    if (parts.length < 3) throw new Error(`Expected group[/subgroup]/project/repo[:branch]. Found: ${path}`)
    return {
      group: parts.slice(0, parts.length-2).join(":"),
      project: parts[parts.length-2],
      repo: parts[parts.length-1].split(":")[0],
      branch: hasBranch ? parts[parts.length-1].split(":")[1] : undefined,
    }
  }

  resolveRepository(ref: ProjectRef) : Repository {
    let found = cfg.groups?.flatMap(g => g.projects).flatMap(p => p?.repositories)
      .find(r => r?.name == ref.repo && r?.project.name == ref.project && r?.project.group.name == ref.group)
    return found as Repository
  }

  /// actions ///

  async makeAllGroups(groups:Group[]=cfg.groups) {
    await groups.forEachAsync(async g => {
      await this.makeGroup(PathRef.fromGroup(g).group as string)
      if (g.groups) await this.makeAllGroups(g.groups)
    })
  }

  async makeAllProjects(groups:Group[]=cfg.groups) {
    await this.makeAllGroups(groups)
    await groups.forEachAsync(async (g) =>
      await cfg.getAllProjects(g).forEachAsync(async (p) =>
        await this.makeGroup(`${PathRef.getGroupPath(p.group)}/${p.name}`)))
  }

  async makeAllRepositories() {
    await this.makeAllProjects()
    await cfg.getAllRepositories().forEachAsync(async (r) =>
      await this.makeProject(`${PathRef.getGroupPath(r.project.group)}/${r.project.name}/${r.name}`))
  }

  async configureVariables(path:string, variables:any) {
    let project = await this.getProject(path)
    let kind = project ? 'projects' : 'groups'
    let current = await load(`${kind}/${encodeURIComponent(path)}/variables`)
      .then((vars:any[]) => vars.reduce((m,o) => (m[o.key] = o.value, m), {}))
    await Object.entries(variables).forEachAsync(async ([k,v]) => {
      let actual = current[k]
      v = (v as any).toString()
      if (actual === undefined)
        await xrequest(post(`${kind}/${encodeURIComponent(path)}/variables`, { key: k, value: v }))
      else if (actual != v)
        await xrequest(put(`${kind}/${encodeURIComponent(path)}/variables/${k}`, { value: v }))
      //else: up to date
    })
    return
  }

  async cleanupVariables(path) {
    let id = encodeURIComponent(path)
    await load(`projects/${id}/variables`).then(async (vars:any[]) =>
      await vars.forEachAsync(async (x) =>
        await xrequest(del(`projects/${id}/variables/${x.key}`))))
  }


  /// groups ///

  async createGroup(name: string, parent?: string | number) : Promise<GitlabGroup> {
    if (parent && !Number.isInteger(parent))
      this.getGroup(parent as string).then(x => parent = x.id)
    return await xrequest(post('groups', {
      name: name, path: name, parent_id: parent,
      visibility: 'private',
    })).catch(rethrow)
  }

  async getGroup(name: string) : Promise<GitlabGroup> {
    assert(name?.length, 'missing/empty group name')
    return await xrequest(get(`groups/${encodeURIComponent(name)}`))
      .catch(err => /Not Found/.test(err.message) ? undefined : rethrow(err))
  }

  async listGroups() : Promise<GitlabGroup[]> {
    return await load('groups')
  }

  async makeGroup(name: string) : Promise<GitlabGroup> {
    assert(name && name.length>0 || false)

    let found : GitlabGroup = await this.getGroup(name)
    if (found) return found

    let names = name.split('/').filter(x=>x)
    let group : GitlabGroup|any = undefined
    for (let i=0; i<names.length; i++) {
      group = await this.makeGroup0(names[i], group)
    }
    return group
  }

  async makeGroup0(name: string, parent?: GitlabGroup) : Promise<GitlabGroup> {
    let fqname = name;
    if (parent) {
      await this.getGroup(parent.full_path).then(x => fqname = `${x.full_path}/${name}`)
    }
    return (
      await this.getGroup(fqname)
      ||
      await this.createGroup(name, parent?.id).catch(async (err) => {
        if (/already been taken/.test(err.message)) return await this.getGroup(name)
        else rethrow(err)
      })
    )
  }

  async deleteGroup(name: string) {
    await xrequest(del(`groups/${encodeURIComponent(name)}`)).catch(rethrow)
  }

  async getBranches(g, p, r) {
    let id = encodeURIComponent(this.repoId(g, p, r));
    return await load(`projects/${id}/repository/branches`);
  }

  async getGroupMembers(groupName, inherit?:boolean) : Promise<GitlabMember[]> {
    return await load(`groups/${encodeURIComponent(groupName)}/members/${inherit ? 'all' : ''}`);
  }

  async getProjectMembers(ref:PathRef, inherit:boolean=false) : Promise<GitlabMember[]> {
    let rid= encodeURIComponent(ref.asRepoRef().toString());
    return await load(`projects/${rid}/members/${inherit ? 'all' : ''}`).catch(rethrow)
  }

  async getMembers(ref:PathRef, inherit=false) {
    let scope = (ref.repo) ? 'projects' : 'groups'
    let rid = encodeURIComponent(ref.toString().replace(/\/*$/, ''))
    return await load(`${scope}/${rid}/members/${inherit ? 'all' : ''}`).catch(rethrow)
  }

  async listGroupProjects(ref: PathRef) {
    let rid= encodeURIComponent(ref.asProjectRef().toString().trimTrailingSlashes())
    return await load(`groups/${rid}/projects?include_subgroups=true`)
  }

  /// projects ///

  async createProject(path) : Promise<GitlabProject> {
    let components = path?.split('/')
    assert(components?.length>1, 'invalid project path')
    let g = await this.getGroup(components.slice(0, components.length-1).join('/'))
    let p = components.last()
    return await xrequest(post('projects', {path: p, namespace_id: g.id}))
  }

  async getProject(path) : Promise<GitlabProject> {
    return await xrequest(get(`projects/${encodeURIComponent(path)}`)).catch(err => {
      if (/not found/i.test(err.message)) return undefined
      else rethrow(err)
    })
  }

  async makeProject(path) : Promise<GitlabProject> {
    let found = await this.getProject(path)
    if (found) return found

    let components = path?.split('/').filter(x=>x)
    assert(components.length>1, `invalid project path: ${path}`)

    await this.makeGroup(components.slice(0, components.length - 1).join('/'))

    return await this.createProject(path).catch(err => {
      if (/already/i.test(err.message)) return this.getProject(path).then()
      else rethrow(err)
    })
  }

  async updateProject(path, settings:any) {
    await xrequest(put(`projects/${encodeURIComponent(path)}`, settings))
  }

  async listProjects() : Promise<GitlabProject[]> {
    return await load('projects')
  }

  async deleteProject(path) {
    await xrequest(del(`projects/${encodeURIComponent(path)}`)).catch(rethrow)
  }

  async uploadFile(path, file, data) : Promise<string> {
    let x = await request(upload(`projects/${encodeURIComponent(path)}/uploads`, file, data))
    return x.url
  }


  /// users ///

  async createUser(username, email, name?) : Promise<GitlabUser> {
    return await xrequest(post('users', {
      username: username,
      name: name || username,
      email: email,
      reset_password: true,
      skip_confirmation: true,
    } as GitlabUser))
  }

  async getUser(username) : Promise<GitlabUser> {
    let found = await xrequest(get(`users`, { username: username }))
    if (found.length > 1) throw fail(`Multiple users matching single username: ${username}, ${found}`)
    return found.first()
  }

  async findUserByEmail(email) : Promise<GitlabUser> {
    let found = await xrequest(get(`users`, { search: email }))
    if (found.length > 1) throw fail(`Multiple users matching search query: ${email}, ${found}`)
    return found.first()
  }

  async updateUser(u: GitlabUser) : Promise<GitlabUser> {
    return xrequest(put(`users/${u.id}`, u))
  }

  async makeUser(username, email, name?) {
    let found = await this.getUser(username)
    return (
      found
        ? await this.updateUser(extend(true, found, <GitlabUser>{ email: email }))
        : await this.createUser(username, email, name)
    )
  }

  async deleteUser(username) {
    let found = await this.getUser(username)
    if (!found) throw fail(`User not found: ${username}`)
    await xrequest(del(`users/${found.id}`, { hard_delete: true })).catch(rethrow)
  }

  async listUsers() : Promise<GitlabUser[]> {
    return await load('users')
  }

  async deleteIdentity(u:GitlabUser, provider:string) {
    await xrequest(del(`users/${u.id}/identities/${provider}`));
  }

  async addEmail(u:GitlabUser, email:string) {
    await xrequest(post(`users/${u.id}/emails`, { email: email, skip_confirmation: true }))
  }

  /// group/project membership ///

  async addMember(username, path, access: GitlabAccess = GitlabAccess.Developer) : Promise<GitlabMember> {
    let user = await this.getUser(username) || fail(`No such user: ${username}`)
    let found : any = await this.getProject(path) || await this.getGroup(path) || fail(`No such group or project: ${path}`)
    let kind = found.projects ? 'groups' : 'projects'

    let encoded = encodeURIComponent(path)
    let opts = { access_level: access }
    return await xrequest(post(`${kind}/${encoded}/members`, extend({ user_id: user.id }, opts)))
      .catch(async (err) => /already exists/i.test(err.message)
        ? await xrequest(put(`${kind}/${encoded}/members/${user.id}`, opts))
        : rethrow(err))
  }

  async removeMember(username, path) {
    let user = await this.getUser(username) || fail(`No such user: ${username}`)
    let found : any = await this.getProject(path) || await this.getGroup(path) || fail(`No such group or project: ${path}`)
    let kind = found.projects ? 'groups' : 'projects'
    await xrequest(del(`${kind}/${encodeURIComponent(path)}/members/${user.id}`))
  }

  async listMembers(path, all:boolean=false) : Promise<GitlabMember[]> {
    let found : any = await this.getProject(path) || await this.getGroup(path) || fail(`No such group or project: ${path}`)
    let kind = found.projects ? 'groups' : 'projects'
    return await xrequest(get(`${kind}/${encodeURIComponent(path)}/members${all ? '/all' : ''}`))
  }

  /// user identities ///

  async addExternalIdentity(username, provider, dn) {
    let user = await this.getUser(username)
    await xrequest(put(`users/${user.id}`, {
      extern_uid: dn,
      provider: provider,
    }))
  }

  async deleteExternalIdentity(username, provider) {
    let user = await this.getUser(username)
    await xrequest(del(`users/${user.id}/identities/${provider}`))
  }

  /// impersonation tokens ///

  async createImpersonationApiToken(username) : Promise<ImpersonationToken> {
    let user = await this.getUser(username)
    return await xrequest(post(`users/${user.id}/impersonation_tokens`, {
      user_id: user.id,
      name: username,
      "scopes[]": "api",
      expires_at: moment().add(365, 'days').format('YYYYMMDD'),
    }))
  }

  async getImpersonationApiToken(username, name?) : Promise<ImpersonationToken> {
    let user = await this.getUser(username)
    let found = await xrequest(get(`users/${user.id}/impersonation_tokens`))
    name = name ?? username;
    return found.filter(x => x.name == name && x.active && !x.revoked).first();
  }

  async listImpersonationApiTokens(username) : Promise<ImpersonationToken[]> {
    let user = await this.getUser(username)
    return await xrequest(get(`users/${user.id}/impersonation_tokens`))
  }

  async revokeImpersonationApiToken(username, token)  {
    let user = await this.getUser(username)
    let found : ImpersonationToken[] = await xrequest(get(`users/${user.id}/impersonation_tokens`, {
      state: "active",
    }))
    await found
      .filter(x => x.name == token && !x.revoked)
      .forEachAsync(x => xrequest(del(`users/${user.id}/impersonation_tokens/${x.id}`)))
  }

  async makeImpersonationApiToken(username, name?) : Promise<ImpersonationToken> {
    let user = await this.getUser(username)
    let found = await xrequest(get(`users/${user.id}/impersonation_tokens`))
    name = name ?? username;
    return found.filter(x => x.name == name).first();
  }

  /// ssh keys ///

  async createUserSshKey(username, sshkey) {
    let user = await this.getUser(username)
    let result = await xrequest(post(`users/${user.id}/keys`, {
      title: username,
      key: sshkey,
    }))
  }

  async listUserSshKeys(username) : Promise<SshKey[]> {
    let user = await this.getUser(username)
    return await xrequest(get(`users/${user.id}/keys`));
  }

  async deleteUserSshKey(username, title?) {
    let user = await this.getUser(username)
    let list : SshKey[] = await xrequest(get(`users/${user.id}/keys`));
    title = title ?? username;
    await list.forEachAsync(async (x) => {
      if (x.title == title) await xrequest(del(`users/${user.id}/keys/${x.id}`));
    });

  }

  /// variables ///

  async createVariable(path, key, value, masked = false) {
    let kind = await this.getProject(path) ? "projects" : "groups";
    await xrequest(post(`${kind}/${encodeURIComponent(path)}/variables`, <GVariable>{
      key: key,
      value: value,
      masked: masked,
    }));
  }

  async updateVariable(path, key, value, masked = false) {
    let kind = await this.getProject(path) ? "projects" : "groups";
    await xrequest(put(`${kind}/${encodeURIComponent(path)}/variables/${key}`, <GVariable>{
      key: key,
      value: value,
      masked: masked,
    }));
  }

  async makeVariable(path, key, value, masked = false) {
    let kind = await this.getProject(path) ? "projects" : "groups";
    let found = await this.getVariable(path, key);
    let method = found ? "PUT" : "POST";
    let uri = `${kind}/${encodeURIComponent(path)}/variables`
    if (found) uri+=`/${key}`
    await xrequest(req(uri, method, {}, <GVariable>{
      key: key,
      value: value,
      masked: masked,
    }))
  }

  async getVariable(path, key) : Promise<string> {
    let kind = await this.getProject(path) ? "projects" : "groups";
    let found = await xrequest(get(`${kind}/${encodeURIComponent(path)}/variables/${key}`)).catch(err => {
      if (/not found/i.test(err.message)) return undefined
      else rethrow(err)
    })
    return found?.value;
  }

  async listVariables(path) : Promise<GVariable[]> {
    let kind = await this.getProject(path) ? "projects" : "groups";
    return await xrequest(get(`${kind}/${encodeURIComponent(path)}/variables`))
  }

  async deleteVariable(path, key) {
    let kind = await this.getProject(path) ? "projects" : "groups";
    await xrequest(del(`${kind}/${encodeURIComponent(path)}/variables/${key}`));
  }

  /// branches ///

  async createBranch(ref:PathRef, name, src) {
    await xrequest(post(`projects/${encodeURIComponent(ref.asRepoRef().toString())}/repository/branches`, {
      branch: name,
      ref: src,
    }))
  }

  async getBranch(ref:PathRef) {

  }

  /// repo files ///

  async createRepositoryFile(ref:PathRef, filePath:string, content, message) {
    await xrequest(post(`projects/${encodeURIComponent(ref.asRepoRef().toString())}/repository/files/${encodeURIComponent(filePath)}`, {
      branch: ref.branch,
      file_path: filePath,
      content: content,
      commit_message: message,
    }))
  }


  /// merge requests ///

  async createMergeRequest(ref:PathRef, targetBranch, title, description) {
    await xrequest(post(`projects/${encodeURIComponent(ref.asRepoRef().toString())}/merge_requests`, {
      source_branch: ref.branch,
      target_branch: targetBranch,
      title: title,
      description: description,
    }))
  }

  async addMergeRequestComment(project, mr, body) {
    await xrequest(post(`projects/${encodeURIComponent(project)}/merge_requests/${mr}/notes`, {
      body: body
    }))
  }

  async resolveMergeRequest(project, branch) {
    let mrs = await xrequest(get(`projects/${encodeURIComponent(project)}/merge_requests`, {
      source_branch: branch,
      order_by: 'updated_at',
    }))
    // looking for open MR, there can be only one
    let found = mrs.find(x => x.state === 'opened')
    if (found) return found;
    // if all MRs are closed, find most recent
    found = mrs.find(x => x.state === 'closed')
    return found
  }

  /**
   *
   * @param project
   * @param branch
   * @param mrid optional, resolved automatically
   */
  async updateSonarBadgesInMergeRequestDescription(project, branch, mrid?) {

    let ref = PathRef.parse(`${project}:${branch}`)

    if (!mrid) {
      let mr = await this.resolveMergeRequest(project, branch)
      mrid = mr?.iid
    }

    if (!mrid) {
      log.error(`No MR found for ${ref.toString()}`)
      return
    }

    let sonar = new Sonar()

    await sonar.updateProjectVisibility(ref, SonarProjectVisibility.private).catch(report)

    let sonarProject = await sonar.getProject(ref)

    if (!sonarProject) fail(`No such project in Sonar: ${ref}`)

    //let mrlink = await sonar.getProjectLink(ref, "merge request")
    let uri=`projects/${encodeURIComponent(ref.asRepoRef().toString())}/merge_requests/${mrid}`
    let mr : any = await xrequest(get(uri));

    log.info(`Resolved merge request: ${mr?.web_url}`)

    let metrics=[
      "alert_status",
      "sqale_rating",
      "reliability_rating",
      "security_rating",
      "coverage",
      "bugs",
      "code_smells",
      "vulnerabilities",
    ]

    let raw = mr.description
      //.replace(new RegExp(`.*(${metrics.map(x => `metric=${x}`).join("|")}).*`, "g"), "")
      .replace(new RegExp(`.*/api/project_badges/measure.*`, "g"), "")
      .replace(new RegExp(`.*(${metrics.map(x=>`${x}.svg`).join('|')}).*`, "g"), "")
      .trim();

    let desc=''

    if (sonarProject.visibility !== SonarProjectVisibility.public) {
      await sonar.updateProjectVisibility(ref, SonarProjectVisibility.public)
    }

    try {
      let sonarProjectId=sonar.sonarObjectId2(ref)
      let uploadsByMetric = new Map<string,string>()
      await metrics.forEachAsync(async (x) => {
        let svg = await sonar.getBadge(ref, x);
        if (/ERROR/.test(svg)) {
          log.error(`Invalid sonar badge: metric=${x}`)
          return
        }
        log.info(`Uploading ${x}.svg`)
        let upload = await this.uploadFile(project, `${x}.svg`, svg)
        uploadsByMetric.set(x, upload)
      })
      metrics.forEach(x => {
        let metric = uploadsByMetric.get(x)
        if (!metric) return
        desc += `[![${x}](${uploadsByMetric.get(x)})](${cfg.sonar.url}/dashboard?id=${sonarProjectId})\n`
      })
    } finally {
      //if (sonarProject.visibility == SonarProjectVisibility.private)
        await sonar.updateProjectVisibility(ref, SonarProjectVisibility.private).catch(report)
    }

    desc = `${desc.trim()}\n\n${raw}`

    if (desc.localeCompare(mr.description.trim()) != 0)
      await xrequest(put(uri, { description: desc}))

    return
  }

  /// k8s cluster

  async createGroupCluster(path, name, url, token) {
    return this.updateGroupCluster(path, name, url, token, true)
  }

  async updateGroupCluster(path, name, url, token, create = false) {
    let method = create ? "POST" : "PUT"
    let found = create ? await this.getGroupCluster(path,"k8s") : undefined;
    await xrequest(req(`groups/${encodeURIComponent(path)}/clusters/user`, method, {}, <KCluster>{
      cluster_id: create ? undefined : found?.cluster_id,
      name: "k8s",
      "platform_kubernetes_attributes[api_url]": url,
      "platform_kubernetes_attributes[token]": token,
      "platform_kubernetes_attributes[authorization_type]": "unknown_authorization",
      environment_scope: "*",
      managed: false,
    }))
  }

  async getGroupCluster(path, name) : Promise<KCluster> {
    let all : KCluster[] = await xrequest(get(`groups/${encodeURIComponent(path)}/clusters`));
    return all.filter(x => x.name == name).first();
  }

  async listGroupClusters() {}
  async deleteGroupCluster() {}

}

async function load(uri) : Promise<any[]> {
  let result = [];
  let opts = req(uri, 'GET', {}, {}, { resolveWithFullResponse: true });
  while (true) {
    let response = await xrequest(opts).catch(rethrow);
    let next = response.headers["x-next-page"];
    result = result.concat(response.body);
    if (next.length === 0) break;
    opts.qs.page = next;
  }
  return result;
}

function req(uri, method = 'GET', query = {}, data = {}, options = {}) : any {
  return extend(
    true,
    {
      url: `${cfg.gitlab.url}/api/v4/${uri}`,
      //qs: { page: 1, per_page: 100 },
      headers: { 'Private-Token': cfg.gitlab.token },
      strictSSL: false,
      method: method,
      json: true,
    },
    options,
    { qs: query },
    { formData: Object.keys(data).length > 0 ? copyAsStrings(data) : undefined }
  );
}

function upload(uri, filename, data) : any {
  return extend(
    true,
    {
      url: `${cfg.gitlab.url}/api/v4/${uri}`,
      method: 'POST',
      headers: { 'Private-Token': cfg.gitlab.token },
      formData: {
        file: {
          value: data,
          options: {
            filename: filename,
            contentType: 'image/svg'
          }
        }
      },
      json: true
    }
  )
}

function get(uri, query?) {
  return req(uri, 'GET', query);
}

function post(uri, data) {
  return req(uri, 'POST', {}, data);
}

function put(uri, data) {
  return req(uri, 'PUT', {}, data);
}

function del(uri, query?) {
  return req(uri, 'DELETE', query);
}

function copyAsStrings(src, dst = {}) {
  Object.keys(src)
    .filter(k => src[k])
    .forEach(k => dst[k] = String(src[k]));
  return dst;
}
