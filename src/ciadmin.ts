import {cfg, Configuration, Group, Repository} from "./cfg";
import {Gitlab, GitlabAccess, GitlabMember} from "./gitlab";
import {Sonar, SonarGlobalPermission} from "./sonar";
import {fail, ignore, parseUrl, report, rethrow, sleep} from "./utils";
import {Ldap} from "./ldap";
import {
  Nexus,
  NGroupRepo,
  NHostedRepo, NLdapServer,
  NProxyRepo,
  NRepo,
  NRepoFormat,
  NRepoStorage,
  NRepoType,
  NRoutingRule,
  NS3BlobStore
} from "./nexus";
import extend from "extend";
import * as k8s from '@kubernetes/client-node'
import {spawnSync} from 'child_process'
import * as fs from 'fs'
import {log} from "./log";
import {Rancher} from "./rancher";
import {Elastic} from "./elastic";
import {Kibana} from "./kibana";

import tmp from "tmp";

/***
 * represents group/project/repo/branch reference
 */
export class PathRef {

  group ?: string
  project ?: string
  repo ?: string
  branch ?: string

  constructor(ref: Partial<PathRef> = {}) {
    Object.assign(this, ref)
  }

  /**
   * Parses a single string reference.
   *
   * Types of references:
   * - group
   * - project
   * - repo
   * - branch
   *
   * Examples:
   * - `group/subgroup/project/repository:branch`
   * - `group/subgroup/project/repository`
   * - `group/subgroup/project/`
   * - `group/subgroup//`
   * - `group//`
   *
   * Notes:
   * - Trailing "/" is needed to disambiguate project reference
   * - Trailing "//" is needed to disambiguate group reference (especially complex ones; simple top level group name does not need trailing "//")
   *
   * Syntax:
   * - `group[/[subgroup/,...]/[project/[repository[:branch]]]]`
   * @param path
   */
  static parse(path:string) : PathRef {

    path = path
      .replace(/^\/+/, '') // trim leading slash
      .replace(/\/+(?!$)/g, '/') // normalize/deduplicate slashes, except trailing ones
      .trim()

    if (path.length == 0) throw new Error('Invalid path (missing or empty)')
    if (path.indexOf('/') == -1) path+='/' // make this a project reference

    let tokens = path.split('/')
    let rb = tokens.last().split(':') //repo with branch

    return new PathRef({
      group: tokens.slice(0, Math.max(0, tokens.length - 2)).join('/').trimToUndefined(),
      project: tokens[tokens.length - 2]?.trimToUndefined(),
      repo: rb.first().trimToUndefined(),
      branch: rb.length > 1 ? rb[1].trimToUndefined() : undefined,
    })




    // // if this is simple top level group reference, normalize it for regexp matching
    // // this prevents adding extra complexity needed to handle this case in regexp below
    // if (path.indexOf("/") == -1) path+="//"
    //
    // // https://regex101.com/r/UJfPiQ/2/
    // let re = /(?<g>[^:]+)\/(?<p>[^/:]+)?\/(?:(?<r>[^/:]+)(?::(?<b>[^/:]+))?)?/
    // let matches = path.match(re)
    // let ref = new PathRef({
    //   group: matches?.[1]?.trimToUndefined(/:+|\/+$/),
    //   project: matches?.[2]?.trimToUndefined(/[/:]+/),
    //   repo: matches?.[3]?.trimToUndefined(/[/:]+/),
    //   branch: matches?.[4]?.trimToUndefined(/[/:]+/),
    // })
    //
    // let s = ref.toString()
    // if (path != s) throw Error(`Suspicious parsing result: input: "${path}", result: ${s}`)
    //
    // return ref
  }

  static parseGroup(path:string) : PathRef {
    if (!path.endsWith('/')) path+='/'
    return this.parse(path)
  }

  static fromGroup(g:Group) : PathRef {
    let all : string[] = []
    for (let x : Group|undefined = g; x; x = x.parent) all.push(x.name)
    let path = all.reverse().join('/')
    return new PathRef({ group: path })
  }

  getParentGroup() : string|undefined {
    let components = this.group?.split('/') || []
    return components.slice(0, components.length - 1).join('/').trimToUndefined()
  }

  getParent() : PathRef|any {
    if (!this.repo && !this.project) {
      // up to parent group
      let p = this.getParentGroup()
      return p ? new PathRef({ group: p }) : undefined
    } else {
      let p = new PathRef({
        group: this.group,
        project: this.repo ? this.project : undefined,
        repo: this.branch ? this.repo : undefined,
      })
      return p.group || p.project || p.repo ? p : undefined;
    }
  }

  asGroupRef() : PathRef {
    return new PathRef({group: this.group })
  }

  asProjectRef() : PathRef {
    return new PathRef({group: this.group, project: this.project })
  }

  asRepoRef() : PathRef {
    return new PathRef({group: this.group, project: this.project, repo: this.repo })
  }

  asBranchRef() : PathRef {
    return new PathRef(this)
  }

  resolveRepoRef() : Repository|undefined {
    return cfg.groups?.flatMap(g=>g.projects?.flatMap(p=>p.repositories))
      .find(r => r?.name == this.repo && r?.project == this.project)
  }

  static getGroupPath(g:Group) : string {
    return PathRef.fromGroup(g).group as string
  }



  toString() : string {
    let gprefix = this.group ? `${this.group}/` : ''
    return (
      this.branch ? `${gprefix}${this.project}/${this.repo}:${this.branch}` :
      this.repo ? `${gprefix}${this.project}/${this.repo}` :
      this.project ? `${gprefix}${this.project}/` :
      this.group ? `${gprefix}/` :
      fail(`Invalid: ${JSON.stringify(this)}`)
    )
  }
}

export enum Role {
  Maintainer,
  Developer,
}

export class CIAdmin {

  ldap = new Ldap()
  gitlab = new Gitlab()
  sonar = new Sonar()
  nexus = new Nexus()
  kibana = new Kibana()
  elastic = new Elastic()

  async updateGitlabUsers() {
    let ldap = this.ldap
    let gitlab = this.gitlab
    try {
      await ldap.connect()
      let src = await ldap.listUsers()
      let dst = await gitlab.listUsers()
      let srcNames = src.map(x => x.sAMAccountName)
      let dstNames = dst.map(x => x.username)
      let existing = srcNames.filter(x => dstNames.contains(x))
      let todo = srcNames.filter(x => !dstNames.contains(x))
      log.info('Found %d LDAP users, %d Gitlab users. Already registered: %d, new: %d', src.length, dst.length, existing.length, todo.length)
      await src
        .filter(x => todo.contains(x.sAMAccountName))
        .forEachAsync(async lu => {
          log.info('Regitering new LDAP user in Gitlab: %s (%s <%s>)', lu.sAMAccountName, lu.cn, lu.mail)
          await gitlab.makeUser(lu.sAMAccountName, lu.mail, lu.cn)
        })


    } finally {
      ldap.disconnect().then()
    }
  }

  async updateSonarUsers() {
    await this.gitlab.listUsers().then(async users => {
      await users.forEachAsync(async u => {
        let x = await this.sonar.makeUser(u.username, u.email, u.name)
        if (x.externalIdentity && x.externalProvider) return
        // in sonar database, external_login and external_id are affected
        // functionally, external_id is important here
        // api does not allow setting both so we're resorting to external_id
        await this.sonar.updateUserIdentityProvider(x.login, u.id, "gitlab") //todo: hardcoded provider
      })
    })
  }

  async updateSonarUserGroups() {
    await this.updateSonarUsers()
    await this.sonar.listUserGroups().then(async groups =>
      await groups
        .filter(x => Sonar.groupRegex.test(x.name))
        .forEachAsync(async (g) => await this.updateSonarUserGroup(g.name)));
  }

  async updateSonarUserGroup(group:string) {

    if (! Sonar.groupRegex.test(group)) {
      log.warn(`Skipping ${group}. It does not refer to a CIAdmin managed user group.`)
      return
    }

    let gitlabPath = group
      .replace(/:(owners|members)$/, '')
      .replace(/:/g, '/')
    let isOwner = group.endsWith(`:${Sonar.ownersGroupSuffix}`)
    let isMember = group.endsWith(`:${Sonar.membersGroupSuffix}`)
    let ownerFilter = (m:GitlabMember) => m.access_level >= GitlabAccess.Maintainer
    let memberFilter = (m:GitlabMember) => m.access_level < GitlabAccess.Maintainer

    let gitlab = this.gitlab
    let sonar = this.sonar

    // Sonar group may refer to Gitlab group or project
    let isGroup = await gitlab.getGroup(gitlabPath).then(x => !!x)
    let isProject = !isGroup && await gitlab.getProject(gitlabPath).then(x => !!x)

    if (!isGroup && !isProject) {
      log.warn(`Skipping ${group} as it does not refer to an existing Gitlab group or project.`)
      return
    }

    let ref = PathRef.parse(`${gitlabPath}${isGroup ? '/' : ''}`)

    // source: Gitlab group members, filtered by access level appropriate for target Sonar group
    let gitlabMembers = await gitlab.getMembers(ref, false)
      .then(x => x.filter(m => isOwner && ownerFilter(m) || isMember && memberFilter(m)))
      .catch(err => /404/.test(err.message) ? [] : rethrow(err))

    // current list of group members in Sonar
    let sonarMembers = await sonar.listGroupMembers(group)

    log.info(`[%s] %d/%d gitlab/sonar users`, group, gitlabMembers.length, sonarMembers.length)

    // for every Gitlab member that does not yet exist in corresponding Sonar group, add user to Sonar group
    let add = gitlabMembers.filter(x => !sonarMembers.find(m => m.login == x.username))

    // usernames of all sonar users originating from gitlab
    let usernames = await sonar.listUsers()
      .then(x => x.filter(x => x.externalProvider == 'gitlab'))
      .then(x => x.map(u => u.login))

    // remove every Sonar group member that does not exist in source Gitlab group
    let del = sonarMembers
      .filter(x => !gitlabMembers.find(m => m.username == x.login))
      .filter(x => usernames.contains(x.login)) // only gitlab users

    if (add.length > 0 || del.length > 0)
      log.info('[%s] : adding [%s], removing [%s]',
        group,
        add.map(x => x.username).join(','),
        del.map(x => x.login).join(',')
      )

    await add.forEachAsync(async (m) => await sonar.addGroupMember(m.username, group))
    await del.forEachAsync(async x => await sonar.removeGroupMember(x.login, group))

  }

  async updateElasticUsers() {
    await this.kibana.syncUsers()
  }

  async configureNexusRepositories() {

    // await this.nexus.listRepositories().then(async repos => repos.forEachAsync(async r => {
    //   await this.nexus.deleteRepository(r.name).catch(ignore)
    // }))

    await cfg.nexus.deleteRepositories?.forEachAsync(async x => {
      while (await this.nexus.getRepository(x)) {
        await this.nexus.deleteRepository(x).catch(ignore)
        await sleep(1000)
      }
    })

    let repocfg = cfg.nexus.repositories;
    let repos = [] as NRepo[]
    let blobstores = [] as NS3BlobStore[]
    let routingRules = [] as NRoutingRule[]
    (Object.keys(repocfg) as NRepoFormat[]).forEach(format => {
      (Object.keys(repocfg[format]) as NRepoType[]).forEach(type => {
        Object.keys(repocfg[format][type]).forEach(name => {
          let rcfg = repocfg[format][type][name]
          let bs = new NS3BlobStore({
            name: name,
            bucketConfiguration: {
              bucket: {name: `${cfg.nexus.bucketNamePrefix}${name}`},
              bucketSecurity: {
                accessKeyId: cfg.s3.accessKey,
                secretAccessKey: cfg.s3.secretKey,
              },
              advancedBucketConnection: {
                endpoint: cfg.s3.internalUrl,
                forcePathStyle: true
              }
            }
          })
          blobstores.push(bs)
          let x = <NRepo>{ name: name, format: format, storage: new NRepoStorage({ blobStoreName: name }) }
          switch (type) {
            case NRepoType.hosted:
              repos.push(new NHostedRepo(extend(x, rcfg.options)));
              break
            case NRepoType.proxy:
              let rr = new NRoutingRule({name: name, matchers: rcfg.routing});
              routingRules.push(rr);
              repos.push(new NProxyRepo(extend(x,
                <NProxyRepo>{ proxy: { remoteUrl: rcfg.url }, routingRule: name }, rcfg.options )))
              break;
            case NRepoType.group:
              repos.push(new NGroupRepo(extend(x, <NGroupRepo>{ group: { memberNames: rcfg.members } }, rcfg.options)))
              break
          }
        })
      })
    })
    await blobstores.forEachAsync(async (bs) => await this.nexus.makeBlobStore(bs))
    await routingRules.forEachAsync(async (rr) => await this.nexus.makeRoutingRule(rr))
    await repos
      .filter(x => x.type != NRepoType.group)
      .forEachAsync(async (r) => await this.nexus.makeRepository(r).catch(report))
    await repos
      .filter(x => x.type == NRepoType.group)
      .forEachAsync(async (r) => await this.nexus.makeRepository(r).catch(report))
  }

  async configureNexusAuth() {
    let ldap = parseUrl(cfg.ldap.url)
    await this.nexus.makeLdapServer(new NLdapServer({
      name: cfg.ldap.domain,
      host: ldap.hostname,
      port: +ldap.port,
      protocol: ldap.protocol.replace(":", "").toUpperCase(),
      searchBase: cfg.ldap.baseDN,
      authScheme: 'simple',
      authUsername: cfg.ldap.username,
      authPassword: cfg.ldap.password,
      userBaseDn: cfg.ldap.users.base,
      userLdapFilter: cfg.ldap.users.filter,
    })).catch(report)
  }

  async setupProject() {

    let gitlab = new Gitlab()
    let sonar = new Sonar()
    let rancher = new Rancher()
    let elastic = new Elastic()
    let kibana = new Kibana()

    let cfg = Configuration.instance;

    let project = cfg.cisetup.project.path;
    let username = cfg.cisetup.project.username;
    let namespace = cfg.cisetup.project.namespace;

    let ref = PathRef.parse(`${project}/`)

    log.info(`Create/Update Gitlab project ${project}, user ${username}, configure membership`)

    await gitlab.makeGroup(project)
    await gitlab.makeUser(username, `${username}@example.com`)
    await gitlab.addMember(username, project, GitlabAccess.Maintainer)

    let projects = (<string[]>[]).concat([project]).concat(cfg.cisetup.project.member);
    await projects.forEachAsync(async (x) =>
      await this.gitlab.addMember(username, x, GitlabAccess.Maintainer))

    // mappings for generated variables
    let mappings = cfg.cisetup.mappings;

    log.info(`Check/Create/Regenerate Gitlab API token for user ${username}`)

    // user: create api token
    let gitlabApiToken = await this.gitlab.getImpersonationApiToken(username);
    if (gitlabApiToken) {
      // token exists, is something in variables? actual value cannot be verified
      gitlabApiToken.token = await this.gitlab.getVariable(project, mappings.gitlabApiToken)
    }
    // no token? revoked? lost token? generate new
    if (!gitlabApiToken || gitlabApiToken.revoked || !gitlabApiToken.token) {
      gitlabApiToken = await this.gitlab.createImpersonationApiToken(username);
    }

    // user: add ssh key

    log.info(`Configure SSH key for user ${username}`)

    let sshPrivate = await gitlab.getVariable(project, mappings.gitSshPrivateKey)
    if (sshPrivate) {
      log.info(`SSH private key already present in ${mappings.gitSshPrivateKey}. To regenerate, delete variable.`)
    } else {
      let sshKeygen = spawnSync(cfg.shell, ['-c', `
      rm ${username}.key ${username}.pub &>/dev/null
      ssh-keygen -f ${username} -C ${username} -N "" &>/dev/null
      mv ${username} ${username}.key
      `])
      if (sshKeygen.status != 0) log.error(sshKeygen.stderr.toString())
      sshPrivate = fs.readFileSync(`${username}.key`)?.toString()
      let sshPublic = fs.readFileSync(`${username}.pub`)?.toString()
      await this.gitlab.deleteUserSshKey(username)
      await this.gitlab.createUserSshKey(username, sshPublic)
      await this.gitlab.makeVariable(project, mappings.gitSshPrivateKey, sshPrivate)
    }


    // sonar user

    log.info(`Propagate user ${username} into Sonar ${cfg.sonar.url}`)

    let sonarUser = await sonar.makeUser(username)
    if (sonarUser.externalProvider !== 'gitlab')
      await sonar.updateUserIdentityProvider(username, username, 'gitlab')

    // sonar token
    log.info(`Configuring SonarQube token for user ${username}`)
    let tokenName = 'CI'
    let sonarToken = await gitlab.getVariable(project, mappings.sonarToken)
    if (sonarToken) {
      log.info(`Sonar token already registered in ${mappings.sonarToken}. To regenerate, delete variable.`)
    } else {
      log.warn(`Token not present in Gitlab variables. Revoking existing ${tokenName} token and generating new one`)
      await sonar.revokeUserToken(username, tokenName).catch(ignore)
      sonarToken = await sonar.generateUserToken(username, tokenName)
    }

    log.info(`Configuring SonarQube permissions for user ${username}`)

    // allow user to create sonar projects, and apply quality profile/gate to new project
    await sonar.addPermissionToUser(username, SonarGlobalPermission.provisioning)
    await sonar.addPermissionToUser(username, SonarGlobalPermission.profileadmin)
    await sonar.addPermissionToUser(username, SonarGlobalPermission.gateadmin)

    log.info(`Configuring SonarQube user groups`)

    // sonar objects
    await sonar.makeUserGroups(ref)
    await sonar.updateUserGroups(ref)
    await sonar.makeApplication(ref)

    log.info(`Configuring Gitlab repositories and related SonarQube objects (groups, templates, quality profiles/gates)`)

    // repositories
    let repositories = cfg.cisetup.repositories
    await repositories.forEachAsync(async (r) => {
      log.info(`- repo name: ${r.name}, languages: [${r.languages.join(',')}]`)
      let repo = PathRef.parse(`${ref.asProjectRef()}${r.name}`)
      await gitlab.makeProject(repo.toString()).catch(rethrow)
      if (r.settings && Object.keys(r.settings).length>0)
        await gitlab.updateProject(repo.toString(), r.settings)
      let languages = r.languages?.join(',')
      if (languages) await gitlab.makeVariable(repo.toString(), mappings.sonarLanguages, languages).catch(rethrow)
      await sonar.makeUserGroups(repo).catch(rethrow)
      await sonar.updateUserGroups(repo).catch(rethrow)
      await sonar.makePermissionTemplate(repo).catch(rethrow)
      await sonar.configurePermissionTemplate(repo).catch(rethrow)

      await this.prepareSonarProject(repo, undefined, r.languages)

      // await r.languages?.forEachAsync(async (l) => {
      //   await sonar.makeDefaultQualityProfile(l)
      //   await sonar.makeQualityProfileHierarchy(repo, l).catch(rethrow)
      // }).catch(rethrow)
      // await sonar.makeQualityGate(repo).catch(rethrow)
      // await sonar.makeProject(repo)
      await sonar.addApplicationProject(repo)
    }).catch(rethrow)

    // rancher project + namespace
    await rancher.makeProject(namespace)
      .then(p => rancher.makeNamespace(namespace, p))
      .catch(rethrow)
    await rancher.makeUser(username)
    // await rancher.addProjectMember(username, namespace, Role.Maintainer)

    // namespace: deploy service accounts, retrieve owner token
    let nsHelmReleaseName = cfg.cd.kubernetes.nsconfig.helmReleaseName ?? 'nsconfig'
    let nsHelmValues = cfg.cd.kubernetes.nsconfig.values ?? {}
    nsHelmValues = extend(true, nsHelmValues, {
      logging: {
        username: `${username}-app`,
        index: `${username}`,
      }
    })
    tmp.setGracefulCleanup()
    let fvalues = tmp.fileSync()
    fs.writeFileSync(fvalues.fd, JSON.stringify(nsHelmValues))
    log.info(`Configuring Kubernetes namespace and service accounts`)
    let nsHelmUpgrade = spawnSync(cfg.shell, ['-c', `
      helm upgrade -i ${nsHelmReleaseName} ./namespace \
        -n ${namespace} --create-namespace \
        -f ./namespace/local.yaml \
        -f ${fvalues.name.replace(/\\/g, '/')} \
        --debug
    `])
    log.debug(nsHelmUpgrade.stdout.toString())
    if (nsHelmUpgrade.status != 0) log.error(nsHelmUpgrade.stderr.toString())

    let kc = new k8s.KubeConfig();
    if (fs.existsSync('/var/run/secrets/kubernetes.io/serviceaccount/token')) {
      kc.loadFromDefault()
    } else {
      kc.loadFromOptions({
        clusters: [{ name: "default", server: `${cfg.rancher.url}/k8s/clusters/${cfg.ci.rancher.clusterId}` }],
        users: [{ name: "ciadmin", user: { token: cfg.rancher.token }}],
        contexts: [{ name: "ciadmin", context: { user: "ciadmin", cluster: "default" }}],
        currentContext: "ciadmin",
      })
    }

    let coreV1Api = kc.makeApiClient(k8s.CoreV1Api)
    //await coreV1Api.createNamespace({ metadata: { name: namespace }})
    // await coreV1Api.createNamespacedServiceAccount(namespace, { metadata: { name: "owner" }})
    let kubeToken ;
    while (!kubeToken) {
      let sa = await coreV1Api.readNamespacedServiceAccount("owner", namespace);
      let secretName = sa.body.secrets?.find(x => x.name != null)?.name ?? "";
      let secret = await coreV1Api.readNamespacedSecret(secretName, namespace)
      kubeToken = secret.body.data?.token;
      if (!kubeToken) await new Promise(resolve => setTimeout(resolve, 100));
    }
    kubeToken = Buffer.from(kubeToken, 'base64').toString('ascii');

    // elastic/kibana
    await elastic.setupProject({
      path: project,
      space: username,
      username: username,
    })
    await kibana.setupProject({path: project, space: namespace, username: username}).catch(report)
    await elastic.updateAppLoggingUser(
      coreV1Api, namespace, `${nsHelmReleaseName}-logging`, 'username', 'password'
    ).catch(report)

    // cleanup
    // await this.gitlab.listVariables(project).then(async x =>
    //   await x.forEachAsync(async v =>
    //     await this.gitlab.deleteVariable(project, v.key)))

    log.info(`Saving Gitlab variables in group ${project}`)

    await Promise.all([
      this.gitlab.makeVariable(project, mappings.gitlabApiToken, gitlabApiToken.token, true).catch(report),
      //this.gitlab.makeVariable(project, mappings.gitSshPrivateKey, sshPrivate).catch(report),
      this.gitlab.makeVariable(project, mappings.kubeUrl, cfg.rancher.url).catch(report),
      this.gitlab.makeVariable(project, mappings.kubeToken, kubeToken, true).catch(report),
      this.gitlab.makeVariable(project, mappings.kubeNamespace, namespace).catch(report),
      this.gitlab.makeVariable(project, mappings.sonarToken, sonarToken, true).catch(report),
    ]);

    await Object.entries(cfg.cisetup.variables).forEachAsync(async ([k,v]) => {
      if (!v) return;
      let masked = k.match(cfg.cisetup.masked) != null && !v.match(/\r?\n/gm) && !v.includes("${");
      await this.gitlab.makeVariable(project, k, v, masked).catch(e=>report(`${k}=${v} => ${e}`));
    })
  }

  async setupSonar(ref: PathRef, languages:string[], privileged=false) {
    let sonar = new Sonar()

    let repo = ref.asRepoRef()
    if (privileged) {
      await sonar.makeUserGroups(repo)
      await sonar.makePermissionTemplate(repo)
      await languages.forEachAsync(async (l) => await sonar.makeQualityProfileHierarchy(repo, l))
      await sonar.makeQualityGate(repo)
      await sonar.configurePermissionTemplate(repo)
      await sonar.updateQualityGatePermissions(repo)
    }
    await sonar.makeProject(ref)

    await languages.forEachAsync(async (l) => await sonar.makeQualityProfile(sonar.sonarObjectId2(repo), l))
    // await sonar.makeQualityGate(repo)

    // await sonar.consolidateSonarUsers()
    //await sonar.updateUserGroups(repo)

    //await sonar.setNewCodePeriod(ref)

    //await sonar.applyPermissionTemplate(ref)
    await languages.forEachAsync(async (l) => sonar.applyQualityProfile(ref, l))
    await sonar.applyQualityGate(ref)

  }

  async prepareSonarProject(ref:PathRef, mainBranch?:string, languages?:string[]) {
    let sonar = new Sonar()
    let gitlab = new Gitlab()

    if (!mainBranch) {
      let gproject = await gitlab.getProject(ref.asRepoRef())
      mainBranch = gproject?.default_branch
    }

    await sonar.makeProject(ref)
    if (mainBranch) await sonar.renameProjectMainBranch(ref, mainBranch)

    // await sonar.makeApplication(ref)
    // await sonar.addApplicationProject(ref)

    await sonar.makeQualityProfiles(ref.asRepoRef(), languages)
    await languages?.forEachAsync(async (l) => {
      await sonar.makeQualityProfileHierarchy(ref.asRepoRef(), l)
      await sonar.applyQualityProfile(ref, l)
    })
    await sonar.makeQualityGate(ref)
    await sonar.updateQualityGatePermissions(ref)
    await sonar.applyQualityGate(ref)
    await sonar.setNewCodePeriod(ref)
  }

  async configureNexusProxy() {
    let proxy = cfg.proxy
    if (!proxy) {
      log.warn('Nothing to do: missing proxu configuration!')
      return
    }
    let url = parseUrl(proxy.url)
    await this.nexus.configureHttpProxy(url.hostname, url.port, url.username, url.password)
    await this.nexus.configureNonProxyHosts(cfg.proxy?.nonProxyHosts ?? [])
  }
  async configureNexusTruststore() {
    await Object.keys(cfg.cacerts).forEachAsync(async k => {
      await this.nexus.addCertificate(cfg.cacerts[k])
    })
  }

}

