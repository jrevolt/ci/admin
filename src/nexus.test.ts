import './testutils'
import {
  NBlobStore,
  NBlobStoreType,
  Nexus,
  NGroupRepo,
  NHostedRepo, NLdapServer,
  NProxyRepo,
  NRepo,
  NRepoFormat,
  NRepoStorage,
  NRoutingRule,
  NS3BlobStore
} from "./nexus";
import {cfg} from "./cfg";
import {ignore, parseUrl} from "./utils";
import each from "jest-each";
import {createS3Client} from "./s3";

let id : string
let nexus : Nexus
let bucket: string
let blobStoreName : string
let reponame : string
let storage : NRepoStorage
let blobstore: NBlobStore

beforeEach(() => {
  jest.setTimeout(60000)

  id = new Date().getTime().toString().replace(/.*(\d{5})$/, '$1')
  nexus = new Nexus()

  bucket = `test-bucket-${id}`
  blobStoreName = `test-blobstore-${id}`
  reponame = `test-repo-${id}`
  storage = new NRepoStorage({ blobStoreName: blobStoreName })
  blobstore = NS3BlobStore.create(blobStoreName, cfg.s3.internalUrl, bucket, cfg.s3.accessKey, cfg.s3.secretKey)

})


describe('blobstores', () => {

  describe('s3', () => {

    beforeEach(async () => {
      await createS3Client().createBucket({Bucket: bucket})
    })

    afterEach(async () => {
      await nexus.deleteBlobStore(blobStoreName).catch(ignore)
      await createS3Client().deleteBucket({Bucket: bucket}).catch(ignore)
    })

    test('create', async () => {
      await nexus.createBlobStore(blobstore)
    })

    test('get', async () => {
      await expect(nexus.getBlobStore(blobStoreName, NBlobStoreType.S3)).resolves.toBeUndefined();
      await nexus.createBlobStore(blobstore);
      await expect(nexus.getBlobStore(blobStoreName, NBlobStoreType.S3)).resolves.toBeInstanceOf(NS3BlobStore);
    })

    test('update', async () => {
      await expect(nexus.updateBlobStore(blobstore)).rejects.toThrow(/no.*blob store/i);
      await nexus.createBlobStore(blobstore);
      await expect(nexus.updateBlobStore(blobstore)).resolves.toBeUndefined();
    })

    test('delete', async () => {
      await expect(nexus.deleteBlobStore(blobStoreName)).rejects.toThrow(/404/i)
      await nexus.createBlobStore(blobstore);
      await expect(nexus.deleteBlobStore(blobStoreName)).resolves.toBeUndefined()
    })

    test('list', async () => {
      await nexus.createBlobStore(blobstore)
      await expect(nexus.listBlobStores()).resolves.toBeNonEmptyArray();
    })
  });
})

const formats = [
  NRepoFormat.npm,
  NRepoFormat.nuget,
  NRepoFormat.docker,
]

each(formats).describe('repositories: %s', (format) => {

  beforeEach(async () => {
    await createS3Client().createBucket({Bucket: bucket})
    await nexus.createBlobStore(blobstore);
  })

  afterEach(async () => {
    await nexus.deleteRepository(reponame).catch(ignore)
    await nexus.deleteBlobStore(blobStoreName).catch(ignore)
    await createS3Client().deleteBucket({Bucket: bucket}).catch(ignore)
  })

  describe(`common`, () => {

    let repo : NHostedRepo

    beforeEach(async () => {
      repo = new NHostedRepo({
        name: reponame,
        format: format,
        storage: storage,
      })
    })

    test(`get`, async () => {
      await expect(nexus.getRepository(reponame)).resolves.toBeUndefined();
      await nexus.createRepository(repo);
      await expect(nexus.getRepository(reponame)).resolves.toBeInstanceOf(NHostedRepo);
    })

    test(`list`, async () => {
      await expect(nexus.listRepositories()).resolves.toBeInstanceOf(Array)
      await nexus.createRepository(repo);
      await expect(nexus.listRepositories()).resolves.toBeNonEmptyArray();
    })

    test(`delete`, async () => {
      await expect(nexus.deleteRepository(reponame)).rejects.toThrow(/404/i);
      await nexus.createRepository(repo);
      await expect(nexus.deleteRepository(reponame)).resolves.toBeUndefined()
    })
  })

  describe('hosted', () => {

    let repo : NHostedRepo

    beforeEach(async () => {
      repo = new NHostedRepo({
        name: reponame,
        format: format,
        storage: storage,
      })
    })

    test('create', async () => {
      await nexus.createRepository(repo)
    })

    test('update', async () => {
      await expect(nexus.updateRepository(repo)).rejects.toThrow(/404/i);
      await nexus.createRepository(repo);
      await expect(nexus.updateRepository(repo)).resolves.toBeInstanceOf(NRepo)
    })
  })

  describe('proxy', () => {

    let repo : NProxyRepo

    beforeEach(async () => {
      repo = new NProxyRepo({
        name: reponame,
        format: format,
        storage: storage,
        proxy: {
          remoteUrl: 'http://registry.npmjs.org',
          contentMaxAge: 0,
          metadataMaxAge: 0,
        },
      })
    })

    test('create', async () => {
      await nexus.createRepository(repo)
    })

    test('update', async () => {
      await expect(nexus.updateRepository(repo)).rejects.toThrow(/404/i);
      await nexus.createRepository(repo);
      await expect(nexus.updateRepository(repo)).resolves.toBeInstanceOf(NProxyRepo)
    })

  })

  describe('group', () => {

    let repo : NGroupRepo
    let member : NHostedRepo

    beforeEach(async () => {
      member = new NHostedRepo({
        name: `${reponame}-member`,
        format: format,
        storage: storage,
      })
      repo = new NGroupRepo({
        name: reponame,
        format: format,
        storage: storage,
        group: {
          memberNames: [ member.name ]
        }
      })
      await nexus.createRepository(member);
    })

    afterEach(async () => {
      await Promise.all([
        nexus.deleteRepository(member.name)
      ]).catch(ignore)
    })

    test('create', async () => {
      await nexus.createRepository(repo)
    })

    test('update', async () => {
      await expect(nexus.updateRepository(repo)).rejects.toThrow(/404/i);
      await nexus.createRepository(repo);
      await expect(nexus.updateRepository(repo)).resolves.toBeInstanceOf(NRepo)
    })

  })

})

describe('routing rules', () => {

  let rule : NRoutingRule
  let repo : NProxyRepo

  beforeEach(() => {
    rule = new NRoutingRule({
      name: `rule-${id}`,
    })
    repo = new NProxyRepo({
      name: `repo-${id}`,
      format: NRepoFormat.npm,
      storage: storage,
      proxy: {
        remoteUrl: 'http://registry.npmjs.org',
      } as any
    })
  })

  afterEach(async () => {
    await Promise.all([
      nexus.deleteRepository(repo.name)
    ]).catch(ignore)
    await Promise.all([
      nexus.deleteRoutingRule(rule.name),
      nexus.deleteBlobStore(blobstore.name),
    ]).catch(ignore)
  })

  test('create', async () => {
    await expect(nexus.createRoutingRule(rule)).resolves.toBeInstanceOf(NRoutingRule);
  })

  test('update', async () => {
    await expect(nexus.updateRoutingRule(rule)).rejects.toThrow(/did.*n.t find.*rule/i)
    await nexus.createRoutingRule(rule);
    await expect(nexus.updateRoutingRule(rule)).resolves.toBeInstanceOf(NRoutingRule);
  })

  test('get', async () => {
    await expect(nexus.getRoutingRule(rule.name)).resolves.toBeUndefined();
    await nexus.createRoutingRule(rule);
    await expect(nexus.getRoutingRule(rule.name)).resolves.toBeInstanceOf(NRoutingRule);
  })

  test('list', async () => {
    await expect(nexus.listRoutingRules()).resolves.toBeInstanceOf(Array);
    await nexus.createRoutingRule(rule);
    await expect(nexus.listRoutingRules()).resolves.toBeNonEmptyArray();
  })

  test('assign', async () => {
    await nexus.createBlobStore(blobstore)
    await Promise.all([
      nexus.createRepository(repo),
      nexus.createRoutingRule(rule),
    ])
    repo.routingRule = rule.name
    let x = await nexus.updateRepository(repo);
    await expect(nexus.getRepository(repo.name)).resolves.toHaveProperty("routingRule", rule.name)
  })
})

describe('ldap', () => {
  test('ldap: create', async () => {
    await nexus.makeLdapServer(new NLdapServer({
      name: 'test',
      host: parseUrl(cfg.ldap.url).hostname,
      port: +parseUrl(cfg.ldap.url).port,
      searchBase: cfg.ldap.baseDN,
      authScheme: 'simple',
      authUsername: cfg.ldap.username,
      authPassword: cfg.ldap.password,
      userBaseDn: cfg.ldap.users.base,
      userLdapFilter: cfg.ldap.users.filter,
    }))
  })
})

describe('cacerts', () => {
  test('add', async () => {
    await Object.keys(cfg.cacerts).forEachAsync(async k => {
      await nexus.addCertificate(cfg.cacerts[k])
    })
  })
})

describe('proxy', () => {
  test('configure', async () => {
    let url = parseUrl(cfg.proxy?.url)
    await nexus.configureHttpProxy('localhost', 3128, 'user', 'pass')
  })
  test('nonProxyHosts', async () => {
    await nexus.configureNonProxyHosts(['.example.com', 'foo.bar'])
  })
})