import './testutils'
import {Rancher} from "./rancher";
import {ignore} from "./utils";

let rancher:Rancher

let id, pname, nsname, username

beforeEach(() => {
  rancher = new Rancher()
  id = new Date().getTime().toString().replace(/.*(\d{5})$/, '$1')
  pname = `prj-${id}`
  nsname = `ns-${id}`
  username = `user-${id}`
})

describe('clusters', () => {

  test('get', async () => {
    let x = await rancher.getCluster('local')
    return
  })

})

describe('namespaces', () => {

  afterEach(async () => {
    await Promise.all([
      rancher.deleteNamespace(nsname)
    ]).catch(ignore)
  })

  test('create', async () => {
    await expect(rancher.getNamespace(nsname)).resolves.toBeUndefined()
    await expect(rancher.createNamespace(nsname)).resolves.toBeDefined()
    await expect(rancher.createNamespace(nsname)).rejects.toThrow(/already exists/i)
  })

  test('get', async () => {
    await expect(rancher.getNamespace(nsname)).resolves.toBeUndefined()
    await expect(rancher.createNamespace(nsname)).resolves.toBeDefined()
    await expect(rancher.getNamespace(nsname)).resolves.toHaveProperty('name', nsname)
  })

  test('list', async () => {
    await expect(rancher.listNamespaces()).resolves.toBeNonEmptyArray()
    let ns = await rancher.createNamespace(nsname)
    let list = await rancher.listNamespaces()
    expect(list.find(x => x.id == ns.id)).toBeDefined()
  })

  test('delete', async () => {
    await expect(rancher.deleteNamespace(nsname)).rejects.toThrow(/not found/i)
    await rancher.createNamespace(nsname)
    await expect(rancher.deleteNamespace(nsname)).resolves.toBeUndefined()
  })
})

describe('projects', () => {

  afterEach(async () => {
    await Promise.all([
      rancher.deleteProject(pname),
      rancher.deleteNamespace(nsname),
    ]).catch(ignore)
  })

  test('create', async () => {
    await expect(rancher.getProject(pname)).resolves.toBeUndefined()
    await expect(rancher.createProject(pname)).resolves.toBeDefined()
    await expect(rancher.createProject(pname)).rejects.toThrow(/already exists/i)
  })

  test('get', async () => {
    await expect(rancher.getProject(pname)).resolves.toBeUndefined()
    await rancher.createProject(pname)
    await expect(rancher.getProject(pname)).resolves.toHaveProperty('name', pname)
  })

  test('list', async () => {
    await expect(rancher.listProjects()).resolves.toBeNonEmptyArray()
    await rancher.createProject(pname)
    let list = await rancher.listProjects()
    expect(list.find(p => p.name == pname)).toBeDefined()
  })

  test('delete', async () => {
    await expect(rancher.deleteProject(pname)).rejects.toThrow(/not found/i)
    await rancher.createProject(pname)
    await expect(rancher.deleteProject(pname)).resolves.toBeUndefined()
  })

  test('make', async () => {
    await expect(rancher.getProject(pname)).resolves.toBeUndefined()
    await expect(rancher.makeProject(pname)).resolves.toHaveProperty('name', pname)
    await expect(rancher.makeProject(pname)).resolves.toHaveProperty('name', pname)
  })
})

describe('users', () => {
  test('create', async () => {
  })
})

describe('memberships', () => {

  afterEach(async () => {
    await Promise.all([
      rancher.deleteUser(username)
    ]).catch(ignore)
  })

  test('cluster', async () => {
    await rancher.createUser(username)
    //await rancher.addClusterMember(username)
  })
})
