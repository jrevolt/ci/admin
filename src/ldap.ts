import * as config from "./cfg";
import {Attribute, Change, Client} from 'ldapts'
import {readFileSync} from 'fs'

const cfg = config.cfg

class LdapUser {
  dn: string
  cn: string
  sAMAccountName: string
  userPrincipalName: string
  mail: string
  memberOf: string[]
}

class LdapGroup {
  dn: string
  cn: string
  sAMAccountName: string
  member: string[]
}

export class Ldap {

  client : Client

  constructor() {
    let strict = cfg.ldap.tls.strict
    let ca
    if (strict) {
      if (cfg.ldap.tls.cacerts) ca = cfg.ldap.tls.cacerts
      else if (cfg.caFile) ca = readFileSync(cfg.caFile)
    }
    this.client = new Client({
      url: cfg.ldap.url,
      strictDN: strict,
      tlsOptions: {
        ca: strict ? ca : undefined,
        rejectUnauthorized: strict,
      }
    })
  }

  async connect() {
    await this.client.bind(cfg.ldap.username, cfg.ldap.password)
  }

  async disconnect() {
    await this.client.unbind()
  }

  /// users ///

  async createUser(username, group?) : Promise<LdapUser> {
    await this.client.add(makedn(`cn=${username}`, cfg.ldap.users.base), {
      cn: username,
      [cfg.ldap.users.username]: username,
      userPrincipalName: `${username}@${cfg.ldap.domain}`,
      [cfg.ldap.users.email]: `${username}@${cfg.ldap.domain}`,
      objectClass: cfg.ldap.users.class,
    })
    if (group) {
      await this.addGroupMember(username, group)
    }
    return await this.getUser(username)
  }

  async getUser(username) : Promise<LdapUser> {
    return await this.client.search(cfg.ldap.baseDN, {
      filter: `(&(${cfg.ldap.users.username}=${username})${cfg.ldap.users.filter})`,
      attributes: cfg.ldap.users.attributes,
    }).then(x => x.searchEntries.first()) as LdapUser
  }

  async findUserByEmail(email) : Promise<LdapUser> {
    return await this.client.search(cfg.ldap.baseDN, {
      filter: `(&(${cfg.ldap.users.email}=${email})${cfg.ldap.users.filter})`,
      attributes: cfg.ldap.users.attributes,
    }).then(x => x.searchEntries.first()) as LdapUser
  }

  async findUserByDN(dn) : Promise<LdapUser> {
    return await this.client.search(dn, {
      attributes: cfg.ldap.users.attributes,
    }).then(x => x.searchEntries.first()) as LdapUser
  }

  async deleteUser(username) {
    await this.client.del(makedn(`cn=${username}`, cfg.ldap.users.base))
  }
  async listUsers() : Promise<LdapUser[]> {
    return await this.client.search(cfg.ldap.baseDN, {
      filter: cfg.ldap.users.filter,
      attributes: cfg.ldap.users.attributes,
    }).then(x => x.searchEntries) as LdapUser[]
  }

  /// groups ///

  async createGroup(name) : Promise<LdapGroup> {
    await this.client.add(makedn(`cn=${name}`, cfg.ldap.groups.base), {
      [cfg.ldap.groups.name]: name,
      objectClass: cfg.ldap.groups.class,
    })
    return await this.getGroup(name)
  }

  async getGroup(name) : Promise<LdapGroup> {
    return await this.client.search(cfg.ldap.baseDN, {
      filter: `(&(${cfg.ldap.groups.name}=${name})${cfg.ldap.groups.filter})`,
      attributes: cfg.ldap.groups.attributes,
    }).then(x => x.searchEntries.first()) as LdapGroup
  }

  async deleteGroup(name) {
    await this.client.del(makedn(`cn=${name}`, cfg.ldap.groups.base))
  }

  async listGroups() {
    return await this.client.search(cfg.ldap.baseDN, {
      filter: cfg.ldap.groups.filter,
      attributes: cfg.ldap.groups.attributes,
    }).then(x => x.searchEntries)
  }

  /// membership

  async addGroupMember(user, group) {
    await this.client.modify(makedn(`cn=${group}`, cfg.ldap.groups.base), new Change({
      operation: 'add',
      modification: new Attribute({
        type: 'member', values: [ makedn(`cn=${user}`, cfg.ldap.users.base) ]
      }),
    }))
  }

  async removeGroupMember(user, group) {
    await this.client.modify(makedn(`cn=${group}`, cfg.ldap.groups.base), new Change({
      operation: 'delete',
      modification: new Attribute({
        type: 'member', values: [ makedn(`cn=${user}`, cfg.ldap.users.base) ]
      }),
    }))
  }

  ///

  async getObject(dn) : Promise<any> {
    return await this.client.search(dn, {}).then(x => x.searchEntries.first())
  }


}

function makedn(...items) : string {
  items.push(cfg.ldap.baseDN)
  return items.filter(x => x != undefined).join(',')
}



