import {CIAdmin, PathRef} from "./ciadmin";
import {Gitlab} from "./gitlab";
import {cfg} from "./cfg";
import {Sonar} from "./sonar";
import {ignore} from "./utils";
import {Nexus} from "./nexus";

describe('PathRef', () => {

  test('parsing', () => {
    expect(PathRef.parse('group/subgroup/project/repo:branch'))
      .toEqual({group: 'group/subgroup', project: 'project', repo: 'repo', branch: 'branch' } as PathRef)
    expect(PathRef.parse('group/subgroup/project/repo'))
      .toEqual({group: 'group/subgroup', project: 'project', repo: 'repo', branch: undefined } as PathRef)
    expect(PathRef.parse('group/subgroup/project/'))
      .toEqual({group: 'group/subgroup', project: 'project', repo: undefined, branch: undefined } as PathRef)
    expect(PathRef.parse('group/subgroup//'))
      .toEqual({group: 'group/subgroup', project: undefined, repo: undefined, branch: undefined } as PathRef)
    expect(PathRef.parse('group//'))
      .toEqual({group: 'group', project: undefined, repo: undefined, branch: undefined } as PathRef)

    expect(PathRef.parse('project/'))
      .toEqual({group: undefined, project: 'project', repo: undefined, branch: undefined } as PathRef)
    expect(PathRef.parse('project/repo'))
      .toEqual({group: undefined, project: 'project', repo: 'repo', branch: undefined } as PathRef)

    expect(PathRef.parse('project'))
      .toEqual({group: undefined, project: 'project', repo: undefined, branch: undefined } as PathRef)
  })

  test('parsing: groups', () => {
    expect(PathRef.parse('group//')).toEqual({group: 'group'} as PathRef)
    expect(PathRef.parse('project/')).toEqual({project: 'project'} as PathRef)
    expect(PathRef.parse('project/repo')).toEqual({project: 'project', repo: 'repo'} as PathRef)
  })

  test('parsing: projects', () => {
    expect(PathRef.parse('project/')).toEqual({project: 'project'} as PathRef)
    expect(PathRef.parse('group/project/')).toEqual({group: 'group', project: 'project'} as PathRef)
    expect(PathRef.parse('group/subgroup/project/')).toEqual({group: 'group/subgroup', project: 'project'} as PathRef)
  })

  test('parsing: repos', () => {
    expect(PathRef.parse('project/repo')).toEqual({project: 'project', repo: 'repo'} as PathRef)
    expect(PathRef.parse('group/project/repo')).toEqual({group: 'group', project: 'project', repo: 'repo'} as PathRef)
    expect(PathRef.parse('group/subgroup/project/repo')).toEqual({group: 'group/subgroup', project: 'project', repo: 'repo'} as PathRef)
    expect(PathRef.parse('group/subgroup/project/repo:branch')).toEqual({group: 'group/subgroup', project: 'project', repo: 'repo', branch: 'branch'} as PathRef)
  })

  test('parsing: branches', () => {
    expect(PathRef.parse('project/repo:branch')).toEqual({project: 'project', repo: 'repo', branch: 'branch'} as PathRef)
    expect(PathRef.parse('group/project/repo:branch')).toEqual({group: 'group', project: 'project', repo: 'repo', branch: 'branch'} as PathRef)
  })

  test('rendering: groups', () => {
    //fixme root ref: expect(PathRef.parse('//')?.toString()).toEqual('group/')
    expect(PathRef.parse('group//')?.toString()).toEqual('group//')
    expect(PathRef.parse('group/subgroup/')?.toString()).toEqual('group/subgroup/')
    expect(PathRef.parse('l1/l2/l3/')?.toString()).toEqual('l1/l2/l3/')
  })

  test('rendering: projects with no group', () => {
    expect(PathRef.parse('project/')?.toString()).toEqual('project/')
    expect(PathRef.parse('project/repo')?.toString()).toEqual('project/repo')
    expect(PathRef.parse('project/repo:branch')?.toString()).toEqual('project/repo:branch')
  })

  test('parsing errors', () => {
    // empty
    expect(() => PathRef.parse('')).toThrow(/invalid/i)
  })

  test('parent group name', () => {
    expect(new PathRef({group: 'l0/l1/l2'}).getParentGroup()).toBe('l0/l1')
    expect(new PathRef({group: 'l0/l1'}).getParentGroup()).toBe('l0')
    expect(new PathRef({group: 'l0'}).getParentGroup()).toBeUndefined()
    expect(new PathRef({project: 'p'}).getParentGroup()).toBeUndefined()
  })

  test('parent', () => {
    let ref = PathRef.parse('group/subgroup/project/repo:branch')
    expect(ref.getParent()?.toString()).toBe('group/subgroup/project/repo')
    expect(ref.getParent()?.getParent()?.toString()).toBe('group/subgroup/project/')
    expect(ref.getParent()?.getParent()?.getParent()?.toString()).toBe('group/subgroup//')
    expect(ref.getParent()?.getParent()?.getParent()?.getParent()?.toString()).toBe('group//')
    expect(ref.getParent()?.getParent()?.getParent()?.getParent()?.getParent()).toBeUndefined()
  })

  test('parent2', () => {
    expect(PathRef.parse('group//').getParent()).toBeUndefined()
    expect(PathRef.parse('project/').getParent()).toBeUndefined()
  })
})

jest.setTimeout(60000)

xdescribe('nexus', () => {
  test('repositories', async () => {
    await new CIAdmin().configureNexusRepositories()
  })
})

xdescribe('development', () => {

  let gitlab = new Gitlab()

  beforeEach(() => {
    cfg.groups = [{
      name: 'jrevolt',
      projects: [{
        name: 'templates',
        repositories: [
          { name: 'database-mssql' },
          { name: 'dotnet-classlib' },
          { name: 'dotnet-console' },
          { name: 'dotnet-webapi' },
          { name: 'nodejs-npm' },
          { name: 'nodejs-react-app' },
        ]
      }],
    }]
    cfg.resolveReferences()
  })

  test('prepare', async () => {

    await gitlab.makeAllRepositories()
    await gitlab.makeProject('jrevolt/ci')
    await Promise.all([
      gitlab.configureVariables("jrevolt/ci", cfg.ci.variables),
      gitlab.configureVariables("jrevolt/templates", {
        CI_DEBUG_TRACE: false,
        CIX_DEBUG: true,
      }),
      // prepareSonar({}, 'jrevolt/templates/dotnet-webapi:master'),
      // updateSonarUserGroups({},'jrevolt/templates/dotnet-webapi'),
    ])
  })
})

describe('project setup', () => {

  let ciadmin = new CIAdmin();
  let gitlab = new Gitlab()
  let sonar = new Sonar()
  let id : string
  let ref : PathRef
  let cleanup : (()=>Promise<void|any>)[]

  beforeEach(async () => {
    id = new Date().getTime().toString().replace(/.*(\d{5})$/, '$1')
    ref = PathRef.parse(`group-${id}/project-${id}/repo-${id}:branch-${id}`)
    cleanup = []
  })

  afterEach(async () => {
    await gitlab.deleteGroup(ref.asGroupRef().toString()).catch(ignore)
    await cleanup.forEachAsync(async (x) => await x().catch(ignore))
  })

  // test("qdh", async () => {
  //   await ciadmin.setupProject(
  //     "test/qdh",
  //     "qdh",
  //     "qdh",
  //     "jrevolt/ci/pipeline",
  //     "jrevolt/ci/deployment")
  // })
  //
  // test("test-project", async () => {
  //   await ciadmin.setupProject(
  //     "test/project",
  //     "test-project",
  //     "test-project",
  //     "jrevolt/ci/pipeline",
  //     "jrevolt/ci/deployment")
  // })

  test("cisetup", async () => {
    await ciadmin.setupProject();
  })

  test("setupSonar", async () => {
    await gitlab.makeProject(ref.asRepoRef().toString())
    await ciadmin.setupSonar(ref, ['cs'])
  })

  test('sonar badges in gitlab merge requests', async () => {
    await gitlab.makeProject(ref.asRepoRef().toString())
    await gitlab.createRepositoryFile(ref, 'README.md', 'README', 'README')
    let feature = `feature-${id}`
    let fref = PathRef.parse(`${ref.asRepoRef()}:${feature}`)
    await gitlab.createBranch(ref, feature, ref.branch)
    await gitlab.createMergeRequest(fref, ref.branch, 'test', 'test')
    await sonar.makeProject(fref)
    cleanup.push(() => sonar.deleteProject(fref))
    await gitlab.updateSonarBadgesInMergeRequestDescription(fref.asRepoRef(), fref.branch)
  })
})

describe('sonar jobs', () => {

  let ciadmin = new CIAdmin()
  let gitlab = new Gitlab()
  let sonar = new Sonar()
  let id
  let user, group

  beforeEach(() => {
    id = new Date().getTime().toString().replace(/.*(\d{5})$/, '$1')
    group = `group-${id}`
    user = `user-${id}`
  })

  afterEach(async () => {
    await Promise.all([
      gitlab.deleteGroup(group),
      sonar.deleteUserGroup(group),
      gitlab.deleteUser(user),
      sonar.deleteUser(user),
    ]).catch(ignore)

    // await sonar.listUserGroups().then(async (groups) =>
    //   await groups.filter(x => /group-\d+/.test(x.name)).forEachAsync(async (g) => {
    //     await sonar.deleteUserGroup(g.name)
    //   }))
    // await gitlab.listGroups().then(async (groups) =>
    //   await groups.filter(g => /group-\d+/.test(g.name)).forEachAsync(async g =>
    //     await gitlab.deleteGroup(g.name)))
    // await gitlab.listUsers().then(async users =>
    //   await users.filter(u => /test-\d+-.*/.test(u.username)).forEachAsync(async u =>
    //     await gitlab.deleteUser(u.username)))

  })

  xtest('consolidate sonar users', async () => {
  })

  test('update user groups', async () => {
    await ciadmin.updateSonarUserGroups();
  })

  test('update user group', async () => {
    await gitlab.makeGroup(group)
    await gitlab.makeUser(user, `${user}@example.com`)
    await gitlab.addMember(user, group)
    await sonar.makeUserGroup(group)
    await expect(ciadmin.updateSonarUserGroup(group)).resolves.toBeUndefined()
    await expect(sonar.listGroupMembers(group)).resolves.toHaveLength(2) //owner + test user
  })
})

describe('gitlab jobs', () => {

  let ciadmin = new CIAdmin()

  test('users: ldap to gitlab', async () => {
    await ciadmin.updateGitlabUsers()
  })
})


describe('nexus', () => {

  let ciadmin = new CIAdmin()
  let nexus = new Nexus()

  test('configure security', async () => {
    await ciadmin.configureNexusAuth()
  })

  test('configure truststore', async () => {

  })

  test('proxy', async () => {
    await ciadmin.configureNexusProxy()
  })

  test('repositories', async () => {
    // await ciadmin.configureNexusTruststore()
    // await ciadmin.configureNexusProxy()
    await ciadmin.configureNexusRepositories()
  })
})
