import {cfg} from "./cfg";
import {ignore, report, rethrow, xrequest} from "./utils";
import extend from "extend";
import {Gitlab, GitlabAccess} from "./gitlab";
import {PathRef} from "./ciadmin";
import {Elastic, EUser} from "./elastic";
import {log} from "./log";
import _ from "lodash";
import nodemailer from "nodemailer";
import AsyncLock = require('async-lock');

export interface KSpace {
  id: string,
  name?: string,
  disabledFeatures?: string[],
}

export interface KRole {
  name?: string,
  metadata?: any,
  elasticsearch?: {
    cluster?: string[],
    indices?: [{
      names?: string[],
      privileges?: string[],
      allow_restricted_indices?: boolean,
    }],
    run_as?: [],
  },
  kibana?: [{
    base?: string[],
    feature?: {
      discover?: string[],
      dashboard?: string[],
      dev_tools?: string[],
      savedObjectsManagement?: string[],
    },
    spaces?: string[],
  }]
}

export interface KIndexPattern {
  id: string,
  version: string,
  title: string,
  fields?: any,
  fieldFormats?: any,
  runtimeFieldMap?: any,
  fieldAttrs?: any,
  allowNoIndex?: boolean,
}

export class Kibana {

  ownersRoleName = (space:string) => `${space}:owner`
  readersRoleName = (space:string) => `${space}:reader`
  writersRoleName = (space:string) => `${space}:writer`


  /// spaces ///

  async createSpace(id, name?) {
    //https://www.elastic.co/guide/en/kibana/current/spaces-api-post.html
    return await(xrequest(post(`api/spaces/space`, {
      id: id,
      name: name ?? id,
      disabledFeatures: [
        'enterpriseSearch',
        'logs',
        'infrastructure',
        'apm',
        'uptime',
        'monitoring',
        'stackAlerts',
        'actions',
      ],
      //initials
      //color
      //imageUrl
    })))
  }

  async getSpace(name) : Promise<KSpace> {
    return await xrequest(get(`api/spaces/space/${name}`))
      .catch(err => err.statusCode == 404 ? undefined : rethrow(err))
  }

  async makeSpace(id, name?) : Promise<KSpace> {
    return (
      await this.getSpace(id)
      ||
      await this.createSpace(id, name)
    )
  }

  async listSpaces() : Promise<KSpace[]> {
    return await xrequest(get(`api/spaces/space`))
  }

  async deleteSpace(name) : Promise<boolean> {
    return await xrequest(del(`api/spaces/space/${name}`))
      .then(() => true)
      .catch(err => err.statusCode == 404 ? false : rethrow(err))
  }

  /// features ///




  /// roles ///

  async makeRole(name, settings : KRole = {}) {
    //https://www.elastic.co/guide/en/kibana/current/role-management-api-put.html
    await xrequest(put(`api/security/role/${name}`, settings))
  }

  async getRole(name) : Promise<KRole> {
    return await xrequest(get(`api/security/role/${name}`))
      .catch(err => err.statusCode == 404 ? undefined : rethrow(err))
  }

  async listRoles() : Promise<KRole[]> {
    return await xrequest(get(`api/security/role`))
  }

  async deleteRole(name) : Promise<boolean> {
    return await xrequest(del(`api/security/role/${name}`))
      .then(()=>true)
      .catch(err => err.statusCode == 404 ? false : rethrow(err))
  }

  /// index patterns ///

  async getDefaultIndexPattern(space) : Promise<string> {
    return await xrequest(get(`s/${space}/api/index_patterns/default`))
      .then(x => x.index_pattern_id ?? undefined)
  }

  async setDefaultIndexPattern(space, id) {
    await xrequest(post(`s/${space}/api/index_patterns/default`, {
      index_pattern_id: id,
      force: true
    }))

  }

  async createIndexPattern(space, options?: {
      pattern?: string,
      override?: boolean,
      refresh?: boolean,
      timeFieldName?: string,
      setDefault?: boolean,
    }): Promise<KIndexPattern> {

    let created = await xrequest(post(`s/${space}/api/index_patterns/index_pattern`, {
      override: options?.override ?? true,
      refresh_fields: options?.refresh ?? true,
      index_pattern: {
        title: options?.pattern ?? `${space}-*`,
        //type
        timeFieldName: options?.timeFieldName ?? '@timestamp',
        //sourceFilters: []
        //fields: {}
        //typeMeta: {}
        //fieldFormats: {},
        //fieldAttrs: {},
        //runtimeFieldMap: {},
        //allowNoIndex: false,
      },
    })).then(x => x.index_pattern)

    if (options?.setDefault ?? false) await this.setDefaultIndexPattern(space, created.id)

    return created
  }

  async getIndexPattern(space, id?) : Promise<KIndexPattern|undefined> {
    id = id ?? await this.getDefaultIndexPattern(space)

    if (!id) return undefined

    return await xrequest(get(`s/${space}/api/index_patterns/index_pattern/${id}`))
      .then(x => x.index_pattern)
  }

  async findIndexPattern(space, pattern?) : Promise<KIndexPattern|undefined> {
    let all = await this.listIndexPatterns(space)
    let found = all.find(x => x.title == pattern)
    return found ? await this.getIndexPattern(space, found.id) : undefined
  }

  async listIndexPatterns(space) : Promise<KIndexPattern[]> {
    // there is no list all API for this
    let saved = await load(`s/${space}/api/saved_objects/_find`, {type: 'index-pattern'}, x => x.saved_objects)
    return saved.map(x => <KIndexPattern>{
      id: x.id,
      title: x.attributes.title,
      version: x.version,
    })
  }

  async deleteIndexPattern(space, id?) {
    id = id ?? await this.getDefaultIndexPattern(space)
    if (!id) return false
    return await xrequest(del(`s/${space}/api/index_patterns/index_pattern/${id}`))
      .then(() => true)
      .catch(err => err.statusCode == 404 ? false : rethrow(ignore))
  }


  /// custom ///

  async setupProject(p : {
    path: string, // gitlab project
    space?: string,
    username?: string,

  }) {

    let space = p.space ?? p.path.replace('/', '-')

    await this.makeSpace(space, p.path)

    await this.createIndexPattern(space, {setDefault: true})

    await this.makeRole(this.ownersRoleName(space), {
      elasticsearch: {
        cluster: [
          'manage_index_templates',
          'monitor',
        ],
        indices: [{
          names: [ `${space}-*` ],
          privileges: [
            'all',
            'view_index_metadata',
            'manage',
          ],
        }],
      },
      kibana: [{
        base: ['all'],
        spaces: [ space ]
      }],
    })

    await this.makeRole(this.readersRoleName(space), {
      elasticsearch: {
        cluster: [],
        indices: [{
          names: [
            `${space}`,
            `${space}-*`,
          ],
          privileges: ['read']
        }],
      },
      kibana: [{
        base: [],
        feature: {
          discover: ['all'],
          dashboard: ['all'],
          dev_tools: ['all'],
          savedObjectsManagement: ['all'],
        },
        spaces: [ space ],
      }],
    })

    await this.makeRole(this.writersRoleName(space), {
      elasticsearch: {
        cluster: [ 'monitor' ], // todo is this needed?
        indices: [{
          names: [ space ],
          privileges: [ 'write' ],
        }],
      }
    })
  }

  /**
   * replicate gitlab group/project membership in elastic/kibana
   */
  async syncUsers() {

    /**
     * elastic:
     * - users are assigned list of managed roles
     * - there is no way to get role memberships
     *
     * approach:
     * - find all gitlab groups for which there is a matching kibana space
     * - for each gitlab project in a known group, list all memberships with inheritance
     * - create user in elastic, if missing
     * - assign a suitable role to user (maintainer => owner, developer => reader
     * - cleanup: remove role assignments for other space-related roles
     *
     * issues:
     * - cleanup: users removed in gitlab are ignored in this workflow
     */

    let kibana = this
    let elastic = new Elastic()
    let gitlab = new Gitlab()

    let kSpaces = await kibana.listSpaces()
    let eUsers = await elastic.listUsers()
    let gGroups = await gitlab.listGroups()

    // space name == gitlab group path
    gGroups = gGroups.filter(g => kSpaces.find(s => s.name == g.full_path)) // todo: maybe slow

    let lock = new AsyncLock()

    await gGroups.forEachAsync(async (g) => {
      let ref = PathRef.parseGroup(g.full_path)
      let space = kSpaces.find(x => x.name == g.full_path)
      let spaceId = space?.id ?? fail()
      let owners = kibana.ownersRoleName(spaceId)
      let readers = kibana.readersRoleName(spaceId)
      let writers = kibana.writersRoleName(spaceId)

      // roles aggregated from all gitlab projects in a given group (space)
      let newUserRolesByUsername : Map<string,Set<string>> = new Map()

      let projects = await gitlab.listGroupProjects(ref)
      await projects.forEachAsync(async (p) => {
        let members = await gitlab.getProjectMembers(PathRef.parse(p.path_with_namespace), true)
        await members.forEachAsync(async (m) => {
          let newRoles
            = m.access_level >= GitlabAccess.Owner ? [ owners, writers ]
            : m.access_level >= GitlabAccess.Maintainer ? [ owners ]
            : m.access_level >= GitlabAccess.Developer ? [ readers ]
            : []

          await lock.acquire(m.username, async () => {
            let roles = newUserRolesByUsername.get(m.username)
            if (!roles) newUserRolesByUsername.set(m.username, roles = new Set())
            newRoles.forEach(r => roles!.add(r))
          })
        })
      })

      // reconcile + update
      let candidates = [owners, readers, writers]
      for (let [user,roles] of newUserRolesByUsername.entries()) {
        await lock.acquire(user, async () => {
          let gUser = await gitlab.getUser(user)
          let eUser = await elastic.makeUserFromGitlab(gUser)

          let eOldRoles : Set<string> = new Set(eUser.roles)
          let eNewRoles : Set<string> = new Set(eOldRoles)

          // remove outdated assignments
          Array.from(eNewRoles)
            .filter(r => candidates.contains(r))
            .filter(r => !roles.has(r))
            .forEach(r => eNewRoles.delete(r))

          // add new
          Array.from(roles)
            .filter(r => !eNewRoles.has(r))
            .forEach(r => eNewRoles.add(r))

          // update
          if (!_.isEqual(eNewRoles, eOldRoles)) {
            let roles = Array.from(eNewRoles)
            log.info(`Updating user ${user}, roles=[${roles}]`)
            eUser.roles = roles
            await elastic.updateUser(eUser).catch(report)
          }
        })
      }

      // remove obsolete
      let actual: Set<string> = new Set()
      candidates.forEach(r => {
        eUsers
          .filter(u => u.metadata?.source == 'gitlab') // ignore non-gitlab users like local ES technical accounts
          .filter(u => u.roles?.contains(r)).forEach(u => actual.add(u.username!))
      })
      let desired: Set<string> = new Set()
      await projects.forEachAsync(async (p) => {
        await gitlab.getProjectMembers(PathRef.parse(p.path_with_namespace), true).then(
          async (members) => await members.forEachAsync(async (m) =>
            desired.add(m.username)))
      })
      await Array.from(actual).filter(u => !desired.has(u)).forEachAsync(async (u) =>
        await lock.acquire(u,
          async () => {
            let eUser = await elastic.getUser(u)
            let oldRoles : Set<string> = new Set(eUser.roles)
            candidates.forEach(x => oldRoles.delete(x))
            let newRoles = Array.from(oldRoles)
            eUser.roles = newRoles
            log.info(`Updating user ${u}, roles=${newRoles}`)
            await elastic.updateUser(eUser)
          })
      )
    })
  }

  async activateNewUsers() {
    let gitlab = new Gitlab()
    let elastic = new Elastic()
    let mailer = nodemailer.createTransport(extend(true, {}, cfg.mailer.transport, {
      logger: cfg.debug,
    }))

    try {
      let eUsers = await elastic.listUsers()
      let mailerAllow = new RegExp(cfg.mailer.domainFilter.allow)
      let mailerDeny = new RegExp(cfg.mailer.domainFilter.deny)
      let eFilteredUsers = eUsers
        .filter(u => u.metadata?.creator == 'ciadmin')
        .filter(u => u.metadata?.source == 'gitlab')
        .filter(u => u.email && mailerAllow.test(u.email) && !mailerDeny.test(u.email))
        .filter(u => !u.metadata?.passwordSent)
      log.info('Found %d ES users. %d users need activation...', eUsers.length, eFilteredUsers.length)

      await eFilteredUsers.forEachAsync(async (u) => {
        let gUser = await gitlab.getUser(u.username)
        if (!gUser) return // ignore

        log.info(`Activating user ${u.username} <${gUser.email}>`)
        let pass = elastic.generatePassword()
        await elastic.updateUserPassword(u.username, pass)
        await mailer.sendMail({
          from: cfg.mailer.from,
          to: gUser.email,
          subject: 'Your Kibana/Elasticsearch account',
          text: `
          url: ${cfg.kibana.url}
          user: ${u.username}
          pass: ${pass}

          Login into ${cfg.kibana.url}/security/account and change your password.
        `,
        })
        await elastic.updateUser(extend(u, <EUser>{
          email: gUser.email,
          full_name: gUser.name,
          metadata: { passwordSent: true }
        }))
      })
    } finally {
      mailer.close()
    }
  }
}

// function uri(uri, space?) {
//   return space ? `s/${space}/api/${uri}` : `api/${uri}`
// }

async function load(uri, query = {}, extractor?) {
  let result : any[] = []
  for (let page=1;; page++) {
    let response = await xrequest(get(uri, extend(query, {page: page})))
    let data = extractor ? extractor(response) : response
    data.forEach(x => result.push(x))
    if (response.page * response.per_page >= response.total) break // no more data
  }
  return result
}

function req(uri, method, data?, options?) {
  // cfg.sonar.url || fail('cfg.sonar.url')
  // cfg.sonar.token || fail('cfg.sonar.token')
  return extend(
    {
      url: `${cfg.kibana.url}/${uri}`,
      headers: {
        'Authorization': `Basic ${Buffer.from(`${cfg.kibana.username}:${cfg.kibana.password}`).toString('base64')}`,
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'kbn-xsrf': 'reporting',
      },
      method: method,
      resolveWithFullResponse: false,
      json: true,
      qs: (method === 'GET') ? data : {},
      query: (method === 'GET') ? data : {},
      //form: (method.match(/PUT|POST/)) ? data : {},
      body: (method.match(/PUT|POST/)) ? data : {},
    },
    options || {}
  );
}

function get(uri, data?) {
  return req(uri, 'GET', data);
}

function post(uri, data) {
  return req(uri, 'POST', data);
}

function put(uri, data) {
  return req(uri, 'PUT', data);
}

// function xpost(uri, data) {
//   return req(uri, 'POST', data, { resolveWithFullResponse: true });
// }

function del(uri) {
  return req(uri, 'DELETE')
}

