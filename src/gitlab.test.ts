import './testutils'
import {Gitlab, GitlabUser} from "./gitlab";
import {ignore, xrequest} from "./utils";
import {PathRef} from "./ciadmin";

let gitlab : Gitlab;

let id, group, subgroup, project, repo, branch, path, pathref:PathRef, user;

beforeEach(() => {
  gitlab = new Gitlab()
  id = new Date().getTime().toString().replace(/.*(\d{5})$/, '$1')
  group = `group-${id}`
  subgroup = `subgroup-${id}`
  project = `project-${id}`
  repo = `repo-${id}`
  path = `${group}/${project}/${repo}`
  branch = `branch-${id}`
  pathref = PathRef.parse(`${path}:${branch}`)
  user = `test-${id}`
})

afterEach(async () => {
  await Promise.all([
    gitlab.deleteGroup(group),
    gitlab.listUsers().then(async (all) =>
      all.filter(u => u.username.startsWith(user)).forEachAsync(async (u) =>
        await gitlab.deleteUser(u.username))),
  ]).catch(ignore)
})

test('cleanup', async () => {
  await Promise.all([
    gitlab.listUsers().then(async (list) =>
      list.filter(u => /^test-*/.test(u.username))
        .forEachAsync(async (u) => await gitlab.deleteUser(u.username))),
    gitlab.listGroups().then(async (list) =>
      list.filter(g => /^(group|gtest)-.*/.test(g.name))
        .forEachAsync(async (g) => await gitlab.deleteGroup(g.name)
      ))

  ])

})

describe('objects', () => {
  test('parse project ref', () => {
    let ref = gitlab.parseProjectPath("group/subgroup/project/repo:branch")
    expect(ref.group).toBe('group:subgroup')
    expect(ref.project).toBe('project')
    expect(ref.repo).toBe('repo')
    expect(ref.branch).toBe('branch')
  })

  xtest('resolve repo', () => {
    let ref = gitlab.parseProjectPath(`${group}/${project}/${repo}`)
    let r = gitlab.resolveRepository(ref)
    expect(r.name).toBe(repo)
    expect(r.project.name).toBe(project)
    expect(r.project.group.name).toBe(group)
  })

  test('parse project ref: no branch', () => {
    let ref = gitlab.parseProjectPath("group/subgroup/project/repo")
    expect(ref.group).toBe('group:subgroup')
    expect(ref.project).toBe('project')
    expect(ref.repo).toBe('repo')
    expect(ref.branch).toBe(undefined)
  })

  test('parse project ref: missing repo', () => {
    expect(() => {
      gitlab.parseProjectPath("group/project")
    }).toThrow(Error)
  })
})

describe('groups', () => {
  test('create group', async () => {
    await expect(gitlab.createGroup(group)).resolves.toHaveProperty('path', group)
    await expect(gitlab.createGroup(group)).rejects.toThrow(/already been taken/)
  })

  test('create subgroup', async () => {
    let g = await gitlab.createGroup(group)
    await expect(gitlab.createGroup(subgroup, g.id))
      .resolves.toHaveProperty('full_path', `${group}/${subgroup}`)
  })

  test('create subgroup full path', async () => {
    await expect(gitlab.makeGroup(`${group}/${subgroup}`))
      .resolves.toHaveProperty('full_path', `${group}/${subgroup}`)
  })

  test('get group', async () => {
    await expect(gitlab.getGroup(group)).resolves.toBeUndefined()
    await gitlab.createGroup(group)
    await expect(gitlab.getGroup(group)).resolves.toHaveProperty('id')
  })

  test('make group', async () => {
    await expect(gitlab.makeGroup(group)).resolves.toHaveProperty('id')
    // await expect(gitlab.createGroup(group)).rejects.toThrow(/already been taken/)
    // await expect(gitlab.makeGroup(group)).resolves.toHaveProperty('id')
  })
})

describe('projects', () => {

  let g, p, r, group, path;

  beforeEach(() => {
    g = `gtest-${id}`
    p = `ptest`
    r = `rtest`
    group = `${g}/${p}`
    path = `${group}/${r}`
  })

  afterEach(async () => {
    await gitlab.deleteGroup(g).catch(ignore)
  })

  test('create', async () => {
    await gitlab.makeGroup(group)
    await expect(gitlab.createProject(path)).resolves.toHaveProperty('id')
    await expect(gitlab.createProject(path)).rejects.toThrow(/already.*taken/i)
  })

  test('get', async () => {
    await expect(gitlab.getProject(path)).resolves.toBeUndefined()
    await gitlab.makeProject(path)
    await expect(gitlab.getProject(path)).resolves.toBeDefined()
  })

  test('make', async () => {
    await expect(gitlab.getProject(path)).resolves.toBeUndefined()
    await expect(gitlab.makeProject(path)).resolves.toHaveProperty('id')
    await expect(gitlab.makeProject(path)).resolves.toBeDefined()
  })

  test('list', async () => {
    await gitlab.makeProject(path)
    await expect(gitlab.listProjects()).resolves.toBeNonEmptyArray()
  })

  test('delete', async () => {
    await expect(gitlab.deleteProject(path)).rejects.toThrow(/not found/i)
    await gitlab.makeProject(path)
    await expect(gitlab.deleteProject(path)).resolves.toBeUndefined()
  })

})

describe('branches', () => {
  beforeEach(async () => {
    await gitlab.makeProject(path)
    await gitlab.createRepositoryFile(PathRef.parse(`${path}:${branch}`), 'README.md', 'README', 'README')
  })
  test('create', async () => {
    await gitlab.createBranch(pathref, `feature-${id}`, branch)
  })
})

describe('merge requests', () => {

  let src, dst, ref:PathRef, srcref:PathRef, dstref:PathRef

  beforeEach(async () => {
    src = `feature-${id}`
    dst = branch
    ref = PathRef.parse(pathref.asRepoRef().toString())
    srcref = PathRef.parse(`${ref.toString()}:${src}`)
    dstref = PathRef.parse(`${ref.toString()}:${dst}`)
    await gitlab.makeProject(ref.toString())
    await gitlab.createRepositoryFile(pathref, 'README.md', 'README', 'README')
    await gitlab.createBranch(ref, src, dst)
  })

  test('create', async () => {
    await gitlab.createMergeRequest(srcref, dst, 'test', 'test')
    return
  })
})

describe('users', () => {
  let prefix, u : GitlabUser;
  beforeEach(() => {
    prefix = `test-${id}`
    u = { username: prefix, email: `${prefix}@example.com` }
  })

  afterEach(async () => {
    let users = await gitlab.listUsers()
    await gitlab.listUsers().then(async (users) =>
      await users
        .filter(u => u.username.startsWith(prefix))
        .forEachAsync(async (u) => await gitlab.deleteUser(u.username))
    ).catch(ignore)
  })

  test('create', async () => {
    await expect(gitlab.createUser(u.username, u.email)).resolves.toHaveProperty('id')
    await expect(gitlab.createUser(u.username, u.email)).rejects.toThrow(/already.*taken/i)
  })

  test('get', async () => {
    await expect(gitlab.getUser(u.username)).resolves.toBeUndefined()
    await gitlab.createUser(u.username, u.email)
    await expect(gitlab.getUser(u.username)).resolves.toHaveProperty('id')
  })

  test('delete', async () => {
    await expect(gitlab.deleteUser(u.username)).rejects.toThrow(/not found/i)
    await gitlab.createUser(u.username, u.email)
    await expect(gitlab.deleteUser(u.username)).resolves.toBeUndefined()
  })

  test('list', async () => {
    await expect(gitlab.listUsers()).resolves.toBeDefined()
    await gitlab.createUser(u.username, u.email)
    expect(gitlab.listUsers().then(list => list.find(x => x.username == u.username))).toBeDefined()
  })
})

describe('tokens', () => {
  test('delete test-qdh tokens', async () => {
    await gitlab.revokeImpersonationApiToken('test-qdh', 'test-qdh')
  })
})

describe('membership', () => {

  let developer, maintainer;

  beforeEach(async () => {
    developer = `${user}-developer`
    maintainer = `${user}-maintainer`
    await Promise.all([
      gitlab.makeProject(path),
      gitlab.createUser(developer, `${developer}@example.com`),
      gitlab.createUser(maintainer, `${maintainer}@example.com`),
    ])
  })

  test('add', async () => {
    await expect(gitlab.addMember(developer, group)).resolves.toBeDefined()
    await expect(gitlab.addMember(developer, group)).resolves.toBeDefined()
    //project
    await expect(gitlab.addMember(maintainer, path)).resolves.toBeDefined()
    await expect(gitlab.addMember(maintainer, path)).resolves.toBeDefined()
  })

  test('remove', async () => {
    await expect(gitlab.removeMember(developer, group)).rejects.toThrow(/not found/i)
    await expect(gitlab.addMember(developer, group)).resolves.toBeDefined()
    await expect(gitlab.removeMember(developer, group)).resolves.toBeUndefined()
    // project
    await expect(gitlab.removeMember(maintainer, path)).rejects.toThrow(/not found/i)
    await expect(gitlab.addMember(maintainer, path)).resolves.toBeDefined()
    await expect(gitlab.removeMember(maintainer, path)).resolves.toBeUndefined()
  })

  test('list', async () => {
    await expect(gitlab.listMembers(group)).resolves.toHaveLength(1) // owner
    await gitlab.addMember(developer, group)
    await expect(gitlab.listMembers(group)).resolves.toHaveLength(2)
    // project
    await expect(gitlab.listMembers(path)).resolves.toHaveLength(0)
    await gitlab.addMember(maintainer, path)
    await expect(gitlab.listMembers(path)).resolves.toHaveLength(1)
  })
})

xdescribe('variables', () => {

  test('configure variables', async () => {
  })

  test('cleanup variables', async () => {
    await gitlab.cleanupVariables("jrevolt/ci")
  })
})

xdescribe('merge_requests', () => {
  xtest('comment', async () => {
    let x = await xrequest({
      method: 'GET',
      url: 'https://sonar-badges.jrevolt.io/api/project_badges/measure?project=test%3Aproject%3Aservices%3Aaaa&metric=alert_status'
    })
    let ref = await gitlab.uploadFile('test/project/services', 'image.svg', x)
    let c = await gitlab.addMergeRequestComment('test/project/services', 2, `![sonar](${ref})`)
    return
  })
  xtest('updatemr', async () => {
    await gitlab.updateSonarBadgesInMergeRequestDescription('test/project/services', 'aaa');
  })
})



