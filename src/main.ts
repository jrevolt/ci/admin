import "./cfg"
import "./log"

import {Options, registerCommands} from "./commands";
import {is_cli, is_unit_test, log} from "./log";
import {reportedErrors, rethrow} from "./utils";
import extend from "extend";
import * as version from './version.json'
import * as commander from 'commander'
import {cfg} from "./cfg";

export class Main {

  private static $instance : Main

  static get instance() : Main {
    return Main.$instance || (Main.$instance = new Main())
  }

  readonly program: commander.Command = new commander.Command('ciadmin')
    .version(this.versionString())
    .option('-d, --debug')


  command : commander.Command = this.program

  options() { return this.program.opts() as Options }

  async run(args?: string[]) {
    if (args) args.unshift('dummy-exe-name', 'dummy-script-name')
    if (!args) args = process.argv

    registerCommands(this)

    return await this.program.parseAsync(args)
  }


  wrap(action) {
    const main = this;
    return async function () {
      let started = Date.now();
      let error: Error | undefined;
      let cmd = main.command = arguments[arguments.length - 1]
      cfg.command = cmd.name()

      try {
        let opts: Options = extend({}, cmd.parent.opts(), cmd.opts())
        let args: any[] = [opts]
        for (let i = 0; i < arguments.length - 1; i++) args.push(arguments[i])

        if (opts.debug) cfg.debug = true
        log.level = opts.debug ? 'debug' : log.level

        log.info(`${main.program.name()} ${main.versionString()})`)
        log.info('Executing %s', cmd.name());

        // @ts-ignore
        await action.apply(this, args);

      } catch (e) {
        error = e;
        if (is_unit_test()) rethrow(e)
        process.exitCode = 3;

      } finally {
        let elapsed = Date.now() - started;
        let logm = error || reportedErrors > 0 ? log.error : log.info;
        let msg = reportedErrors > 0 ? `There were ${reportedErrors} errors reported.` : '';
        let err = error ? error.stack : '';
        logm('Finished command %s in %d msec. %s %s', cmd.name(), elapsed, msg, err);
        //log.level = 'info'
        //log.end();
      }
    }
  }

  versionString() : string {
    return `${version.FullSemVer} (${version.CommitDate}, ${version.ShortSha})`
  }

}

if (is_cli()) Main.instance.run().then()

